package com.proyfinal.taller.repo;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import com.proyfinal.taller.model.UsuariosModel;

public interface UsuariosRepo extends JpaRepository<UsuariosModel,String>  {
	/*
	@Query(value="select e from UsuariosModel e where e.usuario=?1 and e.contrasena=?2")
	UsuariosModel verificar(String user,String passw);

			*/
	@Query("select u "
			 +" from UsuariosModel u "
			 +" where 	u.usuario=?1 and "
			 +"			u.contrasena=?2 ")
		UsuariosModel verificarCuentaUsuario(String xlogin, String xclave);
}			
  
