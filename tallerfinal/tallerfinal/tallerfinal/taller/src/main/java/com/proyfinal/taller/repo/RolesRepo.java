package com.proyfinal.taller.repo;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import com.proyfinal.taller.model.RolesModel;
import com.proyfinal.taller.model.Usuarios_RolesModel;

public interface RolesRepo extends JpaRepository< RolesModel,Integer>  {
	
	@Query(value="select u from RolesModel u where u.id_rol=?1")
	RolesModel obtenerRol(int id_rol);

}