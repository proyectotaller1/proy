package com.proyfinal.taller.repo;

import java.sql.Date;
import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import com.proyfinal.taller.model.mantenimiento_correctivosModel;
import com.proyfinal.taller.model.solicitudes_servicioModel;

public interface mantenimiento_correctivosRepo extends JpaRepository<mantenimiento_correctivosModel,Integer> {
	
	/*----- listar activo, inactivo y todos segun su estado---------*/
	 @Query(value="select u from mantenimiento_correctivosModel u "
	  		  + "where u.cod_estado between :xest1 and :xest2 order by u.comentario_deteccion_falla")
	    List<mantenimiento_correctivosModel> ListarEstadoSol(
	  	@Param("xest1") int xest1,
	  	@Param("xest2") int xest2);
	 
	 
	 /*-----------para eliminar y abilitar cod estado de fallas----------*/
	    @Modifying
	    @Query(value = "update mantenimiento_correctivosModel u set u.cod_estado=?1 where u.seq_mantenimiento_correctivo=?2")
	    public int EliminarHabilitarEstado(@Param("cod_estado") int cod_estado, @Param("seq_mantenimiento_correctivo") int seq_mantenimiento_correctivo); 

	/*----aqui generar para filtrar fecha desde y hasta --*/
	    @Query("SELECT u FROM mantenimiento_correctivosModel u WHERE u.fecha_finalizacion BETWEEN :fechaDesde AND :fechaHasta")
	    List<mantenimiento_correctivosModel> ListarPorFecha(
	        @Param("fechaDesde") Date fechaDesde,
	        @Param("fechaHasta") Date fechaHasta
	    );


}
