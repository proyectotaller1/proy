package com.proyfinal.taller.controller;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import com.proyfinal.taller.model.PersonasModel;
import com.proyfinal.taller.model.Usuarios_RolesModel;
import com.proyfinal.taller.repo.PersonasRepo;
import com.proyfinal.taller.repo.Usuarios_RolesRepo;
import com.proyfinal.taller.service.StorageService;

import jakarta.transaction.Transactional;
import jakarta.security.auth.message.callback.PrivateKeyCallback.Request;
import jakarta.servlet.http.HttpServletRequest;
import org.springframework.core.io.Resource;

@RestController
@CrossOrigin(origins = "http://localhost:4200")
public class PersonasController {
	@Autowired
	public PersonasRepo repoper;
	
	@Autowired
	public Usuarios_RolesRepo usurolrepo;
	
	//listar persona
	//@CrossOrigin(origins = "http://localhost:8100")
	@GetMapping("/api/lisper")
	public List<PersonasModel> lisdePersonas(){
	return repoper.findAll();
	}
	//listar persona de tipo cliente::---------//
	@GetMapping("/api/lisPersonaCliente")
	public List<PersonasModel> listaPersonaCliente(){
		List<PersonasModel> cliente= new ArrayList<>();
		List<Usuarios_RolesModel> UsuRol= this.usurolrepo.findAll();
		for(Usuarios_RolesModel usuario: UsuRol){
			if(usuario.getRoles().getId_rol()==102) cliente.add(usuario.getUsuarios().getPersonas());
		}
		
		return cliente;
	}
	
	//listar persona de tipo no cliente::---------//
		@GetMapping("/api/lisPersonaNoCliente")
		public List<PersonasModel> listaPersonaNOCliente(){
			List<PersonasModel> Tecnico= new ArrayList<>();
			List<Usuarios_RolesModel> UsuRol= this.usurolrepo.findAll();
			for(Usuarios_RolesModel usuario: UsuRol){
				if(usuario.getRoles().getId_rol()!=102) Tecnico.add(usuario.getUsuarios().getPersonas());
			}
			
			return Tecnico;
		}
	
	//adicionar persona
	
	@PostMapping("/api/addper")
	public void guardardocentes(@RequestBody PersonasModel xper) {
	repoper.save(xper);
	}
	//eliminar persona
	@DeleteMapping("/api/delper/{xcod}")
	public void deleteUsers(@PathVariable Integer xcod) {
		repoper.deleteById(xcod);
	}
	//actualizar o modificar persona
		@PutMapping("/api/modper/{xcod}")
		public void updateSystemUsers(@PathVariable Integer xcod, @RequestBody PersonasModel xper) {
			xper.setSeq_persona(xcod);
			repoper.save(xper);
		}
		
		/*----- abilitar inabilitar codigo estado---------*/
		
		@GetMapping("/api/lispe/{xestado}")
		public List<PersonasModel> listaPersonas_jpa(@PathVariable int xestado){
			int xest1=0, xest2=0;
			if (xestado==1) {xest1=1;xest2=1;}
			if (xestado==2) {xest2=1;}
			return repoper.ListaPersona(xest1,xest2);
		}
		
		/*-----------para eliminar cod estado ----------*/
		        @Transactional
				@PutMapping("/api/modPerEst/{xcod}")
				public int ModEstadoPersona(@RequestBody PersonasModel xper) {
					if(xper.getCod_estado()==0)
						return repoper.eliminarper(1, xper.getSeq_persona());
					else
						return repoper.eliminarper(0, xper.getSeq_persona());
				}
		        
		        /* -------------cargar imagen----------- */
		        @Autowired
				private StorageService storageService;

				@Autowired
				private HttpServletRequest request;
				
				 @PostMapping("/api/foto")
				    public Map<String, String> uploadImage(@RequestParam("file") MultipartFile file) {
				        String path = storageService.store(file);

				        // Obtener la URL de la imagen cargada
				        String host = ServletUriComponentsBuilder.fromCurrentContextPath().build().toUriString();
				        String imageUrl = host + "/" + path;

				        return Map.of("imageUrl", imageUrl);
				    }
				 @GetMapping("/{imageName:.+}")
				    public ResponseEntity<Resource> getImage(@PathVariable String imageName) {
				        Resource image = storageService.loadAsResource(imageName);
				        return ResponseEntity.ok()
				                .header("Content-Type", "image/jpeg", "image/png", "image/jpg" ) // Cambia el tipo de contenido según el formato de imagen
				                .body(image);
				    }


	
}
