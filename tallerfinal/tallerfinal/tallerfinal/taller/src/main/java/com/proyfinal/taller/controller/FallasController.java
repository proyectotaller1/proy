package com.proyfinal.taller.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.proyfinal.taller.model.FallasModel;
import com.proyfinal.taller.model.PersonasModel;
import com.proyfinal.taller.repo.FallasRepo;

import jakarta.transaction.Transactional;

@RestController

public class FallasController {
	@Autowired
	public FallasRepo repofall;
	//listar fallas
		@GetMapping("/api/lisfall")
		public List<FallasModel> lisdeFallas(){
		return repofall.findAll();
		}
		//adicionar fallas
		
		@PostMapping("/api/addfall")
		public void guardarFalla(@RequestBody FallasModel xfall) {
		repofall.save(xfall);
		}
		//modificar Fallas
				@PutMapping("/api/modfall/{xcod}")
				public void updateSystemUsers(@PathVariable Integer xcod, @RequestBody FallasModel xfall) {
					xfall.setId_falla(xcod);
					repofall.save(xfall);
				}
				/*----- listar activo, inactivo y todos segun su estado---------*/
				
				@GetMapping("/api/lisfall/{xestado}")
				public List<FallasModel> listaFallas_jpa(@PathVariable int xestado){
					int xest1=0, xest2=0;
					if (xestado==1) {xest1=1;xest2=1;}
					if (xestado==2) {xest2=1;}
					return repofall.ListarEstadoFalla(xest1,xest2);
				}
				
				/*-----------para eliminar y abilitar cod estado de fallas----------*/
				        @Transactional
						@PutMapping("/api/modFallEst/{xcod}")
						public int ModEstadoFallas(@RequestBody FallasModel xfall) {
							if(xfall.getCod_estado()==0)
								return repofall.EliminarHabilitarEstado(1, xfall.getId_falla());
							else
								return repofall.EliminarHabilitarEstado(0, xfall.getId_falla());
						}		

}
