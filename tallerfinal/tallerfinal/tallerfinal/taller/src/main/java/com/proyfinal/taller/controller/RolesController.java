package com.proyfinal.taller.controller;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.proyfinal.taller.model.RolesModel;
import com.proyfinal.taller.model.UsuariosModel;
import com.proyfinal.taller.model.Usuarios_RolesModel;
import com.proyfinal.taller.model.Usuarios_RolesModelPK;
import com.proyfinal.taller.repo.RolesRepo;
import com.proyfinal.taller.repo.Usuarios_RolesRepo;

@RestController
public class RolesController {
	
	@Autowired
	public RolesRepo reporol;
	@GetMapping("/api/lisrol")
	public List<RolesModel> lisdeRoles(){
	return reporol.findAll();
	}
	/*-----metodo para sacar al listar de roles segun al usuario pertenece---*/
	@Autowired
	public  Usuarios_RolesRepo reporolusu;
	@GetMapping("/api/obetenerusurol/{xusu}")
	public List<RolesModel> listarRoles(@PathVariable String xusu){
		List<RolesModel> ListarRoles= new ArrayList<>();
		List<Usuarios_RolesModel> usuariosRoles = this.reporolusu.findAll();
		for(Usuarios_RolesModel rolusu: usuariosRoles) {
			if(rolusu.getUsuarios().getUsuario().equals(xusu))ListarRoles.add(rolusu.getRoles());
		}
		return ListarRoles;
	}
	
}
