package com.proyfinal.taller.model;

import java.sql.Date;

import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import jakarta.persistence.JoinColumn;
import jakarta.persistence.ManyToOne;
import jakarta.persistence.Table;

@Entity
@Table(name = "tecnicos_ejecutantes")
public class tecnicos_ejecutantesModel {
	
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Id
	@Column(name = "seq_tecnicos_ejecutantes")
	int seq_tecnicos_ejecutantes;
	
	@Column(name = "fecha_de_asignacion")
	Date fecha_de_asignacion;

	@Column(name = "cod_responsable")
	int cod_responsable;
	
	@Column(name = "cod_estado")
	int cod_estado;

	//ralacion de muchos a uno con persona
	@ManyToOne
	@JoinColumn(name = "seq_orden_trabajo")
	ordenes_trabajoModel ordenes_trabajo;
	
	//relacion de muchos a uno con orden de trabajo
	@ManyToOne
	@JoinColumn(name = "seq_persona")
	PersonasModel personas;

	
	public ordenes_trabajoModel getOrdenes_trabajo() {
		return ordenes_trabajo;
	}

	public void setOrdenes_trabajo(ordenes_trabajoModel ordenes_trabajo) {
		this.ordenes_trabajo = ordenes_trabajo;
	}

	public PersonasModel getPersonas() {
		return personas;
	}

	public void setPersonas(PersonasModel personas) {
		this.personas = personas;
	}

	public int getSeq_tecnicos_ejecutantes() {
		return seq_tecnicos_ejecutantes;
	}

	public void setSeq_tecnicos_ejecutantes(int seq_tecnicos_ejecutantes) {
		this.seq_tecnicos_ejecutantes = seq_tecnicos_ejecutantes;
	}

	public Date getFecha_de_asignacion() {
		return fecha_de_asignacion;
	}

	public void setFecha_de_asignacion(Date fecha_de_asignacion) {
		this.fecha_de_asignacion = fecha_de_asignacion;
	}

	public int getCod_responsable() {
		return cod_responsable;
	}

	public void setCod_responsable(int cod_responsable) {
		this.cod_responsable = cod_responsable;
	}

	public int getCod_estado() {
		return cod_estado;
	}

	public void setCod_estado(int cod_estado) {
		this.cod_estado = cod_estado;
	}

	

	
}

