package com.proyfinal.taller.model;

import java.io.Serializable;
import java.util.Objects;

import jakarta.persistence.Embeddable;
@Embeddable
public class ordenes_trabajo_activoModelPK  implements Serializable {
	protected int seq_activo;
	protected int seq_orden_trabajo;
	
	
	public int getSeq_activo() {
		return seq_activo;
	}
	public void setSeq_activo(int seq_activo) {
		this.seq_activo = seq_activo;
	}
	public int getSeq_orden_trabajo() {
		return seq_orden_trabajo;
	}
	public void setSeq_orden_trabajo(int seq_orden_trabajo) {
		this.seq_orden_trabajo = seq_orden_trabajo;
	}
	@Override
	public int hashCode() {
		return Objects.hash(seq_activo, seq_orden_trabajo);
	}
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		ordenes_trabajo_activoModelPK other = (ordenes_trabajo_activoModelPK) obj;
		return seq_activo == other.seq_activo && seq_orden_trabajo == other.seq_orden_trabajo;
	}
	
	
}
