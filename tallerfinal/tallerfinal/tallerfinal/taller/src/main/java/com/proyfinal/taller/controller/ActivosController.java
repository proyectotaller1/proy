package com.proyfinal.taller.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.proyfinal.taller.model.ActivosModel;
import com.proyfinal.taller.model.FallasModel;
import com.proyfinal.taller.repo.ActivosRepo;

import jakarta.transaction.Transactional;

@RestController
public class ActivosController {
	@Autowired
	public ActivosRepo repoact;

	// listar Activos
	@GetMapping("/api/lisact")
	public List<ActivosModel> lisdeActivos() {
		return repoact.findAll();
	}
	
	// adicionar activos

	@PostMapping("/api/addact")
	public void guardarActivos(@RequestBody ActivosModel xfall) {
		repoact.save(xfall);
	}

	// modificar actvios
	@PutMapping("/api/modact/{xcod}")
	public void updateSystemUsers(@PathVariable Integer xcod, @RequestBody ActivosModel xfall) {
		xfall.setSeq_activo(xcod);
		repoact.save(xfall);
	}
	/*----- listar activo, inactivo y todos segun su estado---------*/
	@GetMapping("/api/lisact/{xestado}")
	public List<ActivosModel> listaFallas_jpa(@PathVariable int xestado){
		int xest1=0, xest2=0;
		if (xestado==1) {xest1=1;xest2=1;}
		if (xestado==2) {xest2=1;}
		return repoact.ListarEstadoActivo(xest1,xest2);
	}
	
	/*-----------para eliminar y abilitar cod estado de fallas----------*/
	        @Transactional
			@PutMapping("/api/modActEst/{xcod}")
			public int ModEstadoFallas(@RequestBody ActivosModel xfall) {
				if(xfall.getCod_estado()==0)
					return repoact.EliminarHabilitarEstado(1, xfall.getSeq_activo());
				else
					return repoact.EliminarHabilitarEstado(0, xfall.getSeq_activo());
			}		


}
