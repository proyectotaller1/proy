package com.proyfinal.taller.repo;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import com.proyfinal.taller.model.PersonasModel;

public interface PersonasRepo extends JpaRepository< PersonasModel,Integer>  {
	/*-----------consulta para sacar codigo estado para abilitar o inabilitar ---*/
    @Query(value="select u from PersonasModel u "
  		  + "where u.cod_estado between :xest1 and :xest2 order by u.apellido_paterno")
    List<PersonasModel> ListaPersona(
  	@Param("xest1") int xest1,
  	@Param("xest2") int xest2);
    /*------consulta para eliminar cod persona ------*/
    @Modifying
    @Query(value = "update PersonasModel u set u.cod_estado=?1 where u.seq_persona=?2")
    public int eliminarper(@Param("cod_estado") int cod_estado, @Param("seq_persona") int seq_persona);
    
    
  //Persona cliente	
  	@Query (nativeQuery=true, value="select * from personas "
  			+ "where seq_persona in (select seq_persona from usuarios where usuario "
  			+ "in(select usuario from usuarios_roles where id_rol =11 and cod_estado=1))")
  	List<PersonasModel> ListarPersonasClientes();
  	
  	//Persona recepcionate	
  		@Query (nativeQuery=true, value="select * from personas "
  				+ "where seq_persona in (select seq_persona from usuarios where usuario "
  				+ "in(select usuario from usuarios_roles where id_rol =13 and cod_estado=1))")
  		List<PersonasModel> ListarPersonasRecepcionante();

}
