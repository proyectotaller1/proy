package com.proyfinal.taller.model;

import java.sql.Date;
import java.util.Set;

import jakarta.persistence.CascadeType;
import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import jakarta.persistence.JoinColumn;
import jakarta.persistence.ManyToOne;
import jakarta.persistence.OneToMany;
import jakarta.persistence.Table;

@Entity
@Table(name="ordenes_trabajo")
public class ordenes_trabajoModel {
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Id
	@Column(name="seq_orden_trabajo")
	int seq_orden_trabajo;
	
	@Column(name="cod_tipo_mantenimiento")
	String cod_tipo_mantenimiento;
	
	@Column(name="numero_orden_trabajo")
	String numero_orden_trabajo;
	
	@Column(name="fecha_emicion")
	Date fecha_emicion;
	
	@Column(name="ubicacion_activo")
	String ubicacion_activo;
	
	@Column(name="cod_atentido")
	int cod_atentido;
	
	@Column(name="cod_estado")
	int cod_estado;
	//ralacion de muchos a uno con persona
	@ManyToOne
	@JoinColumn(name = "seq_persona")
	PersonasModel personas;
	//relacion de muchos a uno con solicitud
	@ManyToOne
	@JoinColumn(name = "seq_solicitud_servicio")
	solicitudes_servicioModel solicitudes_servicio;
	
	//ralacion de mucho a muchos con orden trab activo
	@OneToMany(mappedBy = "ordenes_trabajo")
	Set<ordenes_trabajo_activoModel> ordenes_trabajo_activo;
	
	//ralacion de Uno a Muchos(mantenimiento correctivo)
		@OneToMany(mappedBy="ordenes_trabajo", cascade = CascadeType.REMOVE)
			private Set<mantenimiento_correctivosModel> mantenimiento_correctivos;
			
	//ralacion de Uno a Muchos(tecnicos ejecutantes)
		@OneToMany(mappedBy="ordenes_trabajo", cascade = CascadeType.REMOVE)
		  private Set<tecnicos_ejecutantesModel> tecnicos_ejecutantes;

	public PersonasModel getPersonas() {
		return personas;
	}

	public void setPersonas(PersonasModel personas) {
		this.personas = personas;
	}

	public solicitudes_servicioModel getSolicitudes_servicio() {
		return solicitudes_servicio;
	}

	public void setSolicitudes_servicio(solicitudes_servicioModel solicitudes_servicio) {
		this.solicitudes_servicio = solicitudes_servicio;
	}

	public int getSeq_orden_trabajo() {
		return seq_orden_trabajo;
	}

	public void setSeq_orden_trabajo(int seq_orden_trabajo) {
		this.seq_orden_trabajo = seq_orden_trabajo;
	}

	public String getCod_tipo_mantenimiento() {
		return cod_tipo_mantenimiento;
	}

	public void setCod_tipo_mantenimiento(String cod_tipo_mantenimiento) {
		this.cod_tipo_mantenimiento = cod_tipo_mantenimiento;
	}

	public String getNumero_orden_trabajo() {
		return numero_orden_trabajo;
	}

	public void setNumero_orden_trabajo(String numero_orden_trabajo) {
		this.numero_orden_trabajo = numero_orden_trabajo;
	}

	public Date getFecha_emicion() {
		return fecha_emicion;
	}

	public int getCod_atentido() {
		return cod_atentido;
	}

	public void setCod_atentido(int cod_atentido) {
		this.cod_atentido = cod_atentido;
	}

	public void setFecha_emicion(Date fecha_emicion) {
		this.fecha_emicion = fecha_emicion;
	}

	public String getUbicacion_activo() {
		return ubicacion_activo;
	}

	public void setUbicacion_activo(String ubicacion_activo) {
		this.ubicacion_activo = ubicacion_activo;
	}

	public int getCod_estado() {
		return cod_estado;
	}

	public void setCod_estado(int cod_estado) {
		this.cod_estado = cod_estado;
	}
	
	
}
