package com.proyfinal.taller.repo;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import com.proyfinal.taller.model.recepciones_activosModel;
import com.proyfinal.taller.model.tecnicos_ejecutantesModel;

public interface tecnicos_ejecutantesRepo extends JpaRepository<tecnicos_ejecutantesModel, Integer> {
	// busca por trabajo de servicio
	@Query(value = "select * from tecnicos_ejecutantes where seq_orden_trabajo =:xcod", nativeQuery = true)
	List<tecnicos_ejecutantesModel> Sacar(@Param("xcod") int xcod);

	/*-----------para eliminar y abilitar cod estado de fallas----------*/
	@Modifying
	@Query(value = "update tecnicos_ejecutantesModel u set u.cod_estado=?1 where u.seq_tecnicos_ejecutantes=?2")
	public int EliminarHabilitarEstado(@Param("cod_estado") int cod_estado,
			@Param("seq_tecnicos_ejecutantes") int seq_tecnicos_ejecutantes);

}
