package com.proyfinal.taller;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.HttpMethod;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.http.SessionCreationPolicy;
import org.springframework.security.web.SecurityFilterChain;
import org.springframework.security.web.authentication.UsernamePasswordAuthenticationFilter;

import com.proyfinal.taller.config.JWTAuthorizationFilter;

@SpringBootApplication
public class TallerApplication {

	public static void main(String[] args) {
		SpringApplication.run(TallerApplication.class, args);
	}
	@Configuration 
	@EnableWebSecurity
	public class WebSecurityConfig {
		@Value("${jwt.secret.key}")
		private String secret;
		
	   @Bean
	   public SecurityFilterChain filterChain(HttpSecurity http) throws Exception { 
	      http.cors().and().csrf().disable()
	      	.addFilterAfter(new JWTAuthorizationFilter(secret), UsernamePasswordAuthenticationFilter.class)
	        .sessionManagement().sessionCreationPolicy(SessionCreationPolicy.STATELESS)
	        .and().authorizeRequests() 
	        .requestMatchers(HttpMethod.POST, "/api/login").permitAll()
	        .requestMatchers(HttpMethod.GET, "/api/lisusu").permitAll()
	        .requestMatchers(HttpMethod.GET, "/{imageName:.+}").permitAll()
	        .anyRequest().authenticated();
	      return http.build();
	   }
	}
}
