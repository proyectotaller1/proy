package com.proyfinal.taller.model;

import java.sql.Date;
import java.util.Set;

import jakarta.persistence.CascadeType;
import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import jakarta.persistence.JoinColumn;
import jakarta.persistence.ManyToOne;
import jakarta.persistence.OneToMany;
import jakarta.persistence.Table;

@Entity
@Table(name="mantenimiento_correctivos")
public class mantenimiento_correctivosModel {
	
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Id
	@Column(name="seq_mantenimiento_correctivo")
	int seq_mantenimiento_correctivo;
	
	@Column(name="fecha_finalizacion")
	Date fecha_finalizacion;
	
	@Column(name="comentario_deteccion_falla")
	String comentario_deteccion_falla;
	
	@Column(name="descripcion_causa")
	String descripcion_causa;
	
	@Column(name="cod_tipo_solucion")
	String cod_tipo_solucion;

	@Column(name="descripcion_solucion")
	String descripcion_solucion;
	
	@Column(name="duracion")
	String duracion;
	
	@Column(name="comentarios ")
	String comentarios ;
	
	@Column(name="cod_estado")
	int cod_estado;

	
	//ralacion de muchos a uno con fallas
	@ManyToOne
	@JoinColumn(name = "id_falla")
	FallasModel fallas;
	
	//relacion de muchos a uno con orden de trabajo
	@ManyToOne
	@JoinColumn(name = "seq_orden_trabajo")
	ordenes_trabajoModel ordenes_trabajo;
	
	
	
	public FallasModel getFallas() {
		return fallas;
	}

	public void setFallas(FallasModel fallas) {
		this.fallas = fallas;
	}

	public ordenes_trabajoModel getOrdenes_trabajo() {
		return ordenes_trabajo;
	}

	public void setOrdenes_trabajo(ordenes_trabajoModel ordenes_trabajo) {
		this.ordenes_trabajo = ordenes_trabajo;
	}

	public int getSeq_mantenimiento_correctivo() {
		return seq_mantenimiento_correctivo;
	}

	public void setSeq_mantenimiento_correctivo(int seq_mantenimiento_correctivo) {
		this.seq_mantenimiento_correctivo = seq_mantenimiento_correctivo;
	}

	public Date getFecha_finalizacion() {
		return fecha_finalizacion;
	}

	public void setFecha_finalizacion(Date fecha_finalizacion) {
		this.fecha_finalizacion = fecha_finalizacion;
	}

	public String getComentario_deteccion_falla() {
		return comentario_deteccion_falla;
	}

	public void setComentario_deteccion_falla(String comentario_deteccion_falla) {
		this.comentario_deteccion_falla = comentario_deteccion_falla;
	}

	public String getDescripcion_causa() {
		return descripcion_causa;
	}

	public void setDescripcion_causa(String descripcion_causa) {
		this.descripcion_causa = descripcion_causa;
	}

	public String getCod_tipo_solucion() {
		return cod_tipo_solucion;
	}

	public void setCod_tipo_solucion(String cod_tipo_solucion) {
		this.cod_tipo_solucion = cod_tipo_solucion;
	}

	public String getDescripcion_solucion() {
		return descripcion_solucion;
	}

	public void setDescripcion_solucion(String descripcion_solucion) {
		this.descripcion_solucion = descripcion_solucion;
	}

	public String getDuracion() {
		return duracion;
	}

	public void setDuracion(String duracion) {
		this.duracion = duracion;
	}

	public String getComentarios() {
		return comentarios;
	}

	public void setComentarios(String comentarios) {
		this.comentarios = comentarios;
	}

	public int getCod_estado() {
		return cod_estado;
	}

	public void setCod_estado(int cod_estado) {
		this.cod_estado = cod_estado;
	}
	
	

}

