package com.proyfinal.taller.repo;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import com.proyfinal.taller.model.ActivosModel;
import com.proyfinal.taller.model.FallasModel;

public interface ActivosRepo extends JpaRepository<ActivosModel,Integer>{
	
	/*----- listar activo, inactivo y todos segun su estado---------*/
	 @Query(value="select u from ActivosModel u "
	  		  + "where u.cod_estado between :xest1 and :xest2 order by u.nombre")
	    List<ActivosModel> ListarEstadoActivo(
	  	@Param("xest1") int xest1,
	  	@Param("xest2") int xest2);
	 
	 
	 /*-----------para eliminar y abilitar cod estado de fallas----------*/
	    @Modifying
	    @Query(value = "update ActivosModel u set u.cod_estado=?1 where u.seq_activo=?2")
	    public int EliminarHabilitarEstado(@Param("cod_estado") int cod_estado, @Param("seq_activo") int seq_activo); 

}
