package com.proyfinal.taller.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.proyfinal.taller.model.FallasModel;
import com.proyfinal.taller.model.ordenes_trabajoModel;
import com.proyfinal.taller.model.recepciones_activosModel;
import com.proyfinal.taller.model.tecnicos_ejecutantesModel;
import com.proyfinal.taller.repo.tecnicos_ejecutantesRepo;

import jakarta.transaction.Transactional;

@RestController

public class tecnicos_ejecutantesController {

	@Autowired
	public tecnicos_ejecutantesRepo repotecnico;

	// listar fallas
	@GetMapping("/api/listecnico")
	public List<tecnicos_ejecutantesModel> lisdeTecnicos() {
		return repotecnico.findAll();
	}
	// adicionar tecnico

	@PostMapping("/api/addtec")
	public void guardarTecnico(@RequestBody tecnicos_ejecutantesModel xfall) {
		repotecnico.save(xfall);
	}

	// Lista por solicitud de servicio
	@GetMapping("/api/ListartenicoActivo/{xcod}")
	public List<tecnicos_ejecutantesModel> listaRecepcionActivo(@PathVariable int xcod) {
		return repotecnico.Sacar(xcod);
	}

	/*-----------para eliminar y abilitar cod estado de fallas----------*/
	@Transactional
	@PutMapping("/api/modOrdenEst/{xcod}")
	public int ModEstadoFallas(@RequestBody tecnicos_ejecutantesModel xfall) {
		if (xfall.getCod_estado() == 0)
			return repotecnico.EliminarHabilitarEstado(1, xfall.getSeq_tecnicos_ejecutantes());
		else
			return repotecnico.EliminarHabilitarEstado(0, xfall.getSeq_tecnicos_ejecutantes());
	}

}
