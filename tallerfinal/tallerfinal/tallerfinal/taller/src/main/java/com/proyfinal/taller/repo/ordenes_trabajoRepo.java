package com.proyfinal.taller.repo;


import java.sql.Date;
import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import com.proyfinal.taller.model.mantenimiento_correctivosModel;
import com.proyfinal.taller.model.ordenes_trabajoModel;
import com.proyfinal.taller.model.solicitudes_servicioModel;

public interface ordenes_trabajoRepo extends JpaRepository<ordenes_trabajoModel,Integer> {
	

	/*----- listar activo, inactivo y todos segun su estado---------*/
	 @Query(value="select u from ordenes_trabajoModel u "
	  		  + "where u.cod_estado between :xest1 and :xest2 order by u.ubicacion_activo")
	    List<ordenes_trabajoModel> ListarEstadox(
	  	@Param("xest1") int xest1,
	  	@Param("xest2") int xest2);
	 
	 /*-----------para eliminar y abilitar cod estado de fallas----------*/
	    @Modifying
	    @Query(value = "update ordenes_trabajoModel u set u.cod_estado=?1 where u.seq_orden_trabajo=?2")
	    public int EliminarHabilitarEstado(@Param("cod_estado") int cod_estado, @Param("seq_orden_trabajo") int seq_orden_trabajo); 

	    /*----aqui generar para filtrar fecha desde y hasta --*/
	    @Query("SELECT u FROM ordenes_trabajoModel u WHERE u.fecha_emicion BETWEEN :fechaDesde AND :fechaHasta")
	    List<ordenes_trabajoModel> ListarPorFecha1(
	        @Param("fechaDesde") Date fechaDesde,
	        @Param("fechaHasta") Date fechaHasta
	    );

}
