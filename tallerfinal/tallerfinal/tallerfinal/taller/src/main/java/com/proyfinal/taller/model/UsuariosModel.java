package com.proyfinal.taller.model;

import java.util.Set;

import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.Id;
import jakarta.persistence.JoinColumn;
import jakarta.persistence.ManyToOne;
import jakarta.persistence.OneToMany;
import jakarta.persistence.OneToOne;
import jakarta.persistence.Table;

@Entity
@Table(name="usuarios")
public class UsuariosModel {
	
	@Id
	@Column(name="usuario")
	String usuario;
	
	
	@Column(name="contrasena")
	String contrasena;
	
	@Column(name="cod_estado")
	int cod_estado;
	
      String token;
	

	//relacion de uno a uno con persona
   
      @OneToOne
      	@JoinColumn(name="seq_persona")
      	private PersonasModel personas;

	

	public PersonasModel getPersonas() {
		return personas;
	}



	public void setPersonas(PersonasModel personas) {
		this.personas = personas;
	}



	@OneToMany(mappedBy = "usuarios")
	Set<Usuarios_RolesModel> usuarios_roles;



	public String getUsuario() {
		return usuario;
	}



	public void setUsuario(String usuario) {
		this.usuario = usuario;
	}
	public String getContrasena() {
		return contrasena;
	}



	public void setContrasena(String contrasena) {
		this.contrasena = contrasena;
	}



	public int getCod_estado() {
		return cod_estado;
	}



	public void setCod_estado(int cod_estado) {
		this.cod_estado = cod_estado;
	}



	public String getToken() {
		return token;
	}



	public void setToken(String token) {
		this.token = token;
	}


}
