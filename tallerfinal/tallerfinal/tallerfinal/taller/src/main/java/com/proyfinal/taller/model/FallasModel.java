package com.proyfinal.taller.model;

import java.util.Set;

import jakarta.persistence.CascadeType;
import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.Id;
import jakarta.persistence.OneToMany;
import jakarta.persistence.Table;

@Entity
@Table(name="fallas")
public class FallasModel {
	@Id
	@Column(name="id_falla")
	int id_falla;
	
	@Column(name="nombre")
	String nombre;
	
	@Column(name="cod_estado")
	int cod_estado;
	
	 //ralacion de Uno a Muchos(mantenimiento correctivo)
	@OneToMany(mappedBy="fallas", cascade = CascadeType.REMOVE)
		private Set<mantenimiento_correctivosModel> mantenimiento_correctivos;

	public int getId_falla() {
		return id_falla;
	}

	public void setId_falla(int id_falla) {
		this.id_falla = id_falla;
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public int getCod_estado() {
		return cod_estado;
	}

	public void setCod_estado(int cod_estado) {
		this.cod_estado = cod_estado;
	}

	
}
