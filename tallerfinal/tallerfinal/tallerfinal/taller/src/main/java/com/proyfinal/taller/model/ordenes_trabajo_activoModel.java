package com.proyfinal.taller.model;

import java.sql.Date;

import jakarta.persistence.Column;
import jakarta.persistence.EmbeddedId;
import jakarta.persistence.Entity;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import jakarta.persistence.JoinColumn;
import jakarta.persistence.ManyToOne;
import jakarta.persistence.MapsId;
import jakarta.persistence.Table;

@Entity
@Table(name="ordenes_trabajo_activo")
public class ordenes_trabajo_activoModel {
	@EmbeddedId
	private ordenes_trabajo_activoModelPK id_ordenes_trabajo;
	//metodo que relaciona con activos model
	@ManyToOne
	@MapsId("seq_activo")
	@JoinColumn(name = "seq_activo")
	ActivosModel activos;
	
	//metodo que relaciona con orden de trabajo
	@ManyToOne
	@MapsId("seq_orden_trabajo")
	@JoinColumn(name = "seq_orden_trabajo")
	ordenes_trabajoModel ordenes_trabajo;

	public ordenes_trabajo_activoModelPK getId_ordenes_trabajo() {
		return id_ordenes_trabajo;
	}

	public void setId_ordenes_trabajo(ordenes_trabajo_activoModelPK id_ordenes_trabajo) {
		this.id_ordenes_trabajo = id_ordenes_trabajo;
	}

	public ActivosModel getActivos() {
		return activos;
	}

	public void setActivos(ActivosModel activos) {
		this.activos = activos;
	}

	public ordenes_trabajoModel getOrdenes_trabajo() {
		return ordenes_trabajo;
	}

	public void setOrdenes_trabajo(ordenes_trabajoModel ordenes_trabajo) {
		this.ordenes_trabajo = ordenes_trabajo;
	}
	
}

