package com.proyfinal.taller.repo;

import org.springframework.data.jpa.repository.JpaRepository;

import com.proyfinal.taller.model.RolesModel;
import com.proyfinal.taller.model.UsuariosModel;
import com.proyfinal.taller.model.Usuarios_RolesModel;
import com.proyfinal.taller.model.Usuarios_RolesModelPK;

public interface Usuarios_RolesRepo extends JpaRepository< Usuarios_RolesModel,Usuarios_RolesModelPK> {


}
