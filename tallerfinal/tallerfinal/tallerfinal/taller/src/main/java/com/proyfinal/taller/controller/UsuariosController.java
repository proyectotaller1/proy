package com.proyfinal.taller.controller;

import java.sql.Date;
import java.util.List;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.AuthorityUtils;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.proyfinal.taller.model.PersonasModel;
import com.proyfinal.taller.model.UsuariosModel;
import com.proyfinal.taller.repo.UsuariosRepo;

import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;

@RestController
public class UsuariosController {
	@Value("${jwt.secret.key}")
	String secretKey;
	@Value("${jwt.time.expiration}")
	String timeExpiration;

	
	@Autowired
	public UsuariosRepo lisus;
	
	
	/*--------------listar usuario ------*/
	@GetMapping("/api/lisusu")
	public List<UsuariosModel> lisdeUsuarios(){
	return lisus.findAll();
	}
	/*--------------sacar su clave primearia de persona para adicionar y modificar usuario ------*/
	@GetMapping("/api/datoPer/{xdato}")
	public UsuariosModel sacardato(@PathVariable int xdato){
		List<UsuariosModel> usuList=lisus.findAll();
		for(UsuariosModel usu: usuList) {
			if(usu.getPersonas().getSeq_persona()==xdato)return usu;
		}
	return null;
	}
	
	/*------------adicionar usuario ------*/

	@PostMapping("/api/addusu")
	public void guardarusuario(@RequestBody UsuariosModel xusu) {
		System.out.println("usuario: "+xusu.toString());
		System.out.println("seqpersona: "+xusu.getPersonas().getSeq_persona());
		System.out.println("persona: "+xusu.getPersonas().toString());
        lisus.save(xusu);
	}
	/*--------------modificar usuario----------------------*/
	@PutMapping("/api/modusu/{xcod}")
	public void updateSystemUsers(@PathVariable String xcod, @RequestBody UsuariosModel xusu) {
		xusu.setUsuario(xcod);
		lisus.save(xusu);
	}
	
	/*------------------------para logearser--------------------*/
	@CrossOrigin(origins = "http://localhost:8100")
	@PostMapping("/api/login")
	public UsuariosModel acceso(@RequestParam("usuario") String xlogin, @RequestParam("contrasena") String xpass ) {
		UsuariosModel user= new UsuariosModel();
		user = lisus.verificarCuentaUsuario(xlogin, xpass);		
		if (user != null) {
			try {
				String xtoken = getJWTToken(xlogin);
				System.out.println("este es mi TOKEN generado::"+ xtoken);
				user.setToken(xtoken);
			} catch (Exception e) {
				user = null;
			}
		}else {
			System.out.println("ACCESO NO PERMITIDO...");
		}
		return user;
	}
	/*
	
	@PostMapping("/api/validar")
	public UsuariosModel login(@RequestParam("usuario") String usuario, @RequestParam("contrasena") String contrasena) {
		System.out.println(usuario);
		System.out.println(contrasena);
		UsuariosModel devuelve = lisus.verificar(usuario, contrasena);
		return devuelve;

		
	} */
	private String getJWTToken(String username) {
		List<GrantedAuthority> grantedAuthorities = AuthorityUtils
				.commaSeparatedStringToAuthorityList("ROLE_USER");
		
		String token = Jwts
				.builder()
				.setId("softtekJWT")
				.setSubject(username)
				.claim("authorities",
						grantedAuthorities.stream()
								.map(GrantedAuthority::getAuthority)
								.collect(Collectors.toList()))
				.setIssuedAt(new Date(System.currentTimeMillis()))
				.setExpiration(new Date(System.currentTimeMillis() +Long.parseLong(timeExpiration)))
				//.setExpiration(new Date(System.currentTimeMillis() + 600000 ))
				.signWith(SignatureAlgorithm.HS512,
						secretKey.getBytes()).compact();
		return "Bearer " + token;
	}
	

	
}
