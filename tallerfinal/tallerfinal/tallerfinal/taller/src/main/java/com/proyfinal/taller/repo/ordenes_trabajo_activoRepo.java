package com.proyfinal.taller.repo;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import com.proyfinal.taller.model.ordenes_trabajo_activoModel;
import com.proyfinal.taller.model.ordenes_trabajo_activoModelPK;
import com.proyfinal.taller.model.tecnicos_ejecutantesModel;

public interface ordenes_trabajo_activoRepo extends JpaRepository<ordenes_trabajo_activoModel, ordenes_trabajo_activoModelPK> {

	// busca por trabajo de servicio
	@Query(value = "select * from ordenes_trabajo_activo where seq_orden_trabajo =:xcod", nativeQuery = true)
	List<ordenes_trabajo_activoModel> Sacar1(@Param("xcod") int xcod);
	
	@Modifying
	@Query(value = "insert into ordenes_trabajo_activo(seq_activo,seq_orden_trabajo) values (:activo,:orden)", nativeQuery = true)
	public int guardarOrden (@Param("orden") int orden, @Param("activo") int activo);
	
	@Modifying
	@Query("update ActivosModel a set a.cod_estado = :cod_estado where a.seq_activo = :seq_activo")
	int eliminarHabilitarEstado(@Param("cod_estado") int cod_estado, @Param("seq_activo") int seq_activo);



}
