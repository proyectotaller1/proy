package com.proyfinal.taller.model;

import jakarta.persistence.EmbeddedId;
import jakarta.persistence.Entity;
import jakarta.persistence.JoinColumn;
import jakarta.persistence.ManyToOne;
import jakarta.persistence.MapsId;
import jakarta.persistence.Table;

@Entity
@Table(name="usuarios_roles")
public class Usuarios_RolesModel {
	
	@EmbeddedId
	private Usuarios_RolesModelPK id_usuarios_roles;
	@ManyToOne
	@MapsId("usuario")
	@JoinColumn(name = "usuario")
	UsuariosModel usuarios;
	
	@ManyToOne
	@MapsId("id_rol")
	@JoinColumn(name = "id_rol")
	RolesModel roles;
	
	public UsuariosModel getUsuarios() {
		return usuarios;
	}

	public void setUsuarios(UsuariosModel usuarios) {
		this.usuarios = usuarios;
	}

	public RolesModel getRoles() {
		return roles;
	}

	public void setRoles(RolesModel roles) {
		this.roles = roles;
	}

	public Usuarios_RolesModelPK getId_usuarios_roles() {
		return id_usuarios_roles;
	}

	public void setId_usuarios_roles(Usuarios_RolesModelPK id_usuarios_roles) {
		this.id_usuarios_roles = id_usuarios_roles;
	}
	
	
	
	
	}


