package com.proyfinal.taller.controller;

import java.sql.Date;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.proyfinal.taller.model.FallasModel;
import com.proyfinal.taller.model.mantenimiento_correctivosModel;
import com.proyfinal.taller.model.ordenes_trabajoModel;
import com.proyfinal.taller.model.solicitudes_servicioModel;
import com.proyfinal.taller.repo.ordenes_trabajoRepo;

import jakarta.transaction.Transactional;

@RestController
public class ordenes_trabajoController {
	@Autowired
	public ordenes_trabajoRepo repotrabajo;

	// listar fallas
	@GetMapping("/api/listrabajo")
	public List<ordenes_trabajoModel> lisdeTrabajos() {
		return repotrabajo.findAll();
	}

	// adicionar fallas

	@PostMapping("/api/addtrab")
	public void guardarFalla(@RequestBody ordenes_trabajoModel xfall) {
		repotrabajo.save(xfall);
	}

	// modificar Fallas
	@PutMapping("/api/modtrab/{xcod}")
	public void updateSystemUsers(@PathVariable Integer xcod, @RequestBody ordenes_trabajoModel xfall) {
		xfall.setSeq_orden_trabajo(xcod);
		repotrabajo.save(xfall);
	}
	// Nuevo método para obtener la información completa de trabajo
    @GetMapping("/api/ObtenerCompleta/{seqSolicitud}")
    public ResponseEntity<ordenes_trabajoModel> obtenerSolicitudCompleta(@PathVariable int seqSolicitud) {
        Optional<ordenes_trabajoModel> solicitud = repotrabajo.findById(seqSolicitud);
        return solicitud.map(ResponseEntity::ok).orElseGet(() -> ResponseEntity.notFound().build());
    }
    
    /*----- listar activo, inactivo y todos segun su estado---------*/
    /*----- listar activo, inactivo y todos segun su estado---------*/
   	@GetMapping("/api/filtrotrabajo/{xestado}")
   	public List<ordenes_trabajoModel> listaFallas_jpa(@PathVariable int xestado) {
   		int xest1 = 0, xest2 = 0;
   		if (xestado == 1) {
   			xest1 = 1;
   			xest2 = 1;
   		}
   		if (xestado == 2) {
   			xest2 = 1;
   		}
   		return repotrabajo.ListarEstadox(xest1, xest2);
   	}

	/*-----------para eliminar y abilitar cod estado de fallas----------*/
	@Transactional
	@PutMapping("/api/modTraEst/{xcod}")
	public int ModEstadoFallas(@RequestBody ordenes_trabajoModel xfall) {
		if (xfall.getCod_estado() == 0)
			return repotrabajo.EliminarHabilitarEstado(1, xfall.getSeq_orden_trabajo());
		else
			return repotrabajo.EliminarHabilitarEstado(0, xfall.getSeq_orden_trabajo());
	}

	/*--------------aqui filtrar la fecha desde y hasta----*/
	@GetMapping("/api/listtra/fecha_emicion")
	public List<ordenes_trabajoModel> listaFallasPorFecha(
	        @RequestParam Date fechaDesde,
	        @RequestParam Date fechaHasta
	) {
	    return repotrabajo.ListarPorFecha1(fechaDesde, fechaHasta);
	}



}
