package com.proyfinal.taller.model;

import java.sql.Date;

import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import jakarta.persistence.JoinColumn;
import jakarta.persistence.ManyToOne;
import jakarta.persistence.Table;

@Entity
@Table(name = "recepciones_activos")
public class recepciones_activosModel {
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	
	@Id
	@Column(name = "seq_recepcion_activo")
	int seq_recepcion_activo;
	
	@Column(name = "fecha")
	Date fecha;
	
	@Column(name = "cod_estado")
	int cod_estado;
	
	/*relacion de muchos a uno con persona*/
	@ManyToOne 
	@JoinColumn(name="seq_persona")
	PersonasModel personas;
	
    /*relacion de muchos a uno con activos*/
	@ManyToOne 
	@JoinColumn(name="seq_activo")
	ActivosModel activos;
	
	/*relacion de muchos a uno con solicitudes de servicio*/
	@ManyToOne 
	@JoinColumn(name="seq_solicitud_servicio")
	solicitudes_servicioModel solicitudes_servicio;

	public int getSeq_recepcion_activo() {
		return seq_recepcion_activo;
	}

	public void setSeq_recepcion_activo(int seq_recepcion_activo) {
		this.seq_recepcion_activo = seq_recepcion_activo;
	}

	public Date getFecha() {
		return fecha;
	}

	public void setFecha(Date fecha) {
		this.fecha = fecha;
	}

	public int getCod_estado() {
		return cod_estado;
	}

	public void setCod_estado(int cod_estado) {
		this.cod_estado = cod_estado;
	}

	public PersonasModel getPersonas() {
		return personas;
	}

	public void setPersonas(PersonasModel personas) {
		this.personas = personas;
	}

	public ActivosModel getActivos() {
		return activos;
	}

	public void setActivos(ActivosModel activos) {
		this.activos = activos;
	}

	public solicitudes_servicioModel getSolicitudes_servicio() {
		return solicitudes_servicio;
	}

	public void setSolicitudes_servicio(solicitudes_servicioModel solicitudes_servicio) {
		this.solicitudes_servicio = solicitudes_servicio;
	}


	
}
