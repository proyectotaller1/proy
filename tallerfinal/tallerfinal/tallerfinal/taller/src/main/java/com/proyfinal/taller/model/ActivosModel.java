package com.proyfinal.taller.model;

import java.util.Set;

import jakarta.persistence.CascadeType;
import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import jakarta.persistence.OneToMany;
import jakarta.persistence.Table;

@Entity
@Table(name = "activos")
public class ActivosModel {
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Id
	@Column(name = "seq_activo")
	int seq_activo;
	@Column(name = "id_etiqueta_fisica")
	String id_etiqueta_fisica;

	@Column(name = "nombre")
	String nombre;

	@Column(name = "marca")
	String marca;

	@Column(name = "modelo")
	String modelo;

	@Column(name = "comentarios")
	String comentarios;

	@Column(name = "cod_estado")
	int cod_estado;
	
	/*---relacion de unos a muchos con recepeciones activos*/
	@OneToMany(mappedBy="activos", cascade = CascadeType.REMOVE)
	private Set<recepciones_activosModel> recepciones_activos;

	//ralacion de mucho a muchos con orden trab activo
		@OneToMany(mappedBy = "activos")
		Set<ordenes_trabajo_activoModel> ordenes_trabajo_activo;
		
	public int getSeq_activo() {
		return seq_activo;
	}

	public void setSeq_activo(int seq_activo) {
		this.seq_activo = seq_activo;
	}

	public String getId_etiqueta_fisica() {
		return id_etiqueta_fisica;
	}

	public void setId_etiqueta_fisica(String id_etiqueta_fisica) {
		this.id_etiqueta_fisica = id_etiqueta_fisica;
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public String getMarca() {
		return marca;
	}

	public void setMarca(String marca) {
		this.marca = marca;
	}

	public String getModelo() {
		return modelo;
	}

	public void setModelo(String modelo) {
		this.modelo = modelo;
	}

	public String getComentarios() {
		return comentarios;
	}

	public void setComentarios(String comentarios) {
		this.comentarios = comentarios;
	}

	public int getCod_estado() {
		return cod_estado;
	}

	public void setCod_estado(int cod_estado) {
		this.cod_estado = cod_estado;
	}
	
	

}
