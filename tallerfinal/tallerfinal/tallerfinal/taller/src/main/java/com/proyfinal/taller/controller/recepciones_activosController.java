package com.proyfinal.taller.controller;

import java.util.Collections;
import java.util.List;
import java.util.Optional;
import java.util.Set;
import java.util.ArrayList;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;
import com.proyfinal.taller.model.ActivosModel;
import com.proyfinal.taller.model.FallasModel;
import com.proyfinal.taller.model.recepciones_activosModel;
import com.proyfinal.taller.model.solicitudes_servicioModel;
import com.proyfinal.taller.repo.recepciones_activosRepo;
import com.proyfinal.taller.repo.solicitudes_servicioRepo;

import jakarta.transaction.Transactional;

@RestController
public class recepciones_activosController {
	
	@Autowired
	public recepciones_activosRepo reporece;
	
	 @Autowired
	    public solicitudes_servicioRepo reposolicitud;
	
	/*listar */
	@GetMapping("/api/lisrece")
	public List<recepciones_activosModel> lisdeRecepciones(){
	return reporece.findAll();
	}
	//Lista por solicitud de servicio
			@GetMapping("/api/ListarRecepcionActivos/{xcod}")
			public List<recepciones_activosModel> listaRecepcionActivo(@PathVariable int xcod){
				return reporece.SacarRecepcionActivo(xcod);
			}
	
			// Nuevo método para obtener la información completa de una solicitud
		    @GetMapping("/api/ObtenerSolicitudCompleta/{seqSolicitud}")
		    public ResponseEntity<solicitudes_servicioModel> obtenerSolicitudCompleta(@PathVariable int seqSolicitud) {
		        Optional<solicitudes_servicioModel> solicitud = reposolicitud.findById(seqSolicitud);
		        return solicitud.map(ResponseEntity::ok).orElseGet(() -> ResponseEntity.notFound().build());
		    }
		
	// adicionar recepciones activos
		@PostMapping("/api/addrece")
			public void guardarRecepciones(@RequestBody recepciones_activosModel xrece) {
			reporece.save(xrece);
			}
		//modificar recepciones activos
		@PutMapping("/api/modrece/{xcod}")
		public void updateSystemUsers(@PathVariable Integer xcod, @RequestBody recepciones_activosModel xfall) {
			xfall.setSeq_recepcion_activo(xcod);
			reporece.save(xfall);
		}
		
		/*-----------para eliminar y abilitar cod estado de recepciones activos----------*/
        @Transactional
		@PutMapping("/api/modReceEst/{xcod}")
		public int ModEstadoRecepciones(@RequestBody recepciones_activosModel xfall) {
			if(xfall.getCod_estado()==0)
				return reporece.EliminarHabilitarEstado(1, xfall.getSeq_recepcion_activo());
			else
				return reporece.EliminarHabilitarEstado(0, xfall.getSeq_recepcion_activo());
		}	

}
