package com.proyfinal.taller.controller;

import java.sql.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.proyfinal.taller.model.ActivosModel;
import com.proyfinal.taller.model.solicitudes_servicioModel;
import com.proyfinal.taller.repo.solicitudes_servicioRepo;

import jakarta.transaction.Transactional;

@RestController
public class solicitudes_servicioController {

	@Autowired
	public solicitudes_servicioRepo reposol;

	// listar solicitudes_servicio
	@GetMapping("/api/lissol")
	public List<solicitudes_servicioModel> lisdesolicitudes_servicio() {
		return reposol.findAll();
	}

	// adicionar solicitud
	@PostMapping("/api/addsol")
	public void guardarSolicitud(@RequestBody solicitudes_servicioModel xsol) {
		reposol.save(xsol);
	}

	// modificar solicitud
	@PutMapping("/api/modsol/{xcod}")
	public void updateSystemUsers(@PathVariable Integer xcod, @RequestBody solicitudes_servicioModel xfall) {
		xfall.setSeq_solicitud_servicio(xcod);
		reposol.save(xfall);
	}

	/*----- listar activo, inactivo y todos segun su estado---------*/
	@GetMapping("/api/listsol/{xestado}")
	public List<solicitudes_servicioModel> listaFallas_jpa(@PathVariable int xestado) {
		int xest1 = 0, xest2 = 0;
		if (xestado == 1) {
			xest1 = 1;
			xest2 = 1;
		}
		if (xestado == 2) {
			xest2 = 1;
		}
		return reposol.ListarEstadoSol(xest1, xest2);
	}

	/*-----------para eliminar y abilitar cod estado de fallas----------*/
	@Transactional
	@PutMapping("/api/modSolEst/{xcod}")
	public int ModEstadoFallas(@RequestBody solicitudes_servicioModel xfall) {
		if (xfall.getCod_estado() == 0)
			return reposol.EliminarHabilitarEstado(1, xfall.getSeq_solicitud_servicio());
		else
			return reposol.EliminarHabilitarEstado(0, xfall.getSeq_solicitud_servicio());
	}

	/*--------------aqui filtrar la fecha desde y hasta----*/
	@GetMapping("/api/listsol/fecha")
	public List<solicitudes_servicioModel> listaFallasPorFecha(
	        @RequestParam Date fechaDesde,
	        @RequestParam Date fechaHasta
	) {
	    return reposol.ListarPorFecha(fechaDesde, fechaHasta);
	}

  //listar persona segun su solicitud-------------
	@CrossOrigin(origins = "http://localhost:8100")
	@GetMapping("/api/listarPerSol/{seq}")
	public List<solicitudes_servicioModel> listaperosnaporsoli(@PathVariable int seq) {
		return reposol.listarpersonaporsoli(seq);
	}

}
