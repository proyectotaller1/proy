package com.proyfinal.taller.controller;


import java.sql.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.proyfinal.taller.model.FallasModel;
import com.proyfinal.taller.model.mantenimiento_correctivosModel;
import com.proyfinal.taller.model.solicitudes_servicioModel;
import com.proyfinal.taller.repo.mantenimiento_correctivosRepo;

import jakarta.transaction.Transactional;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
@RestController
public class mantenimiento_correctivosController {
	@Autowired
	public mantenimiento_correctivosRepo repoman;

	// listar mantenimientos
	@GetMapping("/api/lismant")
	public List<mantenimiento_correctivosModel> lisdemantenimientos() {
		return repoman.findAll();
	}
	
	//modificar Fallas
	private static final Logger logger = LoggerFactory.getLogger(mantenimiento_correctivosModel.class);

    @PutMapping("/api/modman/{xcod}")
    public void updateSystemUsers(@PathVariable Integer xcod, @RequestBody mantenimiento_correctivosModel xfall) {
        logger.info("Recibiendo datos para actualizar. xcod: {}, xfall: {}", xcod, xfall);
        xfall.setSeq_mantenimiento_correctivo(xcod);
        repoman.save(xfall);
        logger.info("Datos actualizados exitosamente. xcod: {}, xfall: {}", xcod, xfall);
    }
    
    /*----- listar activo, inactivo y todos segun su estado---------*/
	@GetMapping("/api/filtraractivoinac/{xestado}")
	public List<mantenimiento_correctivosModel> listaFallas_jpa(@PathVariable int xestado) {
		int xest1 = 0, xest2 = 0;
		if (xestado == 1) {
			xest1 = 1;
			xest2 = 1;
		}
		if (xestado == 2) {
			xest2 = 1;
		}
		return repoman.ListarEstadoSol(xest1, xest2);
	}

	/*-----------para eliminar y abilitar cod estado de fallas----------*/
	@Transactional
	@PutMapping("/api/modManEst/{xcod}")
	public int ModEstadoFallas(@RequestBody mantenimiento_correctivosModel xfall) {
		if (xfall.getCod_estado() == 0)
			return repoman.EliminarHabilitarEstado(1, xfall.getSeq_mantenimiento_correctivo());
		else
			return repoman.EliminarHabilitarEstado(0, xfall.getSeq_mantenimiento_correctivo());
	}

	/*--------------aqui filtrar la fecha desde y hasta----*/
	@GetMapping("/api/listfecha/fecha_finalizacion")
	public List<mantenimiento_correctivosModel> listaFallasPorFecha(
	        @RequestParam Date fechaDesde,
	        @RequestParam Date fechaHasta
	) {
	    return repoman.ListarPorFecha(fechaDesde, fechaHasta);
	}


}
