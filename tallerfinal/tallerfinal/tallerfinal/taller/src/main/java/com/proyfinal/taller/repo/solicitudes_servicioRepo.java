package com.proyfinal.taller.repo;

import java.sql.Date;
import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import com.proyfinal.taller.model.ActivosModel;
import com.proyfinal.taller.model.solicitudes_servicioModel;

public interface solicitudes_servicioRepo extends JpaRepository<solicitudes_servicioModel,Integer>  {
	
	/*----- listar activo, inactivo y todos segun su estado---------*/
	 @Query(value="select u from solicitudes_servicioModel u "
	  		  + "where u.cod_estado between :xest1 and :xest2 order by u.descripcion_problema")
	    List<solicitudes_servicioModel> ListarEstadoSol(
	  	@Param("xest1") int xest1,
	  	@Param("xest2") int xest2);
	 
	 
	 /*-----------para eliminar y abilitar cod estado de fallas----------*/
	    @Modifying
	    @Query(value = "update solicitudes_servicioModel u set u.cod_estado=?1 where u.seq_solicitud_servicio=?2")
	    public int EliminarHabilitarEstado(@Param("cod_estado") int cod_estado, @Param("seq_solicitud_servicio") int seq_solicitud_servicio); 

	/*----aqui generar para filtrar fecha desde y hasta --*/
	    @Query("SELECT u FROM solicitudes_servicioModel u WHERE u.fecha BETWEEN :fechaDesde AND :fechaHasta")
	    List<solicitudes_servicioModel> ListarPorFecha(
	        @Param("fechaDesde") Date fechaDesde,
	        @Param("fechaHasta") Date fechaHasta
	    );
 
	   @Query(value="select u FROM solicitudes_servicioModel u WHERE u.personas.seq_persona=?1")
	   List<solicitudes_servicioModel> listarpersonaporsoli(int seq);
	    
}
