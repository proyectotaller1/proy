package com.proyfinal.taller.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.proyfinal.taller.model.PersonasModel;
import com.proyfinal.taller.model. ordenes_trabajo_activoModel;
import com.proyfinal.taller.model.tecnicos_ejecutantesModel;
import com.proyfinal.taller.repo. ordenes_trabajo_activoRepo;

import jakarta.transaction.Transactional;

@RestController
public class ordenes_trabajo_activoController {

	@Autowired
	public  ordenes_trabajo_activoRepo repoorden;
	//listar fallas
		@GetMapping("/api/lisorden")
		public List<ordenes_trabajo_activoModel> lisdeOrden(){
		return repoorden.findAll();
		}
		
		// Lista por solicitud de servicio
		@GetMapping("/api/ListarOrdenActivo/{xcod}")
		public List<ordenes_trabajo_activoModel> listaRecepcionActivo(@PathVariable int xcod) {
			return repoorden.Sacar1(xcod);
		}
		
		@Transactional
		@PostMapping("/api/addtrabactivo")
		public void guardarOrdenTraActivo(@RequestBody ordenes_trabajo_activoModel xtra) {
		    System.out.println("Datos recibidos: " + xtra);
		    // Resto del código...
		    repoorden.guardarOrden(xtra.getOrdenes_trabajo().getSeq_orden_trabajo() , xtra.getActivos().getSeq_activo());
		}
		
		/*-----------para eliminar y abilitar cod estado de fallas----------*/
		
		@Transactional
		@PutMapping("/api/modOrdenAcEst/{xcod}")
		public int ModEstadoFallas(@RequestBody ordenes_trabajo_activoModel xfall) {
		    int nuevoEstado = (xfall.getActivos().getCod_estado() == 0) ? 1 : 0;
		    repoorden.eliminarHabilitarEstado(nuevoEstado, xfall.getActivos().getSeq_activo());
		    return 1;
		}
}
