package com.proyfinal.taller.model;

import java.io.Serializable;
import java.util.Objects;

import jakarta.persistence.Embeddable;
@Embeddable

public class Usuarios_RolesModelPK implements Serializable {
	protected int id_rol;
	protected String usuario;
	public int getId_rol() {
		return id_rol;
	}
	public void setId_rol(int id_rol) {
		this.id_rol = id_rol;
	}
	public String getUsuario() {
		return usuario;
	}
	public void setUsuario(String usuario) {
		this.usuario = usuario;
	}
	@Override
	public int hashCode() {
		return Objects.hash(id_rol, usuario);
	}
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Usuarios_RolesModelPK other = (Usuarios_RolesModelPK) obj;
		return id_rol == other.id_rol && Objects.equals(usuario, other.usuario);
	}
	
	
	
}