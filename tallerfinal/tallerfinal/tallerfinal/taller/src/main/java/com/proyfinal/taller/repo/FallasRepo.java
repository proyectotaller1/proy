package com.proyfinal.taller.repo;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import com.proyfinal.taller.model.FallasModel;


public interface FallasRepo extends JpaRepository<FallasModel,Integer> {
	
	/*----- listar activo, inactivo y todos segun su estado---------*/
	 @Query(value="select u from FallasModel u "
	  		  + "where u.cod_estado between :xest1 and :xest2 order by u.nombre")
	    List<FallasModel> ListarEstadoFalla(
	  	@Param("xest1") int xest1,
	  	@Param("xest2") int xest2);
	 
	 
	 /*-----------para eliminar y abilitar cod estado de fallas----------*/
	    @Modifying
	    @Query(value = "update FallasModel u set u.cod_estado=?1 where u.id_falla=?2")
	    public int EliminarHabilitarEstado(@Param("cod_estado") int cod_estado, @Param("id_falla") int id_falla);
	    
	    
}






