package com.proyfinal.taller.model;

import java.sql.Date;
import java.util.Set;

import jakarta.persistence.CascadeType;
import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import jakarta.persistence.JoinColumn;
import jakarta.persistence.ManyToOne;
import jakarta.persistence.OneToMany;
import jakarta.persistence.Table;

@Entity
@Table(name = "solicitudes_servicio")
public class solicitudes_servicioModel {
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Id
	@Column(name = "seq_solicitud_servicio") // la ID es autoIncrementativo
	int seq_solicitud_servicio;

	@Column(name = "fecha")
	Date fecha;

	@Column(name = "descripcion_problema")
	String descripcion_problema;

	@Column(name = "cod_estado")
	int cod_estado;

	// relacion de Muchos a Uno(Persona)
	@ManyToOne
	@JoinColumn(name = "seq_persona")
	PersonasModel personas;
    
	/*---relacion de unos a muchos con recepeciones activos*/
	@OneToMany(mappedBy="solicitudes_servicio", cascade = CascadeType.REMOVE)
	private Set<recepciones_activosModel> recepciones_activos;
	
	//ralacion de Uno a Muchos(ordenes de trabajo)
			@OneToMany(mappedBy="solicitudes_servicio", cascade = CascadeType.REMOVE)
				private Set<ordenes_trabajoModel> ordenes_trabajo;
	public int getSeq_solicitud_servicio() {
		return seq_solicitud_servicio;
	}

	public void setSeq_solicitud_servicio(int seq_solicitud_servicio) {
		this.seq_solicitud_servicio = seq_solicitud_servicio;
	}

	public Date getFecha() {
		return fecha;
	}

	public void setFecha(Date fecha) {
		this.fecha = fecha;
	}

	public String getDescripcion_problema() {
		return descripcion_problema;
	}

	public void setDescripcion_problema(String descripcion_problema) {
		this.descripcion_problema = descripcion_problema;
	}

	public int getCod_estado() {
		return cod_estado;
	}

	public void setCod_estado(int cod_estado) {
		this.cod_estado = cod_estado;
	}

	public PersonasModel getPersonas() {
		return personas;
	}

	public void setPersonas(PersonasModel personas) {
		this.personas = personas;
	}


}
