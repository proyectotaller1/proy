package com.proyfinal.taller.repo;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import com.proyfinal.taller.model.recepciones_activosModel;

public interface recepciones_activosRepo extends JpaRepository<recepciones_activosModel, Integer>{
	
	 /*-----------para eliminar y abilitar cod estado de recepciones activos----------*/
    @Modifying
    @Query(value = "update recepciones_activosModel u set u.cod_estado=?1 where u.seq_recepcion_activo=?2")
    public int EliminarHabilitarEstado(@Param("cod_estado") int cod_estado, @Param("seq_recepcion_activo") int seq_recepcion_activo);

  //busca por solicitud de servicio
  		@Query(value="select * from recepciones_activos where seq_solicitud_servicio =:xcod",nativeQuery = true)
  		List<recepciones_activosModel> SacarRecepcionActivo(@Param("xcod") int xcod);
  		
  		//Aumento de Llave primaria
  		@Query(value = "select max(p.seq_recepcion_activo) from recepciones_activos p ", nativeQuery = true)
  		public int maxRActivo();
  		
    
    
}
