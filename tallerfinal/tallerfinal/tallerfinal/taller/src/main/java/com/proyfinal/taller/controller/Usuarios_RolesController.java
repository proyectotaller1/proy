package com.proyfinal.taller.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.proyfinal.taller.model.RolesModel;
import com.proyfinal.taller.model.UsuariosModel;
import com.proyfinal.taller.model.Usuarios_RolesModel;
import com.proyfinal.taller.model.Usuarios_RolesModelPK;
import com.proyfinal.taller.repo.RolesRepo;
import com.proyfinal.taller.repo.UsuariosRepo;
import com.proyfinal.taller.repo.Usuarios_RolesRepo;

@RestController
public class Usuarios_RolesController {
	@Autowired
	public Usuarios_RolesRepo reporolusu;
	
	@Autowired
	private RolesRepo rolesRepo;

	@GetMapping("/api/lisusrol")
	public List<Usuarios_RolesModel> lisdeRolesUsuarios() {
		return reporolusu.findAll();
	}
	

	/*--------------Adicionar Rol a Usuario----------------------*/
	@PostMapping("/api/addcionarusurol/{xcod}")
	public void ModUsuRol(@PathVariable int xcod, @RequestBody UsuariosModel usuario) {
		RolesModel rol = this.rolesRepo.obtenerRol(xcod);
		Usuarios_RolesModelPK clave = new Usuarios_RolesModelPK();
		clave.setId_rol(xcod);
		clave.setUsuario(usuario.getUsuario());

		Usuarios_RolesModel usurol = new Usuarios_RolesModel();
		usurol.setId_usuarios_roles(clave);
		usurol.setRoles(rol);
		usurol.setUsuarios(usuario);
		reporolusu.save(usurol);
	}
	/*--------------Quitar Rol a Usuario----------------------*/
	@DeleteMapping("/api/eliminarRolUsuario/{xusurol}")
	public void eliminarRolUsuarios(@PathVariable String xusurol) {
		String usuario = "";
		int xcod = 0;
		String[] xmarcar = xusurol.split(" ");
		usuario = xmarcar[0];
		xcod = Integer.parseInt(xmarcar[1]);
		Usuarios_RolesModelPK Usuarios_RolesClave = new Usuarios_RolesModelPK();
		Usuarios_RolesClave.setId_rol(xcod);
		Usuarios_RolesClave.setUsuario(usuario);
		this.reporolusu.deleteById(Usuarios_RolesClave);
	}
}
