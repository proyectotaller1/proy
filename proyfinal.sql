create table personas(
seq_persona serial not null,
	cedula_identidad varchar(15),
	nombres varchar(40) not null,
	apellido_paterno varchar(40) not null,
	apellido_materno varchar(40),
	fotografia varchar(255),
	telefono_celular varchar(20),
	cod_estado integer default 1 not null,
	primary key(seq_persona)
	
);

create table usuarios(
usuario varchar(20) not null,
	contrasena varchar(200) not null,
	seq_persona integer not null,
	cod_estado integer default 1 not null,
	foreign key(seq_persona) references personas(seq_persona),
	primary key(usuario)
		
	);
	
create table roles(
id_rol integer not null primary key,
	nombre varchar(40) not null,
	cod_estado integer default 1 not null
);

create table usuarios_roles(
usuario varchar(20) not null,
    id_rol integer not null,
	foreign key(usuario) references usuarios(usuario),
	foreign key(id_rol) references roles(id_rol),
	primary key(usuario, id_rol)
);

select*from personas;
insert into personas values(111,'7569799','juan','martines','loza','eee1','67894512',1);
insert into personas values(222,'89988571','pedro','zuares','flores','eee2','7795855',1);
insert into personas values(333,'75686995','carmen','mendez','pereira','eee3','90847474',1);
insert into personas values(444,'10095881','diana','tapia','mamani','eee4','7675758',1);

select*from usuarios;
insert into usuarios values('userjuan','juan12345',111,1);
insert into usuarios values('usercarmen','carmen1002',333,1);

select*from roles;
insert into roles values(100, 'ADMI',1);
insert into roles values(101, 'TECN',1);
insert into roles values(102, 'CLIE',1);


select*from usuarios_roles;
insert into usuarios_roles values('userjuan',101);
insert into usuarios_roles values('usercarmen',100);