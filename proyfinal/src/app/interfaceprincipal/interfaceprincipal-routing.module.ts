import { NgModule } from '@angular/core';
import { HomeComponent } from './home/home.component';
import { RouterModule, Routes } from '@angular/router';
import { PieComponent } from './pie/pie.component';
import { MenuComponent } from './menu/menu.component';
import { CabeceraComponent } from './cabecera/cabecera.component';
import { ListarComponent } from './listar/listar.component';
import { ModulofallasComponent } from './modulofallas/modulofallas.component';
import { ModulousuariorolComponent } from './modulousuariorol/modulousuariorol.component';
import { ModuloactivosModule } from './moduloactivos/moduloactivos.module';
import { ModulosolicitudserModule } from './modulosolicitudser/modulosolicitudser.module';
import { ModulotrabajoModule } from './modulotrabajo/modulotrabajo.module';
import { ModulomantenimientoModule } from './modulomantenimiento/modulomantenimiento.module';
const routes: Routes = [
  {path:'', redirectTo:'principal', pathMatch:'full'},
  {path:'principal', component: HomeComponent},
  {path: 'listar',component: ListarComponent },
  {path: 'ModuloFallas',component: ModulofallasComponent },
  {path: 'asignacionRolUsuario', component: ModulousuariorolComponent},
  {path: 'moduloactivos', loadChildren: () => import('../interfaceprincipal/moduloactivos/moduloactivos.module').then((mod)=>ModuloactivosModule)},
  {path: 'modulosolicitud', loadChildren: () => import('../interfaceprincipal/modulosolicitudser/modulosolicitudser.module').then((mod)=>ModulosolicitudserModule)},
  {path: 'moduloOrden', loadChildren: () => import('../interfaceprincipal/modulotrabajo/modulotrabajo.module').then((mod)=>ModulotrabajoModule)},
  {path: 'moduloMan', loadChildren: () => import('../interfaceprincipal/modulomantenimiento/modulomantenimiento.module').then((mod)=>ModulomantenimientoModule)},
  /*
  {path:'xcabecera', component: CabeceraComponent,outlet:'cabecera'},
  {path:'xmenu', component: MenuComponent, outlet:'menu'},
  {path:'xpie', component: PieComponent, outlet:'pie'},*/
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class InterfaceprincipalRoutingModule { }
