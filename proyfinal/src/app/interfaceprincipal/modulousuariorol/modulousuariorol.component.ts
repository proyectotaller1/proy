import { Component, OnInit } from '@angular/core';
import { FormControl, Validators, FormGroup, NgModel } from '@angular/forms';
import { Observable, finalize } from 'rxjs';
import { Login } from 'src/app/model/login';
import { Roles } from 'src/app/model/roles';
import { HttpClient } from '@angular/common/http';
import { UsuarioService } from 'src/app/services/usuario.service';
import { RolService } from 'src/app/services/rol.service';
import { FormBuilder } from '@angular/forms'
import { error } from 'jquery';
@Component({
  selector: 'app-modulousuariorol',
  templateUrl: './modulousuariorol.component.html',
  styleUrls: ['./modulousuariorol.component.css']
})
export class ModulousuariorolComponent implements OnInit {

   /*---------metodo para asignados/no asignados/todos  _*/
   generarmarca(marcar: number) {
    this.marcar = marcar;
  }
  /*----para usuarios y persona ---*/
  pag: number = 1;
  buscar = '';
  marcar: number = -1;
  usuperForm: any;
  ListaUsuPer: Login[] = [];
  xusurol!: any;
  visualizar: string = 'todos'


  /*-------para roles----*/

  constructor(private usurioService: UsuarioService, private formBuilder: FormBuilder, private rolservice: RolService) {
    this.usuperForm = this.formBuilder.group({
      usuario: new FormControl(''),
      contrasena: new FormControl(''),
      cod_estado: new FormControl(''),
      personas: this.formBuilder.group({
        nombres: new FormControl(''),
        apellido_paterno: new FormControl(''),
        apellido_materno: new FormControl('')
      })
    });
  }

  get fu() {
    return this.usuperForm.controls
  }
  ListarUsuPer() {
    this.usurioService.getListUserPer().subscribe((usuper) => (this.ListaUsuPer = usuper))
  }

  cambiarFiltro(nombreRol: string) { }

  /*------------listar rol--------------*/
  ListaRol: Roles[] = [];
  rolForm: any = new FormGroup({
    id_rol: new FormControl(''),
    nombre: new FormControl(''),
    cod_estado: new FormControl(''),
  })
  ListarRol() {
    this.rolservice.getListaRol().subscribe((xxrol) => (this.ListaRol = xxrol))
  }
  ngOnInit(): void {
    this.ListarUsuPer()
    this.ListarRol()

  }
  /*------------------------asignar roles a usuario----------------*/
  xpostUsu: boolean = false
  seleccionarUsuario: any = null;

  xasignar(usu: any) { //marcar
    //this.seleccionarUsuario = usu;
    console.log("se selecciono rol para usuario" + usu.usuario);
    if (this.seleccionarUsuario != usu) {
      this.xpostUsu = true
      this.seleccionarUsuario = usu
    } else {
      this.xpostUsu = !this.xpostUsu
      this.seleccionarUsuario = (!this.xpostUsu) ? null : this.seleccionarUsuario
    }
    this.rolservice.getObtenerRolUsuario(usu.usuario).subscribe((xrolusu) => {
      this.rolseleccionado = xrolusu
    })
  }
  rolseleccionado: any[] = []

  //roles segun el usuario---
  rolSegunUser: { [xusuario: string]: string } = {};
  subirRol(xusu: Login): void {
    this.rolservice.getObtenerRolUsuario(xusu.usuario).subscribe(
      (xrolusu) => {
        this.rolSegunUser[xusu.usuario] = xrolusu.map(xrol => xrol.nombres.charAt(0).toUpperCase()).join('')

      },
      (error) => {
        console.log("nose puede cargar roles del usuario");

      }
    );
  }
  rolencontrado(rol: Roles): boolean {
    for (let i = 0; i < this.rolseleccionado.length; i++)
      if (this.rolseleccionado[i].id_rol == rol.id_rol) {
        return true
      }
    return false;
  }
  asignarRol(rol: Roles) {//marcarrol
    if (this.seleccionarUsuario == null) console.log("seleccionar usuario");
    else {
      //this.xpostRol = !this.xpostRol
      if (this.rolencontrado(rol)) {
        const index = this.rolseleccionado.indexOf(rol);
        this.rolseleccionado.splice(index, 1)
        this.rolservice.QuitarRolUsuario(this.seleccionarUsuario, rol).subscribe(() => {
          console.log("se quito el rol")
          this.subirRol(this.seleccionarUsuario)
        })
      } else {
        this.rolseleccionado.push(rol)
        this.rolservice.AsginarRolUsuario(this.seleccionarUsuario, rol).subscribe(() => {
          console.log("se agrego rol al usuario")
          this.subirRol(this.seleccionarUsuario)
        })
      } console.log("este rol" + rol.nombre);
    }
    this.subirRol(this.seleccionarUsuario)
  }

}
