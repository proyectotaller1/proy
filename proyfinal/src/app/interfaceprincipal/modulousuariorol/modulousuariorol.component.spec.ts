import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ModulousuariorolComponent } from './modulousuariorol.component';

describe('ModulousuariorolComponent', () => {
  let component: ModulousuariorolComponent;
  let fixture: ComponentFixture<ModulousuariorolComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ModulousuariorolComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(ModulousuariorolComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
