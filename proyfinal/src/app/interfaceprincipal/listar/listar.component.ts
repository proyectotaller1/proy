import { Component, OnInit, ElementRef, ViewChild } from '@angular/core';
import { Router } from '@angular/router';
import { Persona } from 'src/app/model/persona';
import { FormControl, Validators, FormGroup, NgModel, AbstractControl, ValidatorFn } from '@angular/forms';
import { PersonaService } from 'src/app/services/persona.service';
import { UsuarioService } from 'src/app/services/usuario.service';
import { HttpClient } from '@angular/common/http';
import { Login } from 'src/app/model/login';
import { FormBuilder } from '@angular/forms';
import { OnDestroy } from '@angular/core';
import { User } from '@auth0/auth0-angular';
import { Observable, finalize } from 'rxjs';
import { AngularFireStorage } from '@angular/fire/compat/storage';

import { passwordValue } from '../password.validatos';
import { data } from 'jquery';
import { catchError, map, of } from 'rxjs';
import jsPDF from 'jspdf';
import * as jspdf from 'jspdf';
import html2canvas from 'html2canvas';
declare var window: any;

@Component({
  selector: 'app-listar',
  templateUrl: './listar.component.html',
  styleUrls: ['./listar.component.css']
})
export class ListarComponent implements OnInit {
  @ViewChild('modalContent') modalContent!: ElementRef;

  
  lista: Persona[] = [];
  xpersona!: any;
  pag: number = 1;
  buscar = '';
  visualizar: string = 'todos'
  perInfo = '';
  persona1!: Persona;
  // Variables para imagenes
  formSprache!: FormGroup;
  urlImage!: Observable<string>; // imagen rescatada de Firebase
  url = "/assets/icon/profile.png";
  //-----------persona-----
  regForm: any = new FormGroup({
    seq_persona: new FormControl(''),
    cedula_identidad: new FormControl('', [Validators.required]),
    nombres: new FormControl('', [Validators.required, Validators.pattern(/^[A-Za-z\s\xF1\xD1]+$/), Validators.minLength(2),
    Validators.maxLength(30),]),
    apellido_paterno: new FormControl('', [Validators.required, Validators.pattern(/^[A-Za-z\s\xF1\xD1]+$/), Validators.minLength(2),
    Validators.maxLength(30),]),
    apellido_materno: new FormControl(''),
    fotografia: new FormControl(''),
    telefono_celular: new FormControl(''),
    cod_estado: new FormControl(''),
  })


  get fu() {
    return this.regForm.controls
  }

  constructor(private perService: PersonaService, private usuService: UsuarioService,
    private httpClient: HttpClient, private formBuilder: FormBuilder, private storagee: AngularFireStorage) { }

  listar() {
    this.perService.getListaPersona().subscribe((per) => (this.lista = per))
  }
  ngOnInit(): void {
    this.listar()
    this.formSprache = this.formBuilder.group({
      imageUrl: [''],
    });
  }
  /*------------------foftografia-----------*/
 
  //ximagen:any
  //CargarImagen
  
  nombreArchivoSeleccionado: string="";
  CargarImagen(event: any) {
    const files = event.target.files;
    const formData = new FormData();

  if (files && files.length > 0) {
   
    const archivoSeleccionado = event.target.files[0];
    formData.append('file', archivoSeleccionado);  

    if (archivoSeleccionado) {
      this.nombreArchivoSeleccionado = archivoSeleccionado.name;
      const fotografiaControl = this.regForm.get('fotografia');
      const inputText = document.getElementById('addtextInput') as HTMLInputElement;
       // Comprobar si 'fotografia' existe en el formulario
      if (fotografiaControl) {
        fotografiaControl.setValue(this.nombreArchivoSeleccionado);
        //para mostrar en el input
        inputText.value = this.nombreArchivoSeleccionado;
        console.log(formData)
        this.httpClient.post('http://localhost:8093/api/foto', formData).subscribe((response: any) => { });
        console.log("ver q si entra para consulta")
      }   
    } else {
      this.nombreArchivoSeleccionado = '';
      const fotografiaControl = this.regForm.get('fotografia');
       // Comprobar si 'fotografia' existe en el formulario
      if (fotografiaControl) {
        fotografiaControl.setValue('');
      }
    }
  }
}
  
/*-----------------adicionar persona ----------------------*/
  AddPersona() {
    console.log("pasooo: ");
    this.xpersona = {
      seq_persona: '',
      cedula_identidad: this.regForm.get('cedula_identidad')?.value,
      nombres: this.regForm.get('nombres')?.value,
      apellido_paterno: this.regForm.get('apellido_paterno')?.value,
      apellido_materno: this.regForm.get('apellido_materno')?.value,
      //fotografia: img.value,
      fotografia: this.regForm.get('fotografia')?.value,
      telefono_celular: this.regForm.get('telefono_celular')?.value,
      cod_estado: 1,

    }
    console.log("datos: " + this.xpersona.fotografia);
    this.perService
      .addPersona(this.xpersona)
      .subscribe((persona) => {
        this.perService.getListaPersona().subscribe((per) => (this.lista = per))
      });
  }

  // Metodo para obtener el url de firebase par la imagen
  /*imagePreview(e: any) {
    var reader = new FileReader();
    reader.readAsDataURL(e.target.files[0]);
    reader.onload = (event: any) => {
      this.url = event.target.result;
      const file = e.target.files[0];
      const filepath = this.regForm.controls.nombres.value + "_" + this.regForm.controls.apellido_paterno.value; //+ this.formSprache.value.language; //nombre a la imagen
      const ref = this.storagee.ref(filepath);
      const task = this.storagee.upload(filepath, file);
      task.snapshotChanges().pipe(finalize(() => this.urlImage = ref.getDownloadURL())).subscribe();
    }
  }*/

  getData(): any {
    return localStorage.getItem('currentUser')
  }
  eliminarPersona(seq_persona: number) {
    this.perService
      .delPersona(seq_persona)
      .subscribe(data => {
        for (let i = 0; i < this.lista.length; i++) {
          if (this.lista[i].seq_persona == seq_persona) {
            this.lista.splice(i, 1);
          }
        }
      })
  }
  /*-------------------modificar persona --------------------*/
  modificarPersona(xdat: Persona) {
    console.log("data: " + xdat.seq_persona);
    this.regForm.setValue({
      seq_persona: xdat.seq_persona,
      cedula_identidad: xdat.cedula_identidad,
      nombres: xdat.nombres,
      apellido_paterno: xdat.apellido_paterno,
      apellido_materno: xdat.apellido_materno,
      fotografia: xdat.fotografia,
      telefono_celular: xdat.telefono_celular,
      cod_estado: xdat.cod_estado
    })
    console.log("desde ts id: " + xdat.seq_persona);
  }
  modPersona() {
    this.xpersona = {
      seq_persona: this.regForm.get('seq_persona')?.value,
      cedula_identidad: this.regForm.get('cedula_identidad')?.value,
      nombres: this.regForm.get('nombres')?.value,
      apellido_paterno: this.regForm.get('apellido_paterno')?.value,
      apellido_materno: this.regForm.get('apellido_materno')?.value,
      fotografia: this.regForm.get('fotografia')?.value,
      telefono_celular: this.regForm.get('telefono_celular')?.value,

    }
    this.perService
      .modPersona(this.xpersona)
      .subscribe((persona) => {
        this.perService.getListaPersona().subscribe((per) => (this.lista = per))
      });

  }
  /* ------------------- activar inactivar ala persona ----------------*/
  cambiarFiltro(nombreRol: string) {
    if (this.visualizar === 'activo' && nombreRol === "x") {
      console.log(this.visualizar, nombreRol)
      this.perService.listaPersonasEstado(1)
        .subscribe((persona) => {
          this.perService.listaPersonasEstado(1).subscribe((per) => (this.lista = per))
          console.log(this.lista);
        })
    }  //entra por primer else y inactiva ala persona
    else if (this.visualizar === 'inactivo' && nombreRol === "x") {
      console.log(this.visualizar, nombreRol)
      this.perService.listaPersonasEstado(0)
        .subscribe((persona) => {
          this.perService.listaPersonasEstado(0).subscribe((per) => (this.lista = per))
          console.log(this.lista);

        })
      /*-----else para visuaizar todos las personas ---*/
    } else if (this.visualizar === 'todos' && nombreRol === "x") {
      console.log(this.visualizar, nombreRol)
      this.perService.getListaPersona()
        .subscribe((persona) => {
          this.perService.getListaPersona().subscribe((per) => (this.lista = per))
          console.log(this.lista);
        })
    }
  }
  /*------------abilitar estado persona--------------------------*/
  MandaEstadoPersona(per: Persona) {
    this.perInfo = per.nombres + " " + per.apellido_paterno + " " + per.apellido_materno;
    this.persona1 = per;
    console.log("ingresa aqui:" + this.perInfo);

  }

  /*-------------activar estado persona ---*/
  HabilitarEstado() {
    console.log("paso");

    if (this.persona1.cod_estado == 0) {
      this.perService.modificarEstadoPersona(this.persona1).subscribe((xper) => {
        this.perService.getListaPersona().subscribe((xper) =>
          (this.lista = xper))
      }

      )
    }
  }


  /*------------elimina estado persona-------------*/
  EliminarEstado() {
    if (this.persona1.cod_estado == 1) {
      this.perService.modificarEstadoPersona(this.persona1).subscribe((xper) => {
        this.perService.getListaPersona().subscribe((per) =>
          (this.lista = per))
      })
    }
  }

  /*------------imprimir-----------------*/
  infonombre = '';
  infoap = '';
  infoam = '';
  infocedula = '';
  infoid = '';
  infofotografia = '';
  infotelefono = '';
  infoestado = '';

  ImprimirDato(per: Persona) {
    this.infonombre = per.nombres;
    this.infoap = per.apellido_paterno;
    this.infoam = per.apellido_materno;
    this.infocedula = per.cedula_identidad;
    this.infoestado = per.cod_estado + '';
    this.infofotografia = per.fotografia;
    this.infotelefono = per.telefono_celular;
    this.infoid = per.seq_persona + '';
    this.persona1 = per;
    console.log("ingresa aqui:" + this.perInfo);
  }
  generarPDF() {
    const modal = this.modalContent.nativeElement;

    html2canvas(modal, {
      useCORS: true, // Habilitar CORS
    }).then((canvas) => {
      const pdf = new jspdf.jsPDF('p', 'px', 'a4');
      const pdfWidth = pdf.internal.pageSize.getWidth();
      const pdfHeight = pdf.internal.pageSize.getHeight();
      const imageWidth = canvas.width * 0.75;
      const imageHeight = canvas.height * 0.75;
      const position = 0;

      pdf.addImage(canvas.toDataURL('image/png'), 'PNG', 0, position, imageWidth, imageHeight);

      pdf.save('contenido_modal.pdf');
    });
  }



  /*-------------------------------------------------*/
  /*-------------------------------------------------*/
  /*-------------------------------------------------*/
  /*-------------------------------------------------*/


  //Agregar Usuario-----------------

  listarUsuarios: Login[] = [];
  xusuarios!: any;
  xper!: any;
  usuForm: any = new FormGroup({
    usuario: new FormControl('', [Validators.required]),
    contrasena: new FormControl('', [Validators.required, Validators.pattern(".{8,}")]),
    repetircontrasena: new FormControl(''),
    cod_estado: new FormControl(''),
  });
  
  get validarUsu () {
    return this.usuForm.controls
  }
  resetForm() {
    this.verificar_usuario1 = true;
    this.usuForm.reset();
    this.usuForm.enable();
  }
   /*----------confirmar paswword--------*/
 Valpassword: boolean= false;
 confirmarPassword(){
  if((this.usuForm.get('contrasena')?.value) == (this.usuForm.get('repetircontrasena')?.value)){
    this.Valpassword = false
  }else{
    this.Valpassword = true;
  }
 }
  /*-----listar usuario segun persona---*/
  verificar_usuario1: boolean = true;
  crearUsuario(data: any) {
    this.xper = data
    console.log("igresa aqui----");
    console.log("xd----------" + data);
    this.resetForm();
    this.usuService.lisarPersonaUsu(data).subscribe((usu) => {
      console.log("pasa consulta");

      if (usu != null) {
        this.verificar_usuario1 = false
        console.log("ya existe usuario: " + usu.usuario);
        this.usuForm.patchValue({
          usuario: usu.usuario,
          contrasena: usu.contrasena,
          repetircontrasena:  usu.contrasena
        })
        this.usuForm.disable();
      } else {
        this.verificar_usuario1 = true;
        console.log("adicionar usuario-------");
        this.usuForm.enable();

      }
    })
  }

  AddUsu() {
    this.xusuarios = {
      usuario: this.usuForm.get('usuario')?.value,
      contrasena: this.usuForm.get('contrasena')?.value,
      repetircontrasena: this.usuForm.get('repetircontrasena')?.value,
      personas: this.xper,
      token: '',
      cod_estado: 1,
    }
    console.log(this.xper);

    this.usuService.AddUser(this.xusuarios).subscribe((usu) => (this.listar))
    console.log("usuario guardado---" + this.xusuarios);
  

  }


  //modificar usuario---------------------------------
  resetForm1() {
    this.verificar_usuario = true;
    this.usuForm.reset();
    this.usuForm.enable();
  }
  verificar_usuario: boolean = true;
  modificarUsuario(data: any) {
    this.xper = data
    console.log("xd----------" + data);
    this.resetForm1();
    this.usuService.lisarPersonaUsu(data).subscribe((usu) => {
      console.log("pasa consulta");

      if (usu != null) {
        this.verificar_usuario = true
        console.log("modificar al usuario: " + usu.usuario);
        this.usuForm.patchValue({
          usuario: usu.usuario,
          contrasena: usu.contrasena,
          repetircontrasena: usu.repetircontrasena
        })
        this.usuForm.enable();
        this.usuForm.controls.usuario.disable();
      } else {
        this.verificar_usuario = false;
        console.log("no existe usuario-------");
        this.usuForm.disable();
      }
    })
  }
  ModUsuario() {
    const usuarioActual = this.usuForm.get('usuario')?.value;
    this.xusuarios = {
      usuario: usuarioActual,
      //usuario: this.usuForm.get('usuario')?.value,
      contrasena: this.usuForm.get('contrasena')?.value,
      repetircontrasena: this.regForm.get('repetircontrasena')?.value,
      personas: this.xper,
      token: '',
      cod_estado: 1,

    }
    this.usuService
      .modUsuario(this.xusuarios)
      .subscribe((persona) => {
        this.usuService.getListUserPer().subscribe((usu) => (this.listarUsuarios = usu))
      });

  }

}


