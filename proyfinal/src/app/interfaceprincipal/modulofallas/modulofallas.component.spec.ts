import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ModulofallasComponent } from './modulofallas.component';

describe('ModulofallasComponent', () => {
  let component: ModulofallasComponent;
  let fixture: ComponentFixture<ModulofallasComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ModulofallasComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(ModulofallasComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
