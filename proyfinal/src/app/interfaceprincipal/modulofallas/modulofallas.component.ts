import { Component } from '@angular/core';
import { Fallas } from 'src/app/model/fallas';
import { FormControl, Validators, FormGroup, NgModel } from '@angular/forms';
import { HttpClient } from '@angular/common/http';
import { FallasService } from 'src/app/services/fallas.service';
import { FormBuilder } from '@angular/forms';
import { OnDestroy } from '@angular/core';
import { Observable, finalize } from 'rxjs';



@Component({
  selector: 'app-modulofallas',
  templateUrl: './modulofallas.component.html',
  styleUrls: ['./modulofallas.component.css']
})
export class ModulofallasComponent {
  ListaFallas: Fallas[] = [];
  xfallas!: any;
  pag: number = 1;
  buscar = '';
  visualizar: string = 'todos'
  perInfo = '';
  fallasEstado!: Fallas;
  nombreArchivoSeleccionado: string = '';


  faForm: any = new FormGroup({
    id_falla: new FormControl('', [Validators.required, Validators.pattern("^[0-9]+")]),
    nombre: new FormControl('', [Validators.required, Validators.pattern(/^[A-Za-z\s\xF1\xD1]+$/), Validators.minLength(2),
    Validators.maxLength(250),]), 
    cod_estado: new FormControl(''),
  })
  get fu() {
    return this.faForm.controls
  }
  faForm1: any = new FormGroup({
    id_falla: new FormControl('', [Validators.required, Validators.pattern("^[0-9]+")]),
    nombre: new FormControl('', [Validators.required, Validators.pattern(/^[A-Za-z\s\xF1\xD1]+$/), Validators.minLength(2),
    Validators.maxLength(250),]), 
    cod_estado: new FormControl(''),
  })
  get fu1() {
    return this.faForm1.controls
  }
  constructor(private fallService: FallasService,
    private httpClient: HttpClient, private formBuilder: FormBuilder) {
     }

  ListarFallas() {
    this.fallService.getListaFallas().subscribe((fall) => (this.ListaFallas = fall))
  }
  ngOnInit(): void {
    this.ListarFallas()
  }

  AddFallas() {
    console.log("pasooo: ");
    this.xfallas = {
      id_falla: this.faForm.get('id_falla')?.value,
      nombre: this.faForm.get('nombre')?.value,
      cod_estado: 1,
    }
  
    this.fallService
      .addFallas(this.xfallas)
      .subscribe((fallas) => {
        this.fallService.getListaFallas().subscribe((fall) => (this.ListaFallas = fall))
  
        // Restablecer el formulario después de enviar los datos
        this.faForm.reset();
      });
  }


  getData(): any {
    return localStorage.getItem('currentUser')
  }
  
  /*-------------------modificar fallas--------------------*/
 
  modificarFallas(xdat: Fallas) {
  
    console.log("data: " + xdat.id_falla);
    this.faForm1.setValue({
      id_falla: xdat.id_falla,
      nombre: xdat.nombre,
      cod_estado: xdat.cod_estado
    })
    console.log("desde ts id: " + xdat.id_falla);
    this.faForm1.controls.id_falla.disable();

  }
  modFalllas() {
    this.xfallas = {
      id_falla: this.faForm1.get('id_falla')?.value,
      nombre: this.faForm1.get('nombre')?.value,
      cod_estado:1
    }
    this.fallService
      .modFallas(this.xfallas)
      .subscribe((fallas) => {
        this.fallService.getListaFallas().subscribe((fall) => (this.ListaFallas = fall))
      });

  }
  /* ------------------- activar inactivar ala persona ----------------*/
  cambiarFiltro(nombreRol: string) {
    if (this.visualizar === 'activo' && nombreRol === "x") {
      console.log(this.visualizar, nombreRol)
      this.fallService.listaFallasEstado(1)
        .subscribe((persona) => {
          this.fallService.listaFallasEstado(1).subscribe((per) => (this.ListaFallas = per))
          console.log(this.ListaFallas);
        })
    }  //entra por primer else y inactiva ala persona
    else if (this.visualizar === 'inactivo' && nombreRol === "x") {
      console.log(this.visualizar, nombreRol)
      this.fallService.listaFallasEstado(0)
        .subscribe((persona) => {
          this.fallService.listaFallasEstado(0).subscribe((per) => (this.ListaFallas= per))
          console.log(this.ListaFallas);

        })
      /*-----else para visuaizar todos las personas ---*/
    } else if (this.visualizar === 'todos' && nombreRol === "x") {
      console.log(this.visualizar, nombreRol)
      this.fallService.getListaFallas()
        .subscribe((fallas) => {
          this.fallService.getListaFallas().subscribe((per) => (this.ListaFallas= per))
          console.log(this.ListaFallas);
        })
    }
  }
  /*------------abilitar estado persona--------------------------*/
  // Declarar una variable para controlar el estado del botón
habilitarGuardar = false;
elimnarGuardar = false;

MandaEstadoFallas(fall: Fallas) {
  this.perInfo = fall.nombre;
  this.fallasEstado = fall;
  console.log("ingresa aqui:" + this.perInfo);
  
  // Actualizar la variable habilitarGuardar según la condición
  this.habilitarGuardar = this.fallasEstado.cod_estado === 0;
  this.elimnarGuardar = this.fallasEstado.cod_estado === 1;
}

HabilitarEstado() {
  console.log("paso");
  
  if (this.fallasEstado.cod_estado == 0) {
    this.fallService.modificarEstadoFallas(this.fallasEstado).subscribe((xper) => {
      this.fallService.getListaFallas().subscribe((xper) =>
        (this.ListaFallas = xper))
    });
  }
}



  /*------------elimina estado persona-------------*/
  EliminarEstado() {
    if (this.fallasEstado.cod_estado == 1) {
      this.fallService.modificarEstadoFallas(this.fallasEstado).subscribe((xper) => {
        this.fallService.getListaFallas().subscribe((per) =>
          (this.ListaFallas = per))
      })
    }
  }

}
