import { Component } from '@angular/core';
import { EusuariosService } from 'src/app/services/eusuarios.service';
import { Login } from 'src/app/model/login';

@Component({
  selector: 'app-pie',
  templateUrl: './pie.component.html',
  styleUrls: ['./pie.component.css']
})
export class PieComponent {
  constructor(private obtener: EusuariosService){}
  Persona!: Login
  nombrePersona?: string="vacio"
  nombrePersona1?: string="vacio"
  ngOnInit(){
    this.Persona= this.obtener.ObtenerDato()
    this.nombrePersona= this.Persona.personas?.nombres
    this.nombrePersona1= this.Persona.personas?.apellido_paterno
    console.log("persona en pie : "+this.Persona.personas?.nombres);
    
  }
fecha: Date= new Date();

}
