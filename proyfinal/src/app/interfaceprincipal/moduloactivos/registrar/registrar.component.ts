import { Component } from '@angular/core';
import { FormControl, Validators, FormGroup, NgModel } from '@angular/forms';
import { Observable, finalize } from 'rxjs';
import { HttpClient } from '@angular/common/http';
import { FormBuilder } from '@angular/forms'
import { Activos } from 'src/app/model/activos';
import { ActivosService } from 'src/app/services/activos.service';
@Component({
  selector: 'app-registrar',
  templateUrl: './registrar.component.html',
  styleUrls: ['./registrar.component.css']
})
export class RegistrarComponent {
  xactivos!: any;
  ListaActivos: Activos[] = []
  actiForm: any = new FormGroup({
    seq_activo: new FormControl(''),
    id_etiqueta_fisica: new FormControl('',),
    nombre: new FormControl('',[Validators.required]),
    marca: new FormControl('',[Validators.required]),
    modelo: new FormControl(''),
    comentarios: new FormControl('',[Validators.required]),
    cod_estado: new FormControl(''),
  })
  get fu() {
    return this.actiForm.controls
  }
  constructor(private actiService: ActivosService) { }
  /*listar activos---*/
  ListarActivos() {
    this.actiService.getListaActivos().subscribe((xsol) => (this.ListaActivos = xsol))
  }
  ngOnInit(): void {
    this.ListarActivos()
  }

  AddActivo() {
    console.log("pasooo: ");
    this.xactivos = {
      seq_activo: '',
      id_etiqueta_fisica: this.actiForm.get('id_etiqueta_fisica')?.value,
      nombre: this.actiForm.get('nombre')?.value,
      marca: this.actiForm.get('marca')?.value,
      modelo: this.actiForm.get('modelo')?.value,
      comentarios: this.actiForm.get('comentarios')?.value,
      cod_estado: 1,

    }
    this.actiService
      .addActivos(this.xactivos)
      .subscribe((activos) => {
        this.actiService.getListaActivos().subscribe((fall) => (this.ListaActivos = fall))
        this.actiForm.reset();
      });
  }

}
