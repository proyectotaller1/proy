import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ReactiveFormsModule } from '@angular/forms';
import { ModuloactivosRoutingModule } from './moduloactivos-routing.module';
import { RegistrarComponent } from './registrar/registrar.component';
import { ListaComponent } from './lista/lista.component';
import { FiltroactivosPipe } from 'src/app/components/pipes/filtroactivos.pipe';
import { NgxPaginationModule } from 'ngx-pagination';
import { FormsModule } from '@angular/forms';
import { RouterModule } from '@angular/router';
@NgModule({
  declarations: [
    RegistrarComponent,
    ListaComponent,
    FiltroactivosPipe,
  ],
  imports: [
    CommonModule,
    ModuloactivosRoutingModule,
    RouterModule,
    ReactiveFormsModule,
    FormsModule,
    NgxPaginationModule,
  
  ]
})
export class ModuloactivosModule { }
