import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ModuloactivosComponent } from './moduloactivos.component';

describe('ModuloactivosComponent', () => {
  let component: ModuloactivosComponent;
  let fixture: ComponentFixture<ModuloactivosComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ModuloactivosComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(ModuloactivosComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
