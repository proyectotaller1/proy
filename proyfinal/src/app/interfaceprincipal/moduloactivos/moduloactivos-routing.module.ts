import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { ListaComponent } from './lista/lista.component';
import { RegistrarComponent } from './registrar/registrar.component';
import { InterfaceprincipalComponent } from '../interfaceprincipal.component';
import { ModuloactivosComponent } from './moduloactivos.component';

const routes: Routes = [
  /*{path: 'lista',component: ListaComponent },
  {path: 'registrar',component: RegistrarComponent }, */
  { path:'', component:ModuloactivosComponent, 
  children:[
    {path:'registrar',component:RegistrarComponent}
  ]
},
  { path:'', component:ModuloactivosComponent, 
  children:[
    {path:'listar',component:ListaComponent}
  ]
},

];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ModuloactivosRoutingModule { }
