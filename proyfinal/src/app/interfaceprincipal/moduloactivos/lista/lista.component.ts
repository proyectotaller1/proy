import { Component, OnInit } from '@angular/core';
import { FormControl, Validators, FormGroup, NgModel } from '@angular/forms';
import { Observable, finalize } from 'rxjs';
import { HttpClient } from '@angular/common/http';
import { FormBuilder } from '@angular/forms'
import { Activos } from 'src/app/model/activos';
import { ActivosService } from 'src/app/services/activos.service';

@Component({
  selector: 'app-lista',
  templateUrl: './lista.component.html',
  styleUrls: ['./lista.component.css']
})
export class ListaComponent {
  pag: number = 1;
  buscar = '';
  ListaActivos: Activos[] = [];
  xactivos!: any;
  visualizar: string = 'todos'
  perInfo = '';
  activoEstado!: Activos;
  nombreArchivoSeleccionado: string = '';

  //-----------persona-----
  actiForm: any = new FormGroup({
    seq_activo: new FormControl(''),
    id_etiqueta_fisica: new FormControl('',),
    nombre: new FormControl('',[Validators.required]),
    marca: new FormControl('',[Validators.required]),
    modelo: new FormControl(''),
    comentarios: new FormControl('',[Validators.required]),
    cod_estado: new FormControl(''),
  })
  get fu() {
    return this.actiForm.controls
  }
  constructor(private actiService: ActivosService) {}
  /*listar activos---*/
  ListarActivos() {
    this.actiService.getListaActivos().subscribe((xsol) => (this.ListaActivos = xsol))
  }
  ngOnInit(): void {
    this.ListarActivos()

  }
  //los datos para activos se adiciona desde otro componente llamado registrar
  getData(): any {
    return localStorage.getItem('currentUser')
  }
  
  /*-------------------modificar activos--------------------*/
  modificarActivo(xdat: Activos) {
 
    console.log("data: " + xdat.seq_activo);
    this.actiForm.setValue({
      seq_activo: xdat.seq_activo,
      id_etiqueta_fisica: xdat.id_etiqueta_fisica,
      nombre: xdat.nombre,
      marca: xdat.marca,
      modelo: xdat.modelo,
      comentarios: xdat.comentarios,
      cod_estado: xdat.cod_estado

    })

    console.log("desde ts id: " + xdat.id_etiqueta_fisica);

  }
  modAct() {
    this.xactivos = {
      seq_activo: this.actiForm.get('seq_activo')?.value,
      id_etiqueta_fisica: this.actiForm.get('id_etiqueta_fisica')?.value,
      nombre: this.actiForm.get('nombre')?.value,
      marca: this.actiForm.get('marca')?.value,
      modelo: this.actiForm.get('modelo')?.value,
      comentarios: this.actiForm.get('comentarios')?.value,
      cod_estado: this.actiForm.get('cod_estado')?.value,
    }
    this.actiService
      .modActivos(this.xactivos)
      .subscribe((fallas) => {
        this.actiService.getListaActivos().subscribe((fall) => (this.ListaActivos = fall))
      });
  }
  /* ------------------- activar inactivar ala persona ----------------*/
  cambiarFiltro(nombreRol: string) { 
    if (this.visualizar === 'activo' && nombreRol === "x") {
      console.log(this.visualizar, nombreRol)
      this.actiService.listaActivosEstado(1)
        .subscribe((persona) => {
          this.actiService.listaActivosEstado(1).subscribe((per) => (this.ListaActivos = per))
          console.log(this.ListaActivos);
        })
    }  //entra por primer else y inactiva ala persona
    else if (this.visualizar === 'inactivo' && nombreRol === "x") {
      console.log(this.visualizar, nombreRol)
      this.actiService.listaActivosEstado(0)
        .subscribe((persona) => {
          this.actiService.listaActivosEstado(0).subscribe((per) => (this.ListaActivos= per))
          console.log(this.ListaActivos);

        }) 
      /*-----else para visuaizar todos las personas ---*/
   } else if (this.visualizar === 'todos' && nombreRol === "x") {
      console.log(this.visualizar, nombreRol)
      this.actiService.getListaActivos()
        .subscribe((fallas) => {
          this.actiService.getListaActivos().subscribe((per) => (this.ListaActivos= per))
          console.log(this.ListaActivos);
        })
    } 
  }
  /*------------abilitar estado persona--------------------------*/
  // Declarar una variable para controlar el estado del botón
habilitarGuardar = false;
elimnarGuardar = false;
  MandaEstadoActivo(fall: Activos) {
    this.perInfo = fall.nombre;
    this.activoEstado = fall;
    console.log("ingresa aqui:" + this.perInfo);
     // Actualizar la variable habilitarGuardar según la condición
  this.habilitarGuardar = this.activoEstado.cod_estado === 0;
  this.elimnarGuardar = this.activoEstado.cod_estado === 1;
  }

  /*-------------activar estado persona ---*/
  HabilitarEstado() {
    console.log("paso");
    if (this.activoEstado.cod_estado == 0) {
      this.actiService.modificarEstadoFallas(this.activoEstado).subscribe((xper) => {
        this.actiService.getListaActivos().subscribe((xper) =>
          (this.ListaActivos = xper))
      }

      )
    } 
  }


  /*------------elimina estado persona-------------*/
  EliminarEstado() {
   if (this.activoEstado.cod_estado == 1) {
      this.actiService.modificarEstadoFallas(this.activoEstado).subscribe((xper) => {
        this.actiService.getListaActivos().subscribe((per) =>
          (this.ListaActivos= per))
      })
    }
  }
}
