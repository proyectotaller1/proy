import { Component } from '@angular/core';
import { Router } from '@angular/router';
import { Login } from 'src/app/model/login';
import { LoginService } from 'src/app/services/login.service';
import { Roles } from 'src/app/model/roles';
import * as $ from 'jquery';
import { MenurolService } from 'src/app/services/menurol.service';
$(function () {
  // Sidebar toggle behavior
  $('#sidebarCollapse').on('click', function () {
    $('#sidebar, #content').toggleClass('active');
  });
});

@Component({
  selector: 'app-menu',
  templateUrl: './menu.component.html',
  styleUrls: ['./menu.component.css'],
  template: `<example-sidenav></example-sidenav>`
})
export class MenuComponent {
  moduloActivo: string = 'listar';

  activarModulo(event: Event) {
    const target = event.target as HTMLSelectElement;
    if (target) {
      const value = target.value;
      this.moduloActivo = value;
      // Realiza las acciones necesarias al cambiar de módulo, como la navegación a la página correspondiente.
    }
  }

  constructor(private router: Router, private usuService: LoginService,private menu:MenurolService) { }
  activarhome() {
    this.router.navigate(['computer_maintenance/principal'])
  }
  activarpersona() {
    this.router.navigate(['computer_maintenance/listar'])
  }
  activarfalla() {
    this.router.navigate(['computer_maintenance/ModuloFallas'])
  }
  activarusuariorol() {
    this.router.navigate(['computer_maintenance/asignacionRolUsuario'])
  }
  activaractivos() {
    this.router.navigate(['computer_maintenance/moduloactivos/registrar'])
  }
  activaractivos1() {
    this.router.navigate(['computer_maintenance/moduloactivos/listar'])
  }
  activarSer1() {
    this.router.navigate(['computer_maintenance/modulosolicitud/listar'])
  }
  activarSer2() {
    this.router.navigate(['computer_maintenance/modulosolicitud/registrar'])
  }
  activarSer3() {
    this.router.navigate(['computer_maintenance/modulosolicitud/edicion'])
  }

  activarOrden1() {
    this.router.navigate(['computer_maintenance/moduloOrden/listar'])
  }
  activarOrden2() {
    this.router.navigate(['computer_maintenance/moduloOrden/registrar'])
  }
  activarMan1() {
    this.router.navigate(['computer_maintenance/moduloMan/listar'])
  }
  activarMan2() {
    this.router.navigate(['computer_maintenance/moduloMan/registrar'])
  }
  nombreRol:string=""
  ngOnInit(){
    this.nombreRol=this.menu.obtenerRol()
  }
}
