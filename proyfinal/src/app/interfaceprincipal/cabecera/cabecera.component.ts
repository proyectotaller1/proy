import { Component } from '@angular/core';
import { Router, TitleStrategy } from '@angular/router';
import { LoginService } from 'src/app/services/login.service';
import { Login } from 'src/app/model/login';
import { Roles } from 'src/app/model/roles';
import { RolService } from 'src/app/services/rol.service';
import { FormControl } from '@angular/forms';
import { FormGroup } from '@angular/forms';
import { FormBuilder } from '@angular/forms';
import { Rolusu } from 'src/app/model/rolusu';
import { MenurolService } from 'src/app/services/menurol.service';
import { ChangeDetectorRef } from '@angular/core';
@Component({
  selector: 'app-cabecera',
  templateUrl: './cabecera.component.html',
  styleUrls: ['./cabecera.component.css']
})
export class CabeceraComponent {
  lisRol: Roles[]=[];
  xusuario:any
  xuser:any
  roles=new FormGroup({
    nombre:new FormControl()
  })
  nombre: string = ''; 
constructor(private router:Router,
            public rolesM:MenurolService,
            private xrol:RolService,private cdr: ChangeDetectorRef ){
}
ngOnInit(): void {
  this.xuser = this.NomUser();
  this.xusuario = JSON.parse(this.xuser).usuario;

  this.xrol.getObtenerRolUsuario(this.xusuario).subscribe((xroles) => {
    this.lisRol = xroles;
    console.log('Roles obtenidos:', xroles);
  });
}
 
   NomUser():any{
    return localStorage.getItem('currentUser')
  }  
  Exit(){
    this.router.navigate(['BootService-maintenance/login'])
    this.limpiar()
    }
  limpiar(){
    return localStorage.removeItem('currentUser');
  }
   
  HabilitarMenu(e:any){
    const idRol:number=e.target.value
    const rol:Roles=this.lisRol.find(rol=>rol.id_rol==idRol)!
    this.rolesM.guardarRol(rol.nombre)
    window.location.reload()
   
  }


  /*listaRol!: any;
  xlistaRol: any[]=[];
  usuario!: Login;
  cliente: boolean = false;
  constructor(private router: Router, private ususervice: LoginService){}
  nombreRol: number=0;
  activar(ClaveRol: number){
    this.nombreRol= ClaveRol
    this.ususervice.setOpcion(ClaveRol)
    console.log("cambiando rol...'");
    this.ususervice.setBarra(-2)
    console.log("rol ingresadooo---"+this.ususervice.getBarra());
    this.router.navigate(['computer_maintenance/principal'])     
  }
   
  ngOnInit(){
    this.nombreRol=0
    this.usuario= this.ususervice.obtenerDato()
    let xclaveRol: number = this.ususervice.getOpcion()
    if(xclaveRol!=null) this.nombreRol= xclaveRol
    else this.nombreRol

  }
  xrol(num: number ): boolean {
    this.xlistaRol = this.ususervice.obtenerRol()
    let xxrol: boolean= false
    if(this.xlistaRol != null){
      this.xlistaRol.forEach(rolx =>{
        if(rolx.id_rol== num) xxrol= true;
        
      })
    } else{
      return false
    }
    return xxrol
  }*/
}
