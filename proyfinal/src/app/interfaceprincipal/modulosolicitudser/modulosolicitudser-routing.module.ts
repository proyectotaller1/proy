import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { ListaComponent } from './lista/lista.component';
import { RegistrarComponent } from './registrar/registrar.component';
import { EdicionComponent } from './edicion/edicion.component';
import { ModulosolicitudserComponent } from './modulosolicitudser.component';

const routes: Routes = [
  { path:'', component:ModulosolicitudserComponent, 
  children:[
    {path:'registrar',component:RegistrarComponent}
  ]
},
  { path:'', component:ModulosolicitudserComponent, 
  children:[
    {path:'listar',component:ListaComponent}
  ]
},
{ path:'', component:ModulosolicitudserComponent, 
children:[
  {path:'edicion',component: EdicionComponent}
]
},
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ModulosolicitudserRoutingModule { }
