import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ModulosolicitudserComponent } from './modulosolicitudser.component';

describe('ModulosolicitudserComponent', () => {
  let component: ModulosolicitudserComponent;
  let fixture: ComponentFixture<ModulosolicitudserComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ModulosolicitudserComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(ModulosolicitudserComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
