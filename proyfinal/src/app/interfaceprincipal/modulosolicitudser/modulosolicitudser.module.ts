import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { NgxPaginationModule } from 'ngx-pagination';
import { FormsModule } from '@angular/forms';
import { RouterModule } from '@angular/router';
import { ReactiveFormsModule } from '@angular/forms';
import { ModulosolicitudserRoutingModule } from './modulosolicitudser-routing.module';
import { EdicionComponent } from './edicion/edicion.component';
import { ListaComponent } from './lista/lista.component';
import { RegistrarComponent } from './registrar/registrar.component';
import { FiltrosolicitudesServicioPipe } from 'src/app/components/pipes/filtrosolicitudes-servicio.pipe';
import { FiltrofechasoliPipe } from 'src/app/components/pipes/filtrofechasoli.pipe';

@NgModule({
  declarations: [
    EdicionComponent,
    RegistrarComponent,
    ListaComponent,
    FiltrosolicitudesServicioPipe,
    FiltrofechasoliPipe

    
  ],
  imports: [
    CommonModule,
    ModulosolicitudserRoutingModule,
    RouterModule,
    ReactiveFormsModule,
    FormsModule,
    NgxPaginationModule,
  ]
})
export class ModulosolicitudserModule { }
