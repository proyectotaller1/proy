import { Component } from '@angular/core';
import { FormControl, Validators, FormGroup, NgModel } from '@angular/forms';
import { Observable, finalize } from 'rxjs';
import { HttpClient } from '@angular/common/http';
import { FormBuilder } from '@angular/forms'
import { SolicitudesServicio } from 'src/app/model/solicitudes-servicio';
import { SolicitudesServicioService } from 'src/app/services/solicitudes-servicio.service';
import { Persona } from 'src/app/model/persona';
import { PersonaService } from 'src/app/services/persona.service';
import { RecepactivoService } from 'src/app/services/recepactivo.service';

@Component({
  selector: 'app-registrar',
  templateUrl: './registrar.component.html',
  styleUrls: ['./registrar.component.css']
})
export class RegistrarComponent {
  soliForm: any;
  ListaSoli: SolicitudesServicio[] = [];
  xsoli!: any;

  /*-------para solicitud de servicio----*/
  constructor(private soliService: SolicitudesServicioService, private formBuilder: FormBuilder, private perService: PersonaService, private receActService: RecepactivoService) {
    this.soliForm = this.formBuilder.group({
      seq_solicitud_servicio: new FormControl(''),
      fecha: new FormControl(''),
      descripcion_problema: new FormControl(''),
      cod_estado: new FormControl(''),
      personas: this.formBuilder.group({
        nombres: new FormControl('', [Validators.required]),
      })
    });
  }
  get fu() {
    return this.soliForm.controls
  }

  /*-----sacar listar persona--*/
  xper!: any;
  ListaPersona: Persona[] = [];
  //listar solicitud persona
  ListarSoli() {
    this.soliService.getListaSoli().subscribe((xsol) => (this.ListaSoli = xsol))
  }
  //listar perosona
  ListarPersona() {
    this.receActService.getListarPersonaCliente().subscribe((xper) => (this.ListaPersona = xper))
  }
  ngOnInit(): void {
    this.ListarSoli()
    this.ListarPersona()
    this.inicializarFechas();

  }
  fechaDesde: string="";
  inicializarFechas() {
    const fechaActual = new Date().toISOString().split('T')[0];
    this.fechaDesde = fechaActual;
   
  }
  personas: any
  selectPersona(event: any) {
    const selectedPersonaId = event.target.value;
    console.log("ID de la persona seleccionada:", selectedPersonaId);
    this.personas = this.ListaPersona.find(persona => persona.seq_persona == selectedPersonaId);
    console.log("Persona seleccionada:", this.personas);
    if (this.personas) {
      console.log("Nombre de la persona:", this.personas.nombres);
    }
  }

  AddSoli() {
    console.log("pasooo: ");
    this.xsoli = {
      seq_solicitud_servicio: '',
      fecha: this.soliForm.get('fecha')?.value,
      descripcion_problema: this.soliForm.get('descripcion_problema')?.value,
      cod_estado: 1,
      personas: this.personas
    }
    console.log(" datos de la persona. " + this.xsoli.personas);
    this.soliService
      .addSoli(this.xsoli)
      .subscribe((fallas) => {
        this.soliService.getListaSoli().subscribe((fall) => (this.ListaSoli = fall))
        this.soliForm.reset();
      });
  }


}