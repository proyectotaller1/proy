import { Component, OnInit, Input } from '@angular/core';
import { FormControl, Validators, FormGroup, NgModel } from '@angular/forms';
import { Observable, finalize } from 'rxjs';
import { HttpClient } from '@angular/common/http';
import { FormBuilder } from '@angular/forms'
import { SolicitudesServicio } from 'src/app/model/solicitudes-servicio';
import { SolicitudesServicioService } from 'src/app/services/solicitudes-servicio.service';
import { Persona } from 'src/app/model/persona';
import { PersonaService } from 'src/app/services/persona.service';
import { Recepactivo } from 'src/app/model/recepactivo';
import { RecepactivoService } from 'src/app/services/recepactivo.service';
import { Activos } from 'src/app/model/activos';
import { ActivosService } from 'src/app/services/activos.service';
import { FiltrofechasoliPipe } from 'src/app/components/pipes/filtrofechasoli.pipe';
@Component({
  selector: 'app-lista',
  templateUrl: './lista.component.html',
  styleUrls: ['./lista.component.css']
})
export class ListaComponent implements OnInit {
  
  /*para solicitud de sericios----*/
  pag: number = 1;
  buscar = '';
  soliForm: any;
  ListaSoli: SolicitudesServicio[] = [];
  xsoli!: any;
  visualizar: string = 'todos'
  perInfo = '';
  perInfo1 = '';
  soliEstado!: SolicitudesServicio;
  nombreArchivoSeleccionado: string = '';

  /*--para recepciones activos---*/
  receForm: any;
  ListaRece: Recepactivo[] = [];
  xrece!: any;


  // Función para mostrar la segunda parte al hacer clic en el botón de editar
  mostrarEdicion: boolean = false;
  cancelarEdicion() {
    this.mostrarEdicion = false;
    this.ListarSoli();

  }

  /*-------solicitud de recepciones----*/
  constructor(private soliService: SolicitudesServicioService, private formBuilder: FormBuilder, private perService: 
    PersonaService, private ReceActService: RecepactivoService, private actiService: ActivosService) {
    this.soliForm = this.formBuilder.group({
      seq_solicitud_servicio: new FormControl(''),
      fecha: new FormControl(''),
      descripcion_problema: new FormControl(''),
      cod_estado: new FormControl(''),
      personas: this.formBuilder.group({
        seq_persona: new FormControl(''),
        nombres: new FormControl(''),
        apellido_paterno: new FormControl(''),
        apellido_materno: new FormControl('')
      })
    });
    /*-------recepciones activos---*/
    this.receForm = this.formBuilder.group({
      seq_recepcion_activo: new FormControl(''),
      fecha: new FormControl(''),
      cod_estado: new FormControl(''),
      personas: this.formBuilder.group({
        nombres: new FormControl(''),
        apellido_paterno: new FormControl(''),
        apellido_materno: new FormControl('')
      }),
      activos: this.formBuilder.group({
        nombre: new FormControl(''),
        id_etiqueta_fisica: new FormControl(''),
      }),
      solicitud: this.formBuilder.group({
        seq_solicitud_servicio: new FormControl(''),
      })
    });

    this.receForm1 = this.formBuilder.group({
      seq_recepcion_activo: new FormControl(''),
      fecha: new FormControl(''),
      cod_estado: new FormControl(''),
      personas: this.formBuilder.group({
        nombres: new FormControl(''),
        apellido_paterno: new FormControl(''),
        apellido_materno: new FormControl('')
      }),
      activos: this.formBuilder.group({
        nombre: new FormControl(''),
        id_etiqueta_fisica: new FormControl(''),
      }),
      solicitud: this.formBuilder.group({
        seq_solicitud_servicio: new FormControl(''),
      })
    });

  }
  get fu() {
    return this.soliForm.controls
  }

  //listar persona
  xper!: any;
  ListaPersonaCliente: Persona[] = [];
  ListarPersonaCliente() {
      this.ReceActService.getListarPersonaCliente().subscribe((xper) => (this.ListaPersonaCliente = xper))
    
  }
  //listar persona
  xper1!: any;
  ListaPersonaNoCliente: Persona[] = [];
  ListarPersonaNoCliente() {
    this.ReceActService.getListarPersonaNoCliente().subscribe((xper) => {
      // Filtra las personas con cod_estado igual a 1 y elimina duplicados
      const personasFiltradas = xper.filter(perso => perso.cod_estado === 1);
      const personasUnicas = this.eliminarDuplicados(personasFiltradas);
      this.ListaPersonaNoCliente = personasUnicas;
    });
  }
  
  eliminarDuplicados(personas: Persona[]): Persona[] {
    const personasSet = new Set(personas.map(perso => JSON.stringify(perso)));
    return Array.from(personasSet).map(persoString => JSON.parse(persoString));
  }





  //listar activos
  xact!: any;
  ListaActivos: Activos[] = [];
  ListarActivos() {
    this.actiService.getListaActivos().subscribe((xper) => {
      this.ListaActivos = xper.filter(activo => activo.cod_estado === 1);
    });
  }
  /*---listar solicitud de servicio--*/
  ListarSoli() {
    this.soliService.getListaSoli().subscribe((xsol) => (this.ListaSoli = xsol))
  }
  
  //listar repecion activo
  ListarRece() {
    this.ReceActService.getListaReceActivo().subscribe((xsol) => (this.ListaRece = xsol))
}

  ngOnInit(): void {
    this.ListarSoli()
    this.ListarPersonaCliente()
    this.ListarPersonaNoCliente()
    this.ListarRece()
    this.ListarActivos()
    this.inicializarFechas();
    
   console.log("lista persona noooooooo cliente::"+this.ListarPersonaNoCliente);
   
  }
  /*----metodo para sacar en select de persona --*/
  personaCliente1: any
  selectPersona(event: any) {
    const selectedPersonaId = event.target.value;
    this.personaCliente1 = this.ListaPersonaCliente.find(persona => persona.seq_persona == selectedPersonaId);
    if (this.personaCliente1) {
      this.soliForm.patchValue({
        personas: this.personaCliente1
      });
    }
  }
  
  /*-------------------modificar solicitud--------------------*/
  modificarSoli(xdat: SolicitudesServicio) {
    this.personaCliente1 = this.ListaPersonaCliente.find(persona => persona.seq_persona == xdat.personas?.seq_persona); 
    this.ReceActService.getListaRecepcionActivo(xdat).subscribe((resp)=>{this.ListaRece=resp})
    if (xdat.cod_estado === 1) {
      this.ReceActService.getSolicitudCompleta(xdat.seq_solicitud_servicio).subscribe(
        (solicitudCompleta) => {
          this.solicitudActual = solicitudCompleta;
          }
      );
        }
      this.soliForm.patchValue({
        seq_solicitud_servicio: xdat.seq_solicitud_servicio,
        fecha: xdat.fecha,
        descripcion_problema: xdat.descripcion_problema,
        personas: xdat.personas,
        cod_estado: xdat.cod_estado 
      });
     
  }

  modSoli() {
    this.xsoli = {
      seq_solicitud_servicio: this.soliForm.get('seq_solicitud_servicio')?.value,
      fecha: this.soliForm.get('fecha')?.value,
      descripcion_problema: this.soliForm.get('descripcion_problema')?.value,
      personas: this.personaCliente1,
      cod_estado: this.soliForm.get('cod_estado')?.value,
    }
    
    this.soliService.modSoli(this.xsoli).subscribe(() => {
      this.ListarSoli(); // Vuelve a cargar los datos después de guardar
      this.mostrarEdicion = false; // Oculta el módulo de edición
    });

  }
  /* ------------------- activar inactivar ala persona ----------------*/
  cambiarFiltro(nombreRol: string) {
    if (this.visualizar === 'activo' && nombreRol === "x") {
      console.log(this.visualizar, nombreRol)
      this.soliService.listaSoliEstado(1)
        .subscribe((persona) => {
          this.soliService.listaSoliEstado(1).subscribe((per) => (this.ListaSoli = per))
          console.log(this.ListaSoli);
        })
    }  //entra por primer else y inactiva ala persona
    else if (this.visualizar === 'inactivo' && nombreRol === "x") {
      console.log(this.visualizar, nombreRol)
      this.soliService.listaSoliEstado(0)
        .subscribe((persona) => {
          this.soliService.listaSoliEstado(0).subscribe((per) => (this.ListaSoli = per))
          console.log(this.ListaSoli);

        })
      /*-----else para visuaizar todos las personas ---*/
    } else if (this.visualizar === 'todos' && nombreRol === "x") {
      console.log(this.visualizar, nombreRol)
      this.soliService.getListaSoli()
        .subscribe((fallas) => {
          this.soliService.getListaSoli().subscribe((per) => (this.ListaSoli = per))
          console.log(this.ListaSoli);
        })
    }
  }
  /*------------abilitar estado persona--------------------------*/
  habilitarGuardar = false;
  elimnarGuardar = false;
  MandaEstadoSoli(fall: SolicitudesServicio) {
    this.perInfo = fall.descripcion_problema;
    this.soliEstado = fall;
    console.log("ingresa aqui:" + this.perInfo);
    this.habilitarGuardar = this.soliEstado.cod_estado === 0;
    this.elimnarGuardar = this.soliEstado.cod_estado === 1;

  }

  /*-------------activar estado persona ---*/
  HabilitarEstado() {
    console.log("paso");

    if (this.soliEstado.cod_estado == 0) {
      this.soliService.modificarEstadoSoli(this.soliEstado).subscribe((xper) => {
        this.soliService.getListaSoli().subscribe((xper) =>
          (this.ListaSoli = xper))
      }

      )
    }
  }


  /*------------elimina estado persona-------------*/
  EliminarEstado() {
    if (this.soliEstado.cod_estado == 1) {
      this.soliService.modificarEstadoSoli(this.soliEstado).subscribe((xper) => {
        this.soliService.getListaSoli().subscribe((per) =>
          (this.ListaSoli = per))
      })
    }
  }
 /*----filtrar por fecha desde y hasta ----*/
 fechaHasta: string = ""; 
 fechaDesde: string = ""; 
 inicializarFechas() {
  const fechaActual = new Date().toISOString().split('T')[0];
  this.fechaDesde = fechaActual;
  this.fechaHasta = fechaActual;
 }
 ListarSoliPorFecha() {
  const fechaDesde = (document.getElementById('fechaDesde') as HTMLInputElement).value;
  const fechaHasta = (document.getElementById('fechaHasta') as HTMLInputElement).value;

  if (fechaDesde && fechaHasta) {
    this.soliService.listaSoliPorFecha(new Date(fechaDesde), new Date(fechaHasta)).subscribe((xsol) => {
      this.ListaSoli = xsol;
    });
  }
}


/*-------------------------------------------------------------------------------*/
/*---------------------------------------------------------------------------------*/
/*----metodo para sacar en celect de actvivos --*/
xactivos: any
selectActivos(event: any) {
  const selectedPersonaId = event.target.value;
  console.log("ID de la Activos seleccionada:", selectedPersonaId);
  this.xactivos = this.ListaActivos.find(persona => persona.seq_activo == selectedPersonaId);
  console.log("Persona seleccionada:", this.xactivos);
  if (this.xactivos) {
    console.log("Nombre de la persona:", this.xactivos.nombre);
  }
}

/*------------adicionar recepcion activo---*/
personaNoCliente: any
selectActivos1(event: any) {
  const selectedPersonaId = event.target.value;
  console.log("ID de la Activos seleccionada:", selectedPersonaId);
  this.personaNoCliente = this.ListaPersonaNoCliente.find(persona => persona.seq_persona == selectedPersonaId);
  console.log("Persona seleccionada:", this.personaNoCliente);
  if (this.personaNoCliente) {
    console.log("Nombre de la persona:", this.personaNoCliente.nombres);
  }
}
solicitudActual: any;
AddRece() {
  console.log("persona::::" + this.personaNoCliente.nombres);
  this.xrece = {
    seq_recepcion_activo: '',
    fecha: this.receForm.get('fecha')?.value,
    personas: this.personaNoCliente,
    activos: this.xactivos,
    solicitudes_servicio: this.solicitudActual,
    cod_estado: 1,
  };

  console.log("Objeto xrece antes de la solicitud:", this.xrece); // Agrega este console.log
  this.ReceActService.addReceActivo(this.xrece)
      .subscribe((rece) => {
        this.ReceActService.getListaRecepcionActivo(this.solicitudActual).subscribe((xrece) => (this.ListaRece = xrece))
        this.receForm.reset();
      });
  }
getData(): any {
 return localStorage.getItem('currentUser')
}
//---------------
/*----metodo para sacar en select de pesona recepcionante pero para modificar--*/
personaNoCliente1: any
selectPersonaCli1(event: any) {
  const selectedPersonaId = event.target.value;
  console.log("ID de la persona seleccionada:", selectedPersonaId);
  this.personaNoCliente1 = this.ListaPersonaNoCliente.find(persona => persona.seq_persona == selectedPersonaId);
  console.log("Persona seleccionada:", this.personaNoCliente1);
  if (this.personaNoCliente1) {
    console.log("Nombre de la persona:", this.personaNoCliente1.nombres);
    // Establece el valor seleccionado en el formulario
    this.receForm1.patchValue({
      personas: this.personaNoCliente1
    });
  }
}

/*----metodo para sacar en select de activos recepcionante pero para modificar--*/
activosMod: any
activosMod1(event: any) {
  const selectedPersonaId4 = event.target.value;
  this.activosMod = this.ListaActivos.find(persona => persona.seq_activo  == selectedPersonaId4);
  if (this.activosMod) {
    this.receForm1.patchValue({
      activos: this.activosMod
    });
  }
}

//modificar recepciones activos
modRecepActivo(xdat: Recepactivo) {
  this.activosMod = this.ListaActivos.find(activo => activo.seq_activo == xdat.activos?.seq_activo);
  this.personaNoCliente1 = this.ListaPersonaNoCliente.find(persona => persona.seq_persona == xdat.personas?.seq_persona);
      this.receForm1.patchValue({
        seq_recepcion_activo: xdat.seq_recepcion_activo,
        fecha: xdat.fecha,
        personas: xdat.personas,
        activos: xdat.activos,
        solicitudes_servicio: xdat.solicitudes_servicio,
        cod_estado: xdat.cod_estado
      });
     
    }
receForm1: any;


  modRece() {
    this.xrece = {
      seq_recepcion_activo: this.receForm1.get('seq_recepcion_activo')?.value,
      fecha: this.receForm1.get('fecha')?.value,
      personas: this.personaNoCliente1,
      activos: this.activosMod,
      solicitudes_servicio: this.solicitudActual,
      cod_estado: this.receForm1.get('cod_estado')?.value,
    }
    
    this.ReceActService.modRece(this.xrece).subscribe(() => {
      this.ListarRece(); // Vuelve a cargar los datos después de guardar
  
    });

  }
//eliminar y activar estado de recepciones activos
// Declarar una variable para controlar el estado del botón
habilitarGuardar1 = false;
elimnarGuardar1 = false;
ReceEstado!: Recepactivo;
MandaEstadoRece(xrece: Recepactivo) {
  this.perInfo = xrece.cod_estado+'';
  this.ReceEstado = xrece;
  console.log("ingresa aqui:" + this.perInfo);
  
  // Actualizar la variable habilitarGuardar según la condición
  this.habilitarGuardar1 = this.ReceEstado.cod_estado === 0;
  this.elimnarGuardar1 = this.ReceEstado.cod_estado === 1;
}

HabilitarEstado1() {
  console.log("paso"); 
  if (this.ReceEstado.cod_estado == 0) {
    this.ReceActService.modificarEstadoRece(this.ReceEstado).subscribe((xper) => {
      this.ReceActService.getListaRecepcionActivo(this.solicitudActual).subscribe(
        (resp)=>{this.ListaRece=resp}
      ) 
    });
    
  }
}

  /*------------elimina estado persona-------------*/
  ;
  EliminarEstado1() {
    if (this.ReceEstado.cod_estado == 1) {
      this.ReceActService.modificarEstadoRece(this.ReceEstado).subscribe((xper) => {
        this.ReceActService.getListaRecepcionActivo(this.solicitudActual).subscribe(
          (resp)=>{this.ListaRece=resp}
        ) 
      });
      
    }
  }

}

