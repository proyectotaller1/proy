import { ComponentFixture, TestBed } from '@angular/core/testing';

import { InterfaceprincipalComponent } from './interfaceprincipal.component';

describe('InterfaceprincipalComponent', () => {
  let component: InterfaceprincipalComponent;
  let fixture: ComponentFixture<InterfaceprincipalComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ InterfaceprincipalComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(InterfaceprincipalComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
