import { ValidationErrors, AbstractControl  } from "@angular/forms";

export function passwordValue(contrasena: string, repetircontrasena: string){
    return function (form: AbstractControl){
        const xcontrasena= form.get(contrasena)?.value
        const xrepetircontrasena= form.get(repetircontrasena)?.value
        if(xcontrasena==xrepetircontrasena){
            return null

        }
        return {passwordMismatchError:true}

    }
}