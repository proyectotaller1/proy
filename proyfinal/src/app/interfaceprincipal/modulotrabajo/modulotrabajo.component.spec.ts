import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ModulotrabajoComponent } from './modulotrabajo.component';

describe('ModulotrabajoComponent', () => {
  let component: ModulotrabajoComponent;
  let fixture: ComponentFixture<ModulotrabajoComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ModulotrabajoComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(ModulotrabajoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
