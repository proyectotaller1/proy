import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { ModulotrabajoComponent } from './modulotrabajo.component';
import { RegistroComponent } from './registro/registro.component';
import { ListarrComponent } from './listarr/listarr.component';
const routes: Routes = [
  /*{path: 'lista',component: ListaComponent },
  {path: 'registrar',component: RegistrarComponent }, */
  { path:'', component:ModulotrabajoComponent, 
  children:[
    {path:'registrar',component:RegistroComponent}
  ]
},
  { path:'', component:ModulotrabajoComponent, 
  children:[
    {path:'listar',component:ListarrComponent}
  ]
},

];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ModulotrabajoRoutingModule { }
