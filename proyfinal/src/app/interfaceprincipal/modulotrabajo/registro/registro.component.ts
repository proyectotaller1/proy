import { Component } from '@angular/core';
import { FormControl, Validators, FormGroup, NgModel } from '@angular/forms';
import { Observable, finalize } from 'rxjs';
import { HttpClient } from '@angular/common/http';
import { FormBuilder } from '@angular/forms'
import { Trabajo } from 'src/app/model/trabajo';
import { TrabajoService } from 'src/app/services/trabajo.service';
import { SolicitudesServicioService } from 'src/app/services/solicitudes-servicio.service';
import { SolicitudesServicio } from 'src/app/model/solicitudes-servicio';
@Component({
  selector: 'app-registro',
  templateUrl: './registro.component.html',
  styleUrls: ['./registro.component.css']
})
export class RegistroComponent {
  trabForm: any;
  constructor(private soliService: SolicitudesServicioService, private trabService: TrabajoService, private formBuilder: FormBuilder,) {
    this.trabForm = this.formBuilder.group({
      seq_orden_trabajo: new FormControl(''),
      cod_tipo_mantenimiento: new FormControl(''),
      numero_orden_trabajo: new FormControl(''),
      fecha_emicion: new FormControl(''),
      ubicacion_activo: new FormControl(''),
      cod_estado: new FormControl(''),
      cod_atentido: new FormControl(0),
      solicitudes_servicio: this.formBuilder.group({
        seq_solicitud_servicio: new FormControl(''),
        fecha: new FormControl(''),
        personas: this.formBuilder.group({
          nombres: new FormControl(''),
          apellido_paterno: new FormControl('')
        })
      })
    });
    this.trabForm.patchValue({
      cod_atendido: 0,
    });
  }
  ListaTrabajo: Trabajo[] = [];
  ListarTrabajo() {
    this.trabService.getListaTrabajo().subscribe((xsol) => (this.ListaTrabajo = xsol))
  }
  ListaSoli: SolicitudesServicio[] = [];
  ListarSoli() {
    this.soliService.getListaSoli().subscribe((xsol) => (this.ListaSoli = xsol))
  }

  ngOnInit(): void {
    this.ListarTrabajo();
    this.ListarSoli();
    this.inicializarFechas();
  }

  fechaDesde: string = "";
  inicializarFechas() {
    const fechaActual = new Date().toISOString().split('T')[0];
    this.fechaDesde = fechaActual;

  }
  //seleccionar solitud fecha y cliente
  xsoli: any;
  selectSoli(event: any) {
    const selectedSolicitudId = event.target.value;
    console.log("ID de la solicitud seleccionada:", selectedSolicitudId);
    this.xsoli = this.ListaSoli.find(soli => soli.seq_solicitud_servicio == selectedSolicitudId);
    console.log("Solicitud seleccionada:", this.xsoli);
    if (this.xsoli) {
      console.log("Fecha de la solicitud:", this.xsoli.fecha);
      console.log("Persona de la solicitud:", this.xsoli.personas.nombres);
    }
  }
  //es para seleccionar tipo de mantenimiento
  tipoMantenimientoSeleccionado: any;
  updateTipoMantenimiento(event: any) {
    this.tipoMantenimientoSeleccionado = event.target.value;
  }
  //es para seleccionar atendido y combierte 1 y 0 para guardar
  
  updateAtendido(event: any) {
    const selectedValue = event.target.value;
    // Asigna 0 si es 'noatendido', 1 si es 'atendido'
    this.trabForm.get('cod_atentido')?.setValue(selectedValue === 'atendido' ? 1 : 0);
    console.log('Form Value after update:', this.trabForm.get('cod_atentido')?.value);
}
  //es para sacar el ultimo numero de numero orden de trabajo
  ListarTrabajo1() {
    this.trabService.getListaTrabajo().subscribe((xsol) => {
      this.ListaTrabajo = xsol;
      const ultimoNumero = this.obtenerUltimoNumeroOrdenTrabajo();
      const nuevoNumero = ultimoNumero + 1;
      this.trabForm.get('numero_orden_trabajo')?.setValue(`OT-${nuevoNumero}`);
    });

  }

  xtrab: any = {};
  AddTrab() {
   
    // Calcular el próximo número incrementado en uno
    const ultimoNumero = this.obtenerUltimoNumeroOrdenTrabajo();
    const nuevoNumero = ultimoNumero + 1;
    console.log("pasooo: ");
    this.xtrab = {
      seq_orden_trabajo: '', //por defecto
      cod_tipo_mantenimiento: this.tipoMantenimientoSeleccionado,
      numero_orden_trabajo: `OT-${nuevoNumero}`,
      fecha_emicion: this.trabForm.get('fecha_emicion')?.value,
      ubicacion_activo: this.trabForm.get('ubicacion_activo')?.value,
      cod_atentido: this.trabForm.get('cod_atentido')?.value,
      cod_estado: 1,
      personas: this.xsoli?.personas,
      solicitudes_servicio: this.xsoli
    }

    this.trabService.addTrabajo(this.xtrab)
      .subscribe((fallas) => {
        this.trabService.getListaTrabajo().subscribe((fall) => (this.ListaTrabajo = fall))
        this.trabForm.reset();
      });
  }
  private obtenerUltimoNumeroOrdenTrabajo(): number {
    return this.ListaTrabajo.length > 0 ? +this.ListaTrabajo[this.ListaTrabajo.length - 1].numero_orden_trabajo.replace('OT-', '') : 0;
  }

}

