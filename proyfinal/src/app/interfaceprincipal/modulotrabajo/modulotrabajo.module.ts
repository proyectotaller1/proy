import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FiltrotrabajoPipe } from 'src/app/components/pipes/filtrotrabajo.pipe';
import { ModulotrabajoRoutingModule } from './modulotrabajo-routing.module';
import { RegistroComponent } from './registro/registro.component';
import { ListarrComponent } from './listarr/listarr.component';
import { RouterModule } from '@angular/router';
import { ReactiveFormsModule, FormsModule } from '@angular/forms';
import { NgxPaginationModule } from 'ngx-pagination';
import { FiltroatendidoPipe } from 'src/app/components/pipes/filtroatendido.pipe';

@NgModule({
  declarations: [
    RegistroComponent,
    ListarrComponent,
    FiltrotrabajoPipe,
    FiltroatendidoPipe
  ],
  imports: [
    CommonModule,
    ModulotrabajoRoutingModule,
    RouterModule,
    ReactiveFormsModule,
    FormsModule,
    NgxPaginationModule,                                       
  ]
})
export class ModulotrabajoModule { }
