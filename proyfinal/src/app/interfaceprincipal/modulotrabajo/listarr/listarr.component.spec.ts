import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ListarrComponent } from './listarr.component';

describe('ListarrComponent', () => {
  let component: ListarrComponent;
  let fixture: ComponentFixture<ListarrComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ListarrComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(ListarrComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
