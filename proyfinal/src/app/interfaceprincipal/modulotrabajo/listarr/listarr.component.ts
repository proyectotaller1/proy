import { Component } from '@angular/core';
import { FormControl, Validators, FormGroup, NgModel } from '@angular/forms';
import { Observable, finalize } from 'rxjs';
import { HttpClient } from '@angular/common/http';
import { FormBuilder } from '@angular/forms'
import { TrabajoService } from 'src/app/services/trabajo.service';
import { Trabajo } from 'src/app/model/trabajo';
import { SolicitudesServicio } from 'src/app/model/solicitudes-servicio';
import { SolicitudesServicioService } from 'src/app/services/solicitudes-servicio.service';
import { TecnicoService } from 'src/app/services/tecnico.service';
import { Tecnico } from 'src/app/model/tecnico';
import { Trabajoactivo } from 'src/app/model/trabajoactivo';
import { TrabajoactivoService } from 'src/app/services/trabajoactivo.service';
import { RecepactivoService } from 'src/app/services/recepactivo.service';
import { Recepactivo } from 'src/app/model/recepactivo';
import { Persona } from 'src/app/model/persona';
import { Activos } from 'src/app/model/activos';
import { ActivosService } from 'src/app/services/activos.service';
@Component({
  selector: 'app-listarr',
  templateUrl: './listarr.component.html',
  styleUrls: ['./listarr.component.css']
})
export class ListarrComponent {

  
  filtroEstado: number = 2; // Inicia con Todos (2)

  filtrarTrabajos(estado: number) {
    this.filtroEstado = estado;
  }
  pag: number = 1;
  buscar = '';
  trabForm: any;
  tecForm: any;
  xtrab!: any;
  xtec!: any;
  ordenForm: any;
  xorden!: any;
  constructor(private actiService: ActivosService, private ReceActService: RecepactivoService, private ordentraService: TrabajoactivoService, private tecService: TecnicoService, private soliService: SolicitudesServicioService, private trabService: TrabajoService, private formBuilder: FormBuilder,) {
    this.trabForm = this.formBuilder.group({
      seq_orden_trabajo: new FormControl(''),
      cod_tipo_mantenimiento: new FormControl(''),
      numero_orden_trabajo: new FormControl(''),
      fecha_emicion: new FormControl(''),
      ubicacion_activo: new FormControl(''),
      cod_atentido: new FormControl(''),
      cod_estado: new FormControl(''),
      solicitudes_servicio: this.formBuilder.group({
        seq_solicitud_servicio: new FormControl(''),
        fecha: new FormControl(''),
        personas: this.formBuilder.group({
          nombres: new FormControl(''),
          apellido_paterno: new FormControl('')
        })
      })
    });
    /*---tecnico ejecutante---*/

    this.tecForm = this.formBuilder.group({
      seq_tecnicos_ejecutantes: new FormControl(''),
      fecha_de_asignacion: new FormControl(''),
      cod_responsable: new FormControl(''),
      fecha_emicion: new FormControl(''),
      cod_estado: new FormControl(''),
      ordenes_trabajo: this.formBuilder.group({
        seq_orden_trabajo: new FormControl(''),
      }),
      personas: this.formBuilder.group({
        nombres: new FormControl(''),
        apellido_paterno: new FormControl('')
      })
    });
    /*-----orden de trabajo activo---*/
    this.ordenForm = this.formBuilder.group({
      activos: this.formBuilder.group({
        seq_activo: new FormControl(''),
        id_etiqueta_fisica: new FormControl(''),
        nombre: new FormControl(''),
        cod_estado: new FormControl()
      }),
      ordenes_trabajo: this.formBuilder.group({
        seq_orden_trabajo: new FormControl(''),
      })
    });
    //termina aqui orden de trabajo
  }
  ListaTrabajo: Trabajo[] = [];
  ListarTrabajo() {
    this.trabService.getListaTrabajo().subscribe((xsol) => (this.ListaTrabajo = xsol))
  }
  ListaSoli: SolicitudesServicio[] = [];
  ListarSoli() {
    this.soliService.getListaSoli().subscribe((xsol) => (this.ListaSoli = xsol))
  }
  ListaTecnico: Tecnico[] = [];
  ListarTecnico() {
    this.tecService.getListaTecnico().subscribe((xsol) => (this.ListaTecnico = xsol))
  }
  ListaOrden: Trabajoactivo[] = [];
  ListarOrden() {
    this.ordentraService.getListaOrdenTrabajo().subscribe((xsol) => (this.ListaOrden = xsol))
  }
  ListaRece: Recepactivo[] = [];
  ListarRece() {
    this.ReceActService.getListaReceActivo().subscribe((xsol) => (this.ListaRece = xsol))
  }
  //listar persona
  xper1!: any;
  ListaPersonaNoCliente: Persona[] = [];
  ListarPersonaNoCliente() {
    this.ReceActService.getListarPersonaNoCliente().subscribe((xper) => {
      // Filtra las personas con cod_estado igual a 1 y elimina duplicados
      const personasFiltradas = xper.filter(perso => perso.cod_estado === 1);
      const personasUnicas = this.eliminarDuplicados(personasFiltradas);
      this.ListaPersonaNoCliente = personasUnicas;
    });
  }
  
  eliminarDuplicados(personas: Persona[]): Persona[] {
    const personasSet = new Set(personas.map(perso => JSON.stringify(perso)));
    return Array.from(personasSet).map(persoString => JSON.parse(persoString));
  }
  xactivos!: any;
  ListaActivos: Activos[] = []
  ListarActivos() {
    this.actiService.getListaActivos().subscribe((xsol) => {
      this.ListaActivos = xsol.filter(solo => solo.cod_estado === 1);
    });
  }


  ngOnInit(): void {
    this.ListarTrabajo()
    this.ListarSoli();
    this.ListarTecnico();
    this.ListarOrden();
    this.ListarPersonaNoCliente();
    this.inicializarFechas();
    this.ListarActivos();
    this.inicializarFechas5();
    this.ListarRece()
  }

  fechaDesde: string = "";
  inicializarFechas() {
    const fechaActual = new Date().toISOString().split('T')[0];
    this.fechaDesde = fechaActual;
  }
  // Función para mostrar la segunda parte al hacer clic en el botón de editar
  mostrarEdicion: boolean = false;
  cancelarEdicion() {
    this.mostrarEdicion = false;
    this.ListarTrabajo();


  }

  
  personaNoCliente1: any
  selectPersonaCli1(event: any) {
    const selectedPersonaId = event.target.value;
    console.log("ID de la persona seleccionada:", selectedPersonaId);
    this.personaNoCliente1 = this.ListaSoli.find(persona => persona.seq_solicitud_servicio == selectedPersonaId);
    console.log("Persona seleccionada:", this.personaNoCliente1);
    if (this.personaNoCliente1) {
      console.log("Nombre de la persona:", this.personaNoCliente1.fecha);
      // Establece el valor seleccionado en el formulario
      this.trabForm.patchValue({
        solicitudes_servicio: this.personaNoCliente1
      });
    }
  }


  //es para seleccionar tipo de mantenimiento
  tipoMantenimientoSeleccionado: any;
  updateTipoMantenimiento(event: any) {
    this.tipoMantenimientoSeleccionado = event.target.value;
  }
  //es para seleccionar atendido y combierte 1 y 0 para guardar
  updateAtendido(event: any) {
    const selectedValue = event.target.value;
    // Asigna 0 si es 'noatendido', 1 si es 'atendido'
    this.trabForm.get('cod_atentido')?.setValue(selectedValue === 'atendido' ? 1 : 0);
    console.log('Form Value after update:', this.trabForm.get('cod_atentido')?.value);
}
  solicitudActual: any;
  modificarTrabajo(xdat: Trabajo) {
    this.personaNoCliente1 = this.ListaSoli.find(persona => persona.seq_solicitud_servicio == xdat.solicitudes_servicio?.seq_solicitud_servicio);
    this.tecService.getListaTecnicoSegunTrab(xdat).subscribe((resp) => { this.ListaTecnico = resp })
    this.ordentraService.getListaOrdenSegunTrab(xdat).subscribe((resp) => { this.ListaOrden = resp })
    if (xdat.cod_estado === 1) {
      this.trabService.getSolicitudCompleta(xdat.seq_orden_trabajo).subscribe(
        (solicitudCompleta) => {
          this.solicitudActual = solicitudCompleta;
        }
      );
    }
    // Modifica la lógica para establecer la opción seleccionada en el select
    const atendidoOption = document.getElementById('selee') as HTMLSelectElement;
    atendidoOption.value = xdat.cod_atentido.toString();

    this.trabForm.patchValue({
      seq_orden_trabajo: xdat.seq_orden_trabajo,
      cod_tipo_mantenimiento: xdat.cod_tipo_mantenimiento,
      numero_orden_trabajo: xdat.numero_orden_trabajo,
      fecha_emicion: xdat.fecha_emicion,
      ubicacion_activo: xdat.ubicacion_activo,
      cod_atentido: xdat.cod_atentido.toString(),
      personas: xdat.personas,
      solicitudes_servicio: xdat.solicitudes_servicio,
      cod_estado: xdat.cod_estado
    });
  }
  modTra() {
    const ultimoNumero = this.obtenerUltimoNumeroOrdenTrabajo();
    const nuevoNumero = ultimoNumero + 1;
    this.xtrab = {
      seq_orden_trabajo: this.trabForm.get('seq_orden_trabajo')?.value,
      cod_tipo_mantenimiento: this.trabForm.get('cod_tipo_mantenimiento')?.value,
      numero_orden_trabajo: this.trabForm.get('numero_orden_trabajo')?.value,
      fecha_emicion: this.trabForm.get('fecha_emicion')?.value,
      ubicacion_activo: this.trabForm.get('ubicacion_activo')?.value,
      personas: this.personaNoCliente1?.personas,
      solicitudes_servicio: this.personaNoCliente1,
      cod_atentido: this.trabForm.get('cod_atentido')?.value,
      cod_estado: this.trabForm.get('cod_estado')?.value,
    }

    this.trabService.modtrab(this.xtrab).subscribe(() => {
      this.ListarTrabajo(); // Vuelve a cargar los datos después de guardar
      this.mostrarEdicion = false; // Oculta el módulo de edición
    });

  }
  private obtenerUltimoNumeroOrdenTrabajo(): number {
    return this.ListaTrabajo.length > 0 ? +this.ListaTrabajo[this.ListaTrabajo.length - 1].numero_orden_trabajo.replace('OT-', '') : 0;
  }





  //eliminar y activar estado de recepciones activos
// Declarar una variable para controlar el estado del botón
habilitarGuardar5 = false;
elimnarGuardar5 = false;
perInfo11 = '';
ReceEstado!: Trabajo;
MandaEstadoRece5(xrece: Trabajo) {
  this.perInfo11 = xrece.cod_tipo_mantenimiento
  this.ReceEstado = xrece;
  console.log("ingresa aqui:" + this.perInfo11);
  
  // Actualizar la variable habilitarGuardar según la condición
  this.habilitarGuardar5 = this.ReceEstado.cod_estado === 0;
  this.elimnarGuardar5 = this.ReceEstado.cod_estado === 1;
}

HabilitarEstado5() {
  console.log("paso"); 
  if (this.ReceEstado.cod_estado == 0) {
    this.trabService.modificarEstadoTra(this.ReceEstado).subscribe((xper) => {
      this.trabService.getListaTrabajo().subscribe(
        (resp)=>{this.ListaTrabajo=resp}
      ) 
    });
    
  }
}

  /*------------elimina estado persona-------------*/
  ;
  EliminarEstado5() {
    if (this.ReceEstado.cod_estado == 1) {
      this.trabService.modificarEstadoTra(this.ReceEstado).subscribe((xper) => {
        this.trabService.getListaTrabajo().subscribe(
          (resp)=>{this.ListaTrabajo=resp}
        ) 
      });  
    }
  }

  /* ------------------- activar inactivar ala persona ----------------*/
  visualizar: string = 'todos'
  cambiarFiltro(nombreRol: string) {
    if (this.visualizar === 'activo' && nombreRol === "x") {
      console.log(this.visualizar, nombreRol)
      this.trabService.FiltrarActivoInactivo(1)
        .subscribe((persona) => {
          this.trabService.FiltrarActivoInactivo(1).subscribe((per) => (this.ListaTrabajo = per))
          console.log(this.ListaTrabajo);
        })
    }  //entra por primer else y inactiva ala persona
    else if (this.visualizar === 'inactivo' && nombreRol === "x") {
      console.log(this.visualizar, nombreRol)
      this.trabService.FiltrarActivoInactivo(0)
        .subscribe((persona) => {
          this.trabService.FiltrarActivoInactivo(0).subscribe((per) => (this.ListaTrabajo = per))
          console.log(this.ListaTrabajo);

        })
      /*-----else para visuaizar todos las personas ---*/
    } else if (this.visualizar === 'todos' && nombreRol === "x") {
      console.log(this.visualizar, nombreRol)
      this.trabService.getListaTrabajo()
        .subscribe((fallas) => {
          this.trabService.getListaTrabajo().subscribe((per) => (this.ListaTrabajo = per))
          console.log(this.ListaTrabajo);
        })
    }
  }
  /*----filtrar por fecha desde y hasta ----*/
 fechaHasta5: string = ""; 
 fechaDesde5: string = ""; 
 inicializarFechas5() {
  const fechaActual = new Date().toISOString().split('T')[0];
  this.fechaDesde5 = fechaActual;
  this.fechaHasta5 = fechaActual;
 }
 ListarSoliPorFecha5() {
  const fechaDesde5 = (document.getElementById('fechaDesde5') as HTMLInputElement).value;
  const fechaHasta5 = (document.getElementById('fechaHasta5') as HTMLInputElement).value;

  if (fechaDesde5 && fechaHasta5) {
    this.trabService.listaTraPorFecha(new Date(fechaDesde5), new Date(fechaHasta5)).subscribe((xsol) => {
      this.ListaTrabajo = xsol;
    });
  }
 }
 







































  /*-----------------------------------TABLA TECNICO EJECUTANTE-----*/
  /*------------Seleccionar persona no cliente---*/
  personaNoCliente: any
  selectPersonaNoCliente(event: any) {
    const selectedPersonaId = event.target.value;
    this.personaNoCliente = this.ListaPersonaNoCliente.find(persona => persona.seq_persona == selectedPersonaId);
    if (this.personaNoCliente) {

    }
  }
  //es para seleccionar atendido y combierte 1 y 0 para guardar
  updateResponsable(event: any) {
    const selectedValue1 = event.target.value;
    this.tecForm.get('cod_responsable')?.setValue(selectedValue1 === 'si' ? 1 : 0);
  }
  AddTecnico() {
    const selectedAtendidoValue1 = this.tecForm.get('cod_responsable')?.value;
    this.xtec = {
      seq_tecnicos_ejecutantes: '',
      fecha_de_asignacion: this.tecForm.get('fecha_de_asignacion')?.value,
      personas: this.personaNoCliente,
      cod_responsable: selectedAtendidoValue1,
      ordenes_trabajo: this.solicitudActual,
      cod_estado: 1,
    };

    console.log("Objeto xrece antes de la solicitud:", this.xtec); // Agrega este console.log
    this.tecService.addTecnico(this.xtec)
      .subscribe((rece) => {
        this.tecService.getListaTecnicoSegunTrab(this.solicitudActual).subscribe((xrece) => (this.ListaTecnico = xrece))
        this.trabForm.reset();
      });
  }
  getData(): any {
    return localStorage.getItem('currentUser')
  }
/*------------elimina estado persona-------------*/
habilitarGuardar12 = false;
elimnarGuardar12 = false;
tecEstado!: Tecnico;
xinfoo='';
xtecc!: any;
MandaEstadoTec(xtecc: Tecnico) {
  this.xinfoo = xtecc.cod_estado+'';
  this.tecEstado = xtecc;
  console.log("ingresa aqui:" + this.xinfoo);
  
  this.elimnarGuardar12 = this.tecEstado.cod_estado === 1;
}
EliminarEstado12() {
  if (this.tecEstado.cod_estado == 1) {
    this.tecService.modificarEstadoTecnico(this.tecEstado).subscribe((xper) => {
      this.tecService.getListaTecnicoSegunTrab(this.solicitudActual).subscribe(
        (resp)=>{this.ListaTecnico=resp}
      ) 
    });
    
  }
}
  //adiconar orden de trabajo activoooooooooooooo---------
  activosSele: any
  selectActivos(event: any) {
    const selectedPersonaId2 = event.target.value;
    this.activosSele = this.ListaActivos.find(persona => persona.seq_activo == selectedPersonaId2);
    if (this.activosSele) {

    }
  }
  xorden1!: any;
  AdicionarOrdentraActivo() {
    this.xorden1 = {
      ordenes_trabajo: this.solicitudActual,
      activos: this.activosSele
    };
    console.log("datos aquiiiiiiiii:::::", this.xorden1);
    this.ordentraService.addOrdenActivo(this.xorden1)
      .subscribe((rece) => {
        this.ordentraService.getListaOrdenSegunTrab(this.solicitudActual).subscribe((xrece) => (this.ListaOrden = xrece))
        this.ordenForm.reset();
      });
  }

/*------------elimina estado persona-------------*/

elimnarGuardar13 = false;
ordenEstado!: Trabajoactivo;
xinfoo1='';
xor!: any;
MandaEstadoOrden(xor: Trabajoactivo) {
  this.xinfoo1 = xor.activos?.cod_estado+'';
  this.ordenEstado = xor;
  this.elimnarGuardar13 = this.ordenEstado.activos?.cod_estado === 1;
}
EliminarEstado13() {
  if (this.ordenEstado.activos?.cod_estado == 1) {
    this.ordentraService.modificarEstadoOrdenTraActivo(this.ordenEstado).subscribe((xper) => {
      this.ordentraService.getListaOrdenSegunTrab(this.solicitudActual).subscribe(
        (resp)=>{this.ListaOrden=resp}
      ) 
    });
    
  }
}
}