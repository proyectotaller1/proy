import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { ModulomantenimientoModule } from './modulomantenimiento.module';
import { ModulomantenimientoComponent } from './modulomantenimiento.component';
import { RegistrarComponent } from './registrar/registrar.component';
import { ListaComponent } from './lista/lista.component';
const routes: Routes = [
  /*{path: 'lista',component: ListaComponent },
  {path: 'registrar',component: RegistrarComponent }, */
  { path:'', component:ModulomantenimientoComponent, 
  children:[
    {path:'registrar',component:RegistrarComponent}
  ]
},
  { path:'', component:ModulomantenimientoComponent, 
  children:[
    {path:'listar',component:ListaComponent}
  ]
},

];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ModulomantenimientoRoutingModule { }
