import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ModulomantenimientoComponent } from './modulomantenimiento.component';

describe('ModulomantenimientoComponent', () => {
  let component: ModulomantenimientoComponent;
  let fixture: ComponentFixture<ModulomantenimientoComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ModulomantenimientoComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(ModulomantenimientoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
