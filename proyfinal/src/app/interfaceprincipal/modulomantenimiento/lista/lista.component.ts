import { Component, OnInit, ElementRef, ViewChild} from '@angular/core';
import { FormControl, Validators, FormGroup, NgModel } from '@angular/forms';
import { Observable, finalize } from 'rxjs';
import { HttpClient } from '@angular/common/http';
import { FormBuilder } from '@angular/forms'
import { TrabajoService } from 'src/app/services/trabajo.service';
import { Trabajo } from 'src/app/model/trabajo';
import { SolicitudesServicio } from 'src/app/model/solicitudes-servicio';
import { SolicitudesServicioService } from 'src/app/services/solicitudes-servicio.service';
import { Mantenimiento } from 'src/app/model/mantenimiento';
import { MantenimientoService } from 'src/app/services/mantenimiento.service';
import { Fallas } from 'src/app/model/fallas';
import { FallasService } from 'src/app/services/fallas.service';
import jsPDF from 'jspdf';
import * as jspdf from 'jspdf';
import html2canvas from 'html2canvas';
@Component({
  selector: 'app-lista',
  templateUrl: './lista.component.html',
  styleUrls: ['./lista.component.css']
})
export class ListaComponent implements OnInit {
  @ViewChild('modalContent') modalContent!: ElementRef;

  filtroEstado: number = 2; // Inicia con Todos (2)

  filtrarTrabajos(estado: number) {
    this.filtroEstado = estado;
  }
  pag: number = 1;
  buscar = '';
  trabForm: any;
  manForm: any;
  xtrab!: any;
  xtec!: any;
  ordenForm: any;
  xorden!: any;
  constructor(private fallService: FallasService, private mantService: MantenimientoService, private soliService: SolicitudesServicioService, private trabService: TrabajoService, private formBuilder: FormBuilder,) {
    this.manForm = this.formBuilder.group({
      seq_mantenimiento_correctivo: new FormControl(''),
      fecha_finalizacion: new FormControl(''),
      comentario_deteccion_falla: new FormControl(''),
      duracion: new FormControl(''),
      descripcion_causa: new FormControl(''),
      cod_tipo_solucion: new FormControl(''),
      descripcion_solucion: new FormControl(''),
      cod_estado: new FormControl(''),
      comentarios: new FormControl(''),
      fallas: this.formBuilder.group({
        id_falla: new FormControl(''),
        nombre: new FormControl(''),
      }),
      ordenes_trabajo: this.formBuilder.group({
        seq_orden_trabajo: new FormControl(''),
        numero_orden_trabajo: new FormControl(''),
        solicitudes_servicio: this.formBuilder.group({
          fecha: new FormControl(''),
          personas: this.formBuilder.group({
            nombres: new FormControl(''),
            apellido_paterno: new FormControl('')
          })
        }),
      }),
    });

  }


  ListaMan: Mantenimiento[] = [];
  ListarMan() {
    this.mantService.getListaMantenimiento().subscribe((xsol) => (this.ListaMan = xsol))
  }

  ListaFalla: Fallas[] = [];
  ListarFalla() {
    this.fallService.getListaFallas().subscribe((xsol) => (this.ListaFalla = xsol))
  }

  ngOnInit(): void {
    this.ListarMan();
    this.inicializarFechas();
    this.ListarFalla();
  }

 
  // Función para mostrar la segunda parte al hacer clic en el botón de editar
  mostrarEdicion: boolean = false;
  cancelarEdicion() {
    this.mostrarEdicion = false;
    this.ListarMan();
  }
  //es para seleccionar tipo de mantenimiento
  tipoSeleccionado: any;
  updateSolucion(event: any) {
    this.tipoSeleccionado = event.target.value;
  }
  fallForm: any;

  personaNoCliente1: any
  selectPersonaCli1(event: any) {
    const selectedPersonaId = event.target.value;
    console.log("ID de la persona seleccionada:", selectedPersonaId);
    this.personaNoCliente1 = this.ListaFalla.find(persona => persona.id_falla == selectedPersonaId);
    console.log("Persona seleccionada:", this.personaNoCliente1);
    if (this.personaNoCliente1) {
      console.log("Nombre de la persona:", this.personaNoCliente1.nombre);
      // Establece el valor seleccionado en el formulario
      this.manForm.patchValue({
        fallas: this.personaNoCliente1
      });
    }
  }


  xman!: any;
  modificarMan(xdat: Mantenimiento) {
    this.personaNoCliente1 = this.ListaFalla.find(persona => persona.id_falla == xdat.fallas?.id_falla);
    this.manForm.patchValue({
      seq_mantenimiento_correctivo: xdat.seq_mantenimiento_correctivo,
      fecha_finalizacion: xdat.fecha_finalizacion,
      comentario_deteccion_falla: xdat.comentario_deteccion_falla,
      duracion: xdat.duracion,
      cod_tipo_solucion: xdat.cod_tipo_solucion,
      descripcion_causa: xdat.descripcion_causa,
      descripcion_solucion: xdat.descripcion_solucion,
      comentarios: xdat.comentarios,
      ordenes_trabajo: {
        numero_orden_trabajo: xdat.ordenes_trabajo?.numero_orden_trabajo || '',
        solicitudes_servicio: {
          fecha: xdat.ordenes_trabajo?.solicitudes_servicio?.fecha || ''
        }
      },
      fallas: xdat.fallas,
      cod_estado: xdat.cod_estado
    })
    // this.faForm1.controls.id_falla.disable();

  }
  modMant() {
    const ordenesTrabajo = {
      numero_orden_trabajo: this.manForm.get('ordenes_trabajo.numero_orden_trabajo')?.value || '',
      solicitudes_servicio: {
        fecha: this.manForm.get('ordenes_trabajo.solicitudes_servicio.fecha')?.value || ''
      }
    };
    this.xman = {
      seq_mantenimiento_correctivo: this.manForm.get('seq_mantenimiento_correctivo')?.value,
      fecha_finalizacion: this.manForm.get('fecha_finalizacion')?.value,
      comentario_deteccion_falla: this.manForm.get('comentario_deteccion_falla')?.value,
      duracion: this.manForm.get('duracion')?.value,
      cod_tipo_solucion: this.manForm.get('cod_tipo_solucion')?.value,
      descripcion_causa: this.manForm.get('descripcion_causa')?.value,
      descripcion_solucion: this.manForm.get('descripcion_solucion')?.value,
      comentarios: this.manForm.get('comentarios')?.value,
      ordenes_trabajo: {
        seq_orden_trabajo: 2
      }, //aqui traelo ese objeto que sacaste
      fallas: this.personaNoCliente1,
      cod_estado: this.manForm.get('cod_estado')?.value,
    }
    this.mantService.modmantenimiento(this.xman).subscribe(() => {
      this.ListarMan(); // Vuelve a cargar los datos después de guardar
      this.mostrarEdicion = false; // Oculta el módulo de edición
    });
  }

  //eliminar y activar estado de recepciones activos
// Declarar una variable para controlar el estado del botón
habilitarGuardar1 = false;
elimnarGuardar1 = false;
perInfo = '';
ReceEstado!: Mantenimiento;
MandaEstadoRece(xrece: Mantenimiento) {
  this.perInfo = xrece.comentarios
  this.ReceEstado = xrece;
  console.log("ingresa aqui:" + this.perInfo);
  
  // Actualizar la variable habilitarGuardar según la condición
  this.habilitarGuardar1 = this.ReceEstado.cod_estado === 0;
  this.elimnarGuardar1 = this.ReceEstado.cod_estado === 1;
}

HabilitarEstado1() {
  console.log("paso"); 
  if (this.ReceEstado.cod_estado == 0) {
    this.mantService.modificarEstadoMan(this.ReceEstado).subscribe((xper) => {
      this.mantService.getListaMantenimiento().subscribe(
        (resp)=>{this.ListaMan=resp}
      ) 
    });
    
  }
}

  /*------------elimina estado persona-------------*/
  ;
  EliminarEstado1() {
    if (this.ReceEstado.cod_estado == 1) {
      this.mantService.modificarEstadoMan(this.ReceEstado).subscribe((xper) => {
        this.mantService.getListaMantenimiento().subscribe(
          (resp)=>{this.ListaMan=resp}
        ) 
      });  
    }
  }

  /* ------------------- activar inactivar ala persona ----------------*/
  visualizar: string = 'todos'
  cambiarFiltro(nombreRol: string) {
    if (this.visualizar === 'activo' && nombreRol === "x") {
      console.log(this.visualizar, nombreRol)
      this.mantService.listaManEstado(1)
        .subscribe((persona) => {
          this.mantService.listaManEstado(1).subscribe((per) => (this.ListaMan = per))
          console.log(this.ListaMan);
        })
    }  //entra por primer else y inactiva ala persona
    else if (this.visualizar === 'inactivo' && nombreRol === "x") {
      console.log(this.visualizar, nombreRol)
      this.mantService.listaManEstado(0)
        .subscribe((persona) => {
          this.mantService.listaManEstado(0).subscribe((per) => (this.ListaMan = per))
          console.log(this.ListaMan);

        })
      /*-----else para visuaizar todos las personas ---*/
    } else if (this.visualizar === 'todos' && nombreRol === "x") {
      console.log(this.visualizar, nombreRol)
      this.mantService.getListaMantenimiento()
        .subscribe((fallas) => {
          this.mantService.getListaMantenimiento().subscribe((per) => (this.ListaMan = per))
          console.log(this.ListaMan);
        })
    }
  }

  /*----filtrar por fecha desde y hasta ----*/
 fechaHasta: string = ""; 
 fechaDesde: string = ""; 
 inicializarFechas() {
  const fechaActual = new Date().toISOString().split('T')[0];
  this.fechaDesde = fechaActual;
  this.fechaHasta = fechaActual;
 }
 ListarSoliPorFecha() {
  const fechaDesde = (document.getElementById('fechaDesde') as HTMLInputElement).value;
  const fechaHasta = (document.getElementById('fechaHasta') as HTMLInputElement).value;

  if (fechaDesde && fechaHasta) {
    this.mantService.listaManPorFecha(new Date(fechaDesde), new Date(fechaHasta)).subscribe((xsol) => {
      this.ListaMan = xsol;
    });
  }
}

 /*------------imprimir-----------------*/
 infonombre='';
 infoap='';
 infoam = '';
 infocedula = '';
 infoid = '';
 infofotografia='';
 infotelefono = '';
 infotelefono1 = '';
 infoestado = '';
 persona1!: Mantenimiento;
 ImprimirDato(per: Mantenimiento) {
   this.infonombre = per.ordenes_trabajo?.solicitudes_servicio?.personas?.nombres+'';
   this.infoap = per.ordenes_trabajo?.solicitudes_servicio?.personas?.apellido_materno+'';
   this.infoam = per.descripcion_causa;
   this.infocedula = per.descripcion_solucion;
   this.infoestado = per.cod_estado + '';
   this.infofotografia = per.fallas?.nombre+'';
   this.infotelefono = per.fecha_finalizacion+'';
   this.infotelefono1 = per.ordenes_trabajo?.solicitudes_servicio?.fecha+'';
   this.infoid = per.seq_mantenimiento_correctivo + '';
   this.persona1 = per;
   console.log("ingresa aqui:" + this.perInfo);
 }
 generarPDF() {
   const modal = this.modalContent.nativeElement;

   html2canvas(modal, {
     useCORS: true, // Habilitar CORS
   }).then((canvas) => {
     const pdf = new jspdf.jsPDF('p', 'px', 'a4');
     const pdfWidth = pdf.internal.pageSize.getWidth();
     const pdfHeight = pdf.internal.pageSize.getHeight();
     const imageWidth = canvas.width * 0.75;
     const imageHeight = canvas.height * 0.75;
     const position = 0;

     pdf.addImage(canvas.toDataURL('image/png'), 'PNG', 0, position, imageWidth, imageHeight);

     pdf.save('contenido_modal.pdf');
   });
 }
}
