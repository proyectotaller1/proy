import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';
import { ReactiveFormsModule, FormsModule } from '@angular/forms';
import { NgxPaginationModule } from 'ngx-pagination';
import { ModulomantenimientoRoutingModule } from './modulomantenimiento-routing.module';
import { ListaComponent } from './lista/lista.component';
import { RegistrarComponent } from './registrar/registrar.component';
import { FiltromanPipe } from 'src/app/components/pipes/filtroman.pipe';
import { FiltromanestadoPipe } from 'src/app/components/pipes/filtromanestado.pipe';

@NgModule({
  declarations: [
    ListaComponent,
    RegistrarComponent,
    FiltromanPipe,
    FiltromanestadoPipe
  ],
  imports: [
    CommonModule,
    ModulomantenimientoRoutingModule,
    RouterModule,
    ReactiveFormsModule,
    FormsModule,
    NgxPaginationModule,
  ]
})
export class ModulomantenimientoModule { }
