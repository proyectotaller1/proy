import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { InterfaceprincipalRoutingModule } from './interfaceprincipal-routing.module';
import { HomeComponent } from './home/home.component';

import { InterfaceprincipalComponent } from './interfaceprincipal.component';
import { CabeceraComponent } from './cabecera/cabecera.component';
import { MenuComponent } from './menu/menu.component';
import { PieComponent } from './pie/pie.component';
import { RouterModule } from '@angular/router';
import { NgxPaginationModule } from 'ngx-pagination';
import { ListarComponent } from './listar/listar.component';
import { FiltroPipe } from '../components/pipes/filtro.pipe';
import { ReactiveFormsModule } from '@angular/forms';
import { FormsModule } from '@angular/forms';
import { FiltrofallasPipe } from '../components/pipes/filtrofallas.pipe';
import { FiltrousuariosPipe } from '../components/pipes/filtrousuarios.pipe';
import { FiltroactivosPipe } from '../components/pipes/filtroactivos.pipe';
import { FiltrosolicitudesServicioPipe } from '../components/pipes/filtrosolicitudes-servicio.pipe';
//Firebase
import { AngularFireAuthModule } from '@angular/fire/compat/auth';
import { AngularFireStorageModule } from '@angular/fire/compat/storage';
import { AngularFireModule } from '@angular/fire/compat';
import { AngularFireDatabaseModule } from '@angular/fire/compat/database';
import { environment } from 'src/environments/environment';
import { ModulofallasComponent } from './modulofallas/modulofallas.component';
import { ModulousuariorolComponent } from './modulousuariorol/modulousuariorol.component';
import { FiltrorolPipe } from '../components/pipes/filtrorol.pipe';
import { ModuloactivosComponent } from './moduloactivos/moduloactivos.component';
import { ModulosolicitudserComponent } from './modulosolicitudser/modulosolicitudser.component';
import { ModulotrabajoComponent } from './modulotrabajo/modulotrabajo.component';

import { ModulomantenimientoComponent } from './modulomantenimiento/modulomantenimiento.component';
@NgModule({
  declarations: [
    HomeComponent,
    InterfaceprincipalComponent,
    CabeceraComponent,
    MenuComponent,
    PieComponent,
    ListarComponent,
    FiltroPipe,
    FiltrofallasPipe,
    FiltrousuariosPipe,
    FiltrorolPipe,
    ModulofallasComponent,
    ModulousuariorolComponent,
    ModuloactivosComponent,
    ModulosolicitudserComponent,
    ModulotrabajoComponent,
    ModulomantenimientoComponent
  
    
  ],
  imports: [
    CommonModule,
    InterfaceprincipalRoutingModule,
    RouterModule,
    ReactiveFormsModule,
    FormsModule,
    NgxPaginationModule,
  
    AngularFireModule.initializeApp(environment.firebase),
    AngularFireStorageModule,
    AngularFireAuthModule,
    AngularFireDatabaseModule
   
    
  ]
})
export class InterfaceprincipalModule { }
