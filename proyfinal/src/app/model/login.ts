import { Persona } from "./persona";
export interface Login{
    usuario: string;
    contrasena: string;
    cod_estado: number;
    personas?:Persona;
    token?: string;
}