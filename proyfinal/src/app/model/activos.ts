
export interface Activos {
    seq_activo: number;
    id_etiqueta_fisica: string;
    nombre: string;
    marca: string
    modelo: string;
    comentarios: string;
    cod_estado: number;
}