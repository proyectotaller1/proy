import { Trabajo } from "./trabajo";
import { Persona } from "./persona";
export interface Tecnico {
    seq_tecnicos_ejecutantes: number;
    fecha_de_asignacion: Date;
    cod_responsable: number;
    ordenes_trabajo?: Trabajo;
    personas?: Persona;
    cod_estado: number
}
