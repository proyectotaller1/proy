import { Activos } from "./activos";
import { SolicitudesServicio } from "./solicitudes-servicio";
import { Persona } from "./persona";
export interface Recepactivo {
    seq_recepcion_activo: number,
	fecha: Date,
	cod_estado: number
    personas?: Persona,
    activos?: Activos,
    solicitudes_servicio?: SolicitudesServicio,
    

}
