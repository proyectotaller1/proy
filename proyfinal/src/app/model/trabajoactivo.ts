import { Activos } from "./activos";
import { Trabajo } from "./trabajo";
export interface Trabajoactivo {
    activos?: Activos;
    ordenes_trabajo?: Trabajo
}
