import { Login } from "./login";
export interface Roles {
    id_rol: number;
    nombre: string;
    cod_estado?: number;
    login?: Login
}
