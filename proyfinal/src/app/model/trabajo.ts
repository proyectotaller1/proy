import { Persona } from "./persona"
import { SolicitudesServicio } from "./solicitudes-servicio"
export interface Trabajo {
	seq_orden_trabajo: number,
	cod_tipo_mantenimiento: string,
	numero_orden_trabajo: string,
    fecha_emicion: Date,
	ubicacion_activo: string,
	cod_atentido: number,
    personas?: Persona,
    solicitudes_servicio?: SolicitudesServicio 
	cod_estado: number
}
