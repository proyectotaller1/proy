import { Trabajo } from "./trabajo";
import { Fallas } from "./fallas";
export interface Mantenimiento {
    seq_mantenimiento_correctivo: number;
    fecha_finalizacion: Date;
    comentario_deteccion_falla: string;
    descripcion_causa: string;
    cod_tipo_solucion: string;
    descripcion_solucion: string;
    duracion: string;
    comentarios: string;
    cod_estado: number;
    fallas?: Fallas;
    ordenes_trabajo?: Trabajo
}
