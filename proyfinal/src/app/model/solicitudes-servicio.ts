import { Persona } from "./persona"
export interface SolicitudesServicio {
    seq_solicitud_servicio: number,
    fecha: Date,
    descripcion_problema: string,
    cod_estado: number,
    personas?:Persona; 
}
