import { Injectable } from '@angular/core';
import { HttpClient} from '@angular/common/http';
import { Login } from '../model/login';
import { Observable } from 'rxjs';
import { Persona } from '../model/persona';


@Injectable({
  providedIn: 'root'
})
export class UsuarioService {

  private apiURL = '/api'
  constructor( private http:HttpClient) { }

  /*-------------------alistar usuarios -------------*/
getListUserPer(): Observable<Login[]>{
  return this.http.get<Login[]>(this.apiURL+"/lisusu")

} 

/*--------------------Listar Persona esto para ver si tiene usuario o no-------------------*/
lisarPersonaUsu(per: Persona): Observable<any>{
  console.log("llega persona: "+per.nombres+"  ID: "+per.seq_persona);
  return this.http.get<any>(this.apiURL+"/datoPer/"+ per.seq_persona)

} 
/*---------------adicionar usuario ------------------------*/

AddUser(per:Login): Observable<any> {
  return this.http.post<Login>(this.apiURL+"/addusu", per);
}
/*-----modificar usuario-----*/
modUsuario(usu: Login): Observable<any> {
  const modURL = this.apiURL + "/modusu/" + usu.usuario;
    return this.http.put<Login>(modURL, usu);
    
  }


}
