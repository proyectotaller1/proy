import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import {Observable} from 'rxjs';
import { Roles } from '../model/roles';

@Injectable({
  providedIn: 'root'
})
export class EusuariosService {
 private clave="usukey";
 private sharedData: any
  constructor( private http: HttpClient) { }
  private dato: any
  guardarDato(data:any){
    this.sharedData= data
    localStorage.setItem(this.clave, JSON.stringify(data))
  }
  ObtenerDato(){
    const storedData=localStorage.getItem(this.clave)
    return storedData? JSON.parse(storedData):null
  }
  llamarRoles(idRol:number): Observable<Roles[]>{
    return this.http.get<Roles[]>("/api/lisrol"+idRol)
  }
}
