import { Injectable } from '@angular/core';
import { Login } from '../model/login';
import { Observable } from 'rxjs'; //importamos
import { HttpClient } from '@angular/common/http';
import { Persona } from '../model/persona';
import { HttpHeaders } from '@angular/common/http';
import { AuthService } from '@auth0/auth0-angular';
import { LoginService } from './login.service';
import { get } from 'jquery';

@Injectable({
  providedIn: 'root'
})
export class PersonaService {
  
  private apiURL = '/api'
  private token?: string; 
  private personas : Persona[];
  constructor(private http: HttpClient, private authService: LoginService) { 
    this.personas = [];
  }
  //listar persona

  getListaPersona(): Observable<Persona[]>{
    return this.http.get<Persona[]>(this.apiURL+"/lisper")
  } 
  
  //adicionar persona
  addPersona(per: Persona): Observable<any> {
    return this.http.post<Persona>(this.apiURL+"/addper", per);
  }
  //eliminar persona

   delPersona(seq_persona: number): Observable<any> {
   
    const sistemUsersUrl = `${this.apiURL+"/delper"}/${seq_persona}`;
    return this.http.delete(sistemUsersUrl);
  }
  //modificar persona

  modPersona(per: Persona): Observable<any> {
    const modURL = this.apiURL + "/modper/" + per.seq_persona;
      return this.http.put<Persona>(modURL, per);
      
    }
 
  listaPersonasEstado(cod_estado: number): Observable<any> {
    const listaPorEstado = this.apiURL+"/lispe/"+cod_estado;
    return this.http.get(listaPorEstado);
  }
  /*----------modficar persona estado ----*/
  modificarEstadoPersona( per:Persona): Observable<any> {
    let url = this.apiURL + "/modPerEst/"+ per.seq_persona;  
    return this.http.put(url, per);
  } 
  //aqui incorporalo   

}