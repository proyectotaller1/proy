import { Injectable } from '@angular/core';
import {HttpRequest,HttpHandler,HttpEvent,HttpErrorResponse,HttpInterceptor} from '@angular/common/http';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
/*export class JwtService  {

  constructor() { }
  
  }
*/
export class JwtService implements HttpInterceptor {
  
  constructor() { }
   
  intercept(request: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
    
    const  per = this.getData()
    const token=JSON.parse(per).token; 
if(!this.isLoggin(request.url)){
    console.log('paso el interceptor');
    console.log(token);
    request = request.clone({
         setHeaders:
          {Authorization:  `Bearer ${token}`}
      });
    }
   return next.handle(request)
    
   }
  
  getData():any {
    return localStorage.getItem('currentUser')
 }
 isLoggin(url:String):boolean{
  return url.search('/api/login') != -1
 }
}

