import { Injectable } from '@angular/core';
import { HttpClient, } from '@angular/common/http';
import { Activos } from '../model/activos';
import { Observable } from 'rxjs'; //importamos

@Injectable({
  providedIn: 'root'
})
export class ActivosService {

  private apiURL = '/api'
  constructor(private http: HttpClient) {
  }

  //listar activos
  getListaActivos(): Observable<Activos[]> {
    return this.http.get<Activos[]>(this.apiURL + "/lisact")
  }
  //adicionar activos
  addActivos(xactivo: Activos): Observable<any> {
    return this.http.post<Activos>(this.apiURL + "/addact", xactivo);
  }

  //modificar activos
  modActivos(xactivo: Activos): Observable<any> {
    const modURL = this.apiURL + "/modact/" + xactivo.seq_activo;
      return this.http.put<Activos>(modURL, xactivo);
      
    }
  //listar estado activo inactivo y todos
  listaActivosEstado(cod_estado: number): Observable<any> {
    const listaPorEstado = this.apiURL+"/lisact/"+cod_estado;
    return this.http.get(listaPorEstado);
  }
  /*----------modficar persona estado ----*/
  modificarEstadoFallas( xactivo: Activos): Observable<any> {
    let url = this.apiURL + "/modActEst/"+ xactivo.seq_activo;  
    return this.http.put(url, xactivo);
  } 
}
