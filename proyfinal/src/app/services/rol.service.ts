import { Injectable } from '@angular/core';
import { Observable } from 'rxjs'; //importamos
import { HttpClient } from '@angular/common/http';
import { Roles } from '../model/roles';
import { Login } from '../model/login';

@Injectable({
  providedIn: 'root'
})
export class RolService {
  private apiURL = '/api'
  constructor(private http: HttpClient) { }
  /*------------listar roles----*/
  getListaRol(): Observable<Roles[]> {
    return this.http.get<Roles[]>(this.apiURL + "/lisrol")
  }
  /*---------------asignar rol a usuario------------------------*/

AsginarRolUsuario(usu: Login, rol: Roles): Observable<any> {
  return this.http.post<any>(this.apiURL+"/addcionarusurol/"+rol.id_rol, usu);
}
 /*---------------eliminamos rol a usuario------------------------*/
QuitarRolUsuario(usu: Login, rol: Roles): Observable<any> {
  const xrolusu= usu.usuario+" "+rol.id_rol
  return this.http.delete(this.apiURL+"/eliminarRolUsuario/"+xrolusu);
}
/*------------listar roles----*/
getObtenerRolUsuario(xusu: string): Observable<any[]> {
  return this.http.get<any[]>(this.apiURL + "/obetenerusurol/"+ xusu)
}

}
