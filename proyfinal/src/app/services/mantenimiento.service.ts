import { Injectable } from '@angular/core';
import { Observable } from 'rxjs'; //importamos
import { HttpClient,HttpParams } from '@angular/common/http';
import { Trabajo } from '../model/trabajo';
import { Mantenimiento } from '../model/mantenimiento';
@Injectable({
  providedIn: 'root'
})
export class MantenimientoService {

  private apiURL = '/api'

  constructor(private http: HttpClient) {

  }
  //listar solicitud

  getListaMantenimiento(): Observable<Mantenimiento[]> {
    return this.http.get<Mantenimiento[]>(this.apiURL + "/lismant")
  }
  
    modmantenimiento(fall: Mantenimiento): Observable<any> {
      const modURL = this.apiURL + "/modman/" + fall.seq_mantenimiento_correctivo;
      console.log("Objeto que llega para modificar:.:", fall);
        return this.http.put<Mantenimiento>(modURL, fall);
        
      }

      //listar activo inactivo o todos
  listaManEstado(cod_estado: number): Observable<any> {
    const listaPorEstado = this.apiURL + "/filtraractivoinac/" + cod_estado;
    return this.http.get(listaPorEstado);
  }
  /*----------modficar solicitud estado ----*/
  modificarEstadoMan(soli: Mantenimiento): Observable<any> {
    let url = this.apiURL + "/modManEst/" +  soli.seq_mantenimiento_correctivo;
    return this.http.put(url, soli);
  }

  /*filtrar fecha desde y hasta--*/
  listaManPorFecha(fechaDesde: Date, fechaHasta: Date): Observable<any> {
    const params = new HttpParams()
      .set('fechaDesde', fechaDesde.toISOString().split('T')[0]) //convercion 
      .set('fechaHasta', fechaHasta.toISOString().split('T')[0]);
    return this.http.get<any>(`${this.apiURL}/listfecha/fecha_finalizacion`, { params });
  }

}
