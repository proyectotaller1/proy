import { Injectable } from '@angular/core';
import { Observable } from 'rxjs'; //importamos
import { HttpClient } from '@angular/common/http';
import { Fallas } from '../model/fallas';

@Injectable({
  providedIn: 'root'
})
export class FallasService {
  private apiURL = '/api'
  
  constructor(private http: HttpClient) { 
   
  }
  //listar fallas

  getListaFallas(): Observable<Fallas[]>{
    return this.http.get<Fallas[]>(this.apiURL+"/lisfall")
  } 
  
  //adicionar fallas
  addFallas(fall: Fallas): Observable<any> {
    return this.http.post<Fallas>(this.apiURL+"/addfall", fall);
  }

  //modificar fallas

  modFallas(fall: Fallas): Observable<any> {
    const modURL = this.apiURL + "/modfall/" + fall.id_falla;
      return this.http.put<Fallas>(modURL, fall);
      
    }
 
  listaFallasEstado(cod_estado: number): Observable<any> {
    const listaPorEstado = this.apiURL+"/lisfall/"+cod_estado;
    return this.http.get(listaPorEstado);
  }
  /*----------modficar persona estado ----*/
  modificarEstadoFallas( fall: Fallas): Observable<any> {
    let url = this.apiURL + "/modFallEst/"+ fall.id_falla;  
    return this.http.put(url, fall);
  } 
}
