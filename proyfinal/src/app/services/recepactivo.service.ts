import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { Recepactivo } from '../model/recepactivo';
import { SolicitudesServicio } from '../model/solicitudes-servicio';
import { Persona } from '../model/persona';

@Injectable({
  providedIn: 'root'
})
export class RecepactivoService {

  private apiURL = '/api'
  constructor(private http: HttpClient) {
  }

  //listar activos
  getListaReceActivo(): Observable<Recepactivo[]> {
    return this.http.get<Recepactivo[]>(this.apiURL + "/lisrece")
  }
  //listar persona de tipo cliente ---------
  getListarPersonaCliente():Observable<any>{
    return this.http.get<any>(this.apiURL+'/lisPersonaCliente')
  }
  //listar persona de tipo cliente ---------
  getListarPersonaNoCliente():Observable<Persona[]>{
    console.log("personaaaa no cliente service :::" );
    
    return this.http.get<any>(this.apiURL+'/lisPersonaNoCliente')
  }
  //listar recepcion segun servicio
  getListaRecepcionActivo(solSer:SolicitudesServicio):Observable<any>{
    return this.http.get<any>(this.apiURL+'/ListarRecepcionActivos/'+solSer.seq_solicitud_servicio)
  }                                            
  getSolicitudCompleta(seqSolicitud: number): Observable<SolicitudesServicio> {
    const url = `${this.apiURL}/ObtenerSolicitudCompleta/${seqSolicitud}`;
    return this.http.get<SolicitudesServicio>(url);
  }
  //adicionar activos
   addReceActivo(xactivo: Recepactivo): Observable<any> {
    return this.http.post<Recepactivo>(this.apiURL+"/addrece", xactivo);
  }
  
  

  //modificar fallas

  modRece(fall: Recepactivo): Observable<any> {
    const modURL = this.apiURL + "/modrece/" + fall.seq_recepcion_activo;
      return this.http.put<Recepactivo>(modURL, fall);
      
    }

    modificarEstadoRece( fall: Recepactivo): Observable<any> {
      let url = this.apiURL + "/modReceEst/"+ fall.seq_recepcion_activo;  
      return this.http.put(url, fall);
    } 
}
