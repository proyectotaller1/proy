import { Injectable } from '@angular/core';
import { Trabajo } from '../model/trabajo';
import { Observable } from 'rxjs'; //importamos
import { HttpClient,HttpParams } from '@angular/common/http';
@Injectable({
  providedIn: 'root'
})
export class TrabajoService {

  private apiURL = '/api'

  constructor(private http: HttpClient) {

  }
  //listar solicitud

  getListaTrabajo(): Observable<Trabajo[]> {
    return this.http.get<Trabajo[]>(this.apiURL + "/listrabajo")
  }
  //adicionar solicitud
  addTrabajo(soli: Trabajo): Observable<any> {
    console.log("llegan datos: ");
    return this.http.post<Trabajo>(this.apiURL + "/addtrab", soli);
  }
  modtrab(fall: Trabajo): Observable<any> {
    const modURL = this.apiURL + "/modtrab/" + fall.seq_orden_trabajo;
      return this.http.put<Trabajo>(modURL, fall);
      
    }
  getSolicitudCompleta(seqSolicitud: number): Observable<Trabajo> {
    const url = `${this.apiURL}/ObtenerCompleta/${seqSolicitud}`;
    return this.http.get<Trabajo>(url);
  }
 //listar activo inactivo o todos
 FiltrarActivoInactivo(cod_estado: number): Observable<any> {
  const listaPorEstado = this.apiURL + "/filtrotrabajo/" + cod_estado;
  return this.http.get(listaPorEstado);
}
/*----------modficar solicitud estado ----*/
modificarEstadoTra(soli: Trabajo): Observable<any> {
  let url = this.apiURL + "/modTraEst/" +  soli.seq_orden_trabajo;
  return this.http.put(url, soli);
}

/*filtrar fecha desde y hasta--*/
listaTraPorFecha(fechaDesde: Date, fechaHasta: Date): Observable<any> {
  const params = new HttpParams()
    .set('fechaDesde', fechaDesde.toISOString().split('T')[0]) //convercion 
    .set('fechaHasta', fechaHasta.toISOString().split('T')[0]);
  return this.http.get<any>(`${this.apiURL}/listtra/fecha_emicion`, { params });
}

}
