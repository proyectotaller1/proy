import { Injectable } from '@angular/core';
import { Observable } from 'rxjs'; //importamos
import { HttpClient,HttpParams } from '@angular/common/http';
import { Tecnico } from '../model/tecnico';
import { Trabajo } from '../model/trabajo';
@Injectable({
  providedIn: 'root'
})
export class TecnicoService {
  private apiURL = '/api'
  constructor(private http: HttpClient) {
  }
  //listar solicitud
  getListaTecnico(): Observable<Tecnico[]> {
    return this.http.get<Tecnico[]>(this.apiURL + "/listecnico")
  }
  //listar tenico ejecutante segun trabajo que corresponde
  getListaTecnicoSegunTrab(solSer:Trabajo):Observable<any>{
    return this.http.get<any>(this.apiURL+'/ListartenicoActivo/'+solSer.seq_orden_trabajo)
  } 
  //adicionar solicitud
  addTecnico(soli: Tecnico): Observable<any> {
    console.log("llegan datos: ");
    return this.http.post<Tecnico>(this.apiURL + "/addtec", soli);
  }
  /*----------modficar solicitud estado ----*/
  modificarEstadoTecnico(soli: Tecnico): Observable<any> {
    let url = this.apiURL + "/modOrdenEst/" +  soli.seq_tecnicos_ejecutantes;
    return this.http.put(url, soli);
  }
}
