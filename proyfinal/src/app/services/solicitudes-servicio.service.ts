import { Injectable } from '@angular/core';
import { Observable } from 'rxjs'; //importamos
import { HttpClient,HttpParams } from '@angular/common/http';
import { SolicitudesServicio } from '../model/solicitudes-servicio';
import { Persona } from '../model/persona';
@Injectable({
  providedIn: 'root'
})
export class SolicitudesServicioService {

  private apiURL = '/api'

  constructor(private http: HttpClient) {

  }
  //listar solicitud

  getListaSoli(): Observable<SolicitudesServicio[]> {
    return this.http.get<SolicitudesServicio[]>(this.apiURL + "/lissol")
  }


  //adicionar solicitud
  addSoli(soli: SolicitudesServicio): Observable<any> {
    console.log("llegan datos: "+soli.personas);
    
    return this.http.post<SolicitudesServicio>(this.apiURL + "/addsol", soli);
  }

  //modificar solicitud

  modSoli(soli: SolicitudesServicio): Observable<any> {
    const modURL = this.apiURL + "/modsol/" + soli.seq_solicitud_servicio;
    return this.http.put<SolicitudesServicio>(modURL, soli);

  }
  //listar activo inactivo o todos
  listaSoliEstado(cod_estado: number): Observable<any> {
    const listaPorEstado = this.apiURL + "/listsol/" + cod_estado;
    return this.http.get(listaPorEstado);
  }
  /*----------modficar solicitud estado ----*/
  modificarEstadoSoli(soli: SolicitudesServicio): Observable<any> {
    let url = this.apiURL + "/modSolEst/" +  soli.seq_solicitud_servicio;
    return this.http.put(url, soli);
  }

  /*filtrar fecha desde y hasta--*/
  listaSoliPorFecha(fechaDesde: Date, fechaHasta: Date): Observable<any> {
    const params = new HttpParams()
      .set('fechaDesde', fechaDesde.toISOString().split('T')[0]) //convercion 
      .set('fechaHasta', fechaHasta.toISOString().split('T')[0]);
    return this.http.get<any>(`${this.apiURL}/listsol/fecha`, { params });
  }
  getListaCliente():Observable<any>{
    return this.http.get<Persona>(this.apiURL+'/lisPersonasCliente')
  }
    

}
