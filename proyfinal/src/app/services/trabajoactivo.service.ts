import { Injectable } from '@angular/core';
import { Observable } from 'rxjs'; //importamos
import { HttpClient,HttpParams } from '@angular/common/http';
import { Trabajo } from '../model/trabajo';
import { Activos } from '../model/activos';
import { Trabajoactivo } from '../model/trabajoactivo';

@Injectable({
  providedIn: 'root'
})
export class TrabajoactivoService {

  private apiURL = '/api'
  constructor(private http: HttpClient) {
  }
  //listar solicitud
  getListaOrdenTrabajo(): Observable<Trabajoactivo[]> {
    return this.http.get<Trabajoactivo[]>(this.apiURL + "/lisorden")
  }
  //listar por orden activo segun orden de trabajo
  getListaActivosSegunSoli(seq: number): Observable<any[]> {
    return this.http.get<any[]>(this.apiURL + "/listaractt"+seq)
  }
  //listar tenico ejecutante segun trabajo que corresponde
  getListaOrdenSegunTrab(solSer:Trabajo):Observable<any>{
    return this.http.get<any>(this.apiURL+'/ListarOrdenActivo/'+solSer.seq_orden_trabajo)
  } 
  //adicionarorden de trabajoa activo
  addOrdenActivo(xactivo: Trabajoactivo): Observable<any> {
    return this.http.post<Trabajoactivo>(this.apiURL + "/addtrabactivo", xactivo);
  }
  /*----------modficar solicitud estado ----*/
  modificarEstadoOrdenTraActivo(soli: Trabajoactivo): Observable<any> {
    let url = this.apiURL + "/modOrdenAcEst/" +  soli.activos?.seq_activo;
    return this.http.put(url, soli);
  }
}
