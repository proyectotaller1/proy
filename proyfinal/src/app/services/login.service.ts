import { Injectable } from '@angular/core';
import { HttpClient, HttpParams, HttpHeaders } from '@angular/common/http';
import { Login } from '../model/login';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class LoginService {
  private apiURL = '/api'
  clave: string ="barra"
  clave1: string ="barra1"
  clave2: string ="barra2"
  clave3: string ="barra3"
  clave4: string ="barra3"
  constructor(
    private http:HttpClient
  ) { }
/*------sacar datos para rol listr y seleccionar desde la cabecera--*/
guardarDato(dat: any){
  localStorage.setItem(this.clave, JSON.stringify(dat))
}
obtenerDato(){
  const storedData = localStorage.getItem(this.clave)
  return storedData?JSON.parse(storedData):null
}
guardarRol(dat: any){
  localStorage.setItem(this.clave2, JSON.stringify(dat))
}
obtenerRol(){
  const storedData = localStorage.getItem(this.clave2)
  return storedData?JSON.parse(storedData):null
}

setBarra(num: number){
  localStorage.setItem(this.clave3, JSON.stringify(num))
}
getBarra(){
  const storedData = localStorage.getItem(this.clave3)
  return storedData?JSON.parse(storedData):null
}


setOpcion(num: number){
  localStorage.setItem(this.clave4, JSON.stringify(num))
}
getOpcion(){
  const storedData = localStorage.getItem(this.clave4)
  return storedData?JSON.parse(storedData):null
}



validarLogin(log: Login): Observable<any>{
  const parametro = new HttpParams()
  .set('usuario', log.usuario)
  .set('contrasena', log.contrasena);
  return this.http.post('/api/login',parametro.toString(),
  {
    headers: new HttpHeaders()
    .set('Content-type', 'application/x-www-form-urlencoded')
  })
}


}