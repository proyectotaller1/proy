import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

import { Observable, of, throwError } from 'rxjs';
import { catchError, map } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class FotoService {

  private apiURL='/api'
  private apiUrl='http://localhost:8092/'
  constructor(private http:HttpClient) { }

  
  
  getImage(imageName:string): Observable<Blob> {
    return this.http.get(`${this.apiUrl}${imageName}`, { responseType: 'blob' });
  } 

  loadImg(imageName: string): Observable<any> {
    return this.getImage(imageName).pipe(
      map((imagenBlob: Blob) => {
        // Convierte el Blob a una URL segura para la imagen
        const imagenUrl = URL.createObjectURL(imagenBlob);
        return imagenUrl;
      }),
      catchError((error: any) => {
        console.error('Error al cargar la imagen', error);
        return of(null);
      })
    );
  }


  uploadFile(formData:FormData):Observable<any>{
    return this.http.post(this.apiURL+'/images',formData);
  }
}
