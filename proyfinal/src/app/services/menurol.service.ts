import { Injectable } from '@angular/core';
import { Login } from '../model/login';

@Injectable({
  providedIn: 'root'
})
export class MenurolService {
clave="rol"
  constructor() { }
  guardarRol(dat: any){
    localStorage.setItem(this.clave, JSON.stringify(dat))
  }
  obtenerRol(){
    const storedData = localStorage.getItem(this.clave)
    return storedData?JSON.parse(storedData):null
  }
}
