import { Component } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { Login } from 'src/app/model/login';
import { EusuariosService } from 'src/app/services/eusuarios.service';
import { LoginService } from 'src/app/services/login.service';


@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent {
  xusuario!:any
  valido:any;
  
  regForm= new FormGroup({
    xuser : new FormControl('',[Validators.required]),
    xcontrasena  : new FormControl('',[Validators.required])
   })
   get fu(){
    return this.regForm.controls
  }
  constructor(private enviarUsu: EusuariosService, private router: Router,private logSer: LoginService){}
   get controlar(){
    return this.regForm.controls
  }
  mostrarErr: boolean= false;
  usuarioErr: boolean= false;
  mostrar(){
    console.log("mostrando....");
    this.mostrarErr= this.vacio()
  }
  vacio(){
    if(this.regForm.get('xuser')?.value==='' || this.regForm.get('xcontrasena')?.value==='' ){
      return true;

    }return false;
  }
  
  Ingresar(){
   if(this.vacio()){
    console.log("ingresando... ");
    }else{
      console.log('obteniendo');
      this.xusuario={
      usuario: this.regForm.get('xuser')?.value,
      contrasena: this.regForm.get('xcontrasena')?.value
    }
    /*---------------token------------------*/
    this.logSer.validarLogin(this.xusuario).subscribe(
      (data: Login) =>  { // start of (1)
        if (data){
            //this.xper = data;
            console.log("BIENVENIDOS....");
            console.log("token"+ data.token);
            
              this.xusuario=data.usuario                  
              localStorage.setItem('currentUser', JSON.stringify(data));
        }else{ }
      }, // end of (1)
      (error: any)   => console.log(error), // (2) second argument
      ()             => console.log('all data gets') // (3) second argument 
    );

    console.log(this.xusuario);
    this.logSer.validarLogin(this.xusuario).subscribe((val)=>{
      if(val !== null){
        this.valido = val;
        this.router.navigate(['computer_maintenance/principal']) 
        this.enviarUsu.guardarDato(val)

      }else{
        console.log("no existe el usuario");
        this.usuarioErr=true;
      }
      

      
    })
    console.log("validado " + this.valido);

  }
  }
}