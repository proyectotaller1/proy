import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { LoginComponent } from './principal/login/login.component';
import { FormControl,FormGroup,Validators,FormBuilder,ReactiveFormsModule, FormsModule } from '@angular/forms';



@NgModule({
  declarations: [
LoginComponent
  ],
  imports: [
    CommonModule,
    FormsModule,
  ]
})
export class LoginModule { }