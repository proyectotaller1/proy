import { Pipe, PipeTransform } from '@angular/core';
import { Mantenimiento } from 'src/app/model/mantenimiento';
@Pipe({
  name: 'filtromanestado'
})
export class FiltromanestadoPipe implements PipeTransform {

  transform(listaTrabajo: Mantenimiento[], estado: number): Mantenimiento[] {
    if (estado === 1) {
      return listaTrabajo.filter(trabajo => trabajo.ordenes_trabajo?.cod_atentido === 1);
    } else if (estado === 0) {
      return listaTrabajo.filter(trabajo => trabajo.ordenes_trabajo?.cod_atentido === 0);
    } else {
      return listaTrabajo;
    }
  }
}
