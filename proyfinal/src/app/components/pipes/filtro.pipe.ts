import { Pipe, PipeTransform } from '@angular/core';
import { Persona } from 'src/app/model/persona';

@Pipe({
  name: 'filtro1'
})
export class FiltroPipe implements PipeTransform {

  transform(listaa:Persona[], search:string):Persona[] {

    if(search=='')
    return listaa
    else
         return listaa.filter( (dat) => (
      (dat.nombres+" "+dat.apellido_paterno+""+dat.apellido_materno).includes(search)
    ));
  }

}

