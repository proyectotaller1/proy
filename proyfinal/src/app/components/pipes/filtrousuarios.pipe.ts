import { Pipe, PipeTransform } from '@angular/core';
import { Login } from 'src/app/model/login';

@Pipe({
  name: 'filtro3'
})
export class FiltrousuariosPipe implements PipeTransform {

  transform(lista:Login[], search:string):Login[] {

    if(search=='')
    return lista
    else
         return lista.filter( (dat) => (
      (dat.usuario).includes(search)
    ));
}

}
