import { Pipe, PipeTransform } from '@angular/core';
import { Trabajo } from 'src/app/model/trabajo';

@Pipe({
  name: 'filtroatendido'
})
export class FiltroatendidoPipe implements PipeTransform {

  transform(listaTrabajo: Trabajo[], estado: number): Trabajo[] {
    if (estado === 1) {
      return listaTrabajo.filter(trabajo => trabajo.cod_atentido === 1);
    } else if (estado === 0) {
      return listaTrabajo.filter(trabajo => trabajo.cod_atentido === 0);
    } else {
      return listaTrabajo;
    }
  }

}
