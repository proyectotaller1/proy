import { Pipe, PipeTransform } from '@angular/core';
import { SolicitudesServicio } from 'src/app/model/solicitudes-servicio';

@Pipe({
  name: 'filtrofechasoli'
})
export class FiltrofechasoliPipe implements PipeTransform {  
  transform(solicitudes: SolicitudesServicio[], fechaDesde: Date, fechaHasta: Date): SolicitudesServicio[] {
    if (!solicitudes || solicitudes.length === 0) {
      return [];
    }
  
    return solicitudes.filter(solicitud => {
      const fechaSolicitud = new Date(solicitud.fecha);
      fechaSolicitud.setHours(0, 0, 0, 0); // Ajusta la hora a 00:00:00
  
      return fechaSolicitud >= fechaDesde && fechaSolicitud <= fechaHasta;
    });
  }
}
