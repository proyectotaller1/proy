import { Pipe, PipeTransform } from '@angular/core';
import { Roles } from 'src/app/model/roles';
@Pipe({
  name: 'filtrorol'
})
export class FiltrorolPipe implements PipeTransform {

  transform(xlista: Roles[], marcados: Roles[], marcar: number): Roles[] {
    let listados: Roles[] = xlista
    if (marcar == -1)
      return xlista;
    else {
      if (marcar == 1) {
        return marcados;
      } else {
        const excluidos = new Set(marcados.map(item => item['id_rol']));
        return xlista.filter(item => !excluidos.has(item['id_rol']));
      }
    }
  }

}
