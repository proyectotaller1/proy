import { Pipe, PipeTransform } from '@angular/core';
import { Mantenimiento } from 'src/app/model/mantenimiento';
@Pipe({
  name: 'filtroman'
})
export class FiltromanPipe implements PipeTransform {

  transform(lista: Mantenimiento[], search: string): Mantenimiento[] {

    if (search == '')
      return lista
    else
      return lista.filter((dat) => (
    (dat.ordenes_trabajo?.solicitudes_servicio?.personas?.nombres.toLowerCase()+""+dat.ordenes_trabajo?.solicitudes_servicio?.personas?.apellido_paterno.toLowerCase()).includes(search) 
      ));
  }

}
