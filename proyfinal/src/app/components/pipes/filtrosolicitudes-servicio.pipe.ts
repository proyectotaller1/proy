import { Pipe, PipeTransform } from '@angular/core';
import { SolicitudesServicio } from 'src/app/model/solicitudes-servicio';

@Pipe({
  name: 'filtrosol'
})
export class FiltrosolicitudesServicioPipe implements PipeTransform {

  transform(lista: SolicitudesServicio[], search: string): SolicitudesServicio[] {

    if (search == '')
      return lista
    else
      return lista.filter((dat) => (
    (dat.personas?.nombres.toLowerCase()+""+dat.personas?.apellido_paterno.toLowerCase()).includes(search) 
      ));
  }
}
