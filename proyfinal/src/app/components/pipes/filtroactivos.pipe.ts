import { Pipe, PipeTransform } from '@angular/core';
import { Activos } from 'src/app/model/activos';

@Pipe({
  name: 'filtroactivos'
})
export class FiltroactivosPipe implements PipeTransform {
  transform(lista:Activos[], search:string):Activos[] {

    if(search=='')
    return lista
    else
         return lista.filter( (dat) => (
      (dat.id_etiqueta_fisica+" "+dat.nombre).includes(search)
    ));
}

}
