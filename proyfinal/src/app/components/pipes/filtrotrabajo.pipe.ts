import { Pipe, PipeTransform } from '@angular/core';
import { Trabajo } from 'src/app/model/trabajo';
@Pipe({
  name: 'filtrotrabajo'
})
export class FiltrotrabajoPipe implements PipeTransform {

 
  transform(lista: Trabajo[], search: string): Trabajo[] {

    if (search == '')
      return lista
    else
      return lista.filter((dat) => (
    (dat.solicitudes_servicio?.personas?.nombres.toLowerCase()+""+dat.solicitudes_servicio?.personas?.apellido_paterno.toLowerCase()).includes(search) 
      ));
  }
}
