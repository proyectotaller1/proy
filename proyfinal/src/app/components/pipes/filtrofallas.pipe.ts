import { Pipe, PipeTransform } from '@angular/core';
import { Fallas } from 'src/app/model/fallas';

@Pipe({
  name: 'filtro2'
})
export class FiltrofallasPipe implements PipeTransform {

  transform(lista:Fallas[], search:string):Fallas[] {

    if(search=='')
    return lista
    else
         return lista.filter( (dat) => (
      (dat.nombre).includes(search)
    ));
}
}