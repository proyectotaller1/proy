import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { CommonModule } from '@angular/common';
import { LoginComponent } from './auth/principal/login/login.component';
import { InterfaceprincipalComponent } from './interfaceprincipal/interfaceprincipal.component';

const routes: Routes = [
  { path: '', redirectTo: '/computer_maintenance/login', pathMatch: 'full'},
  { path: 'computer_maintenance/login', component: LoginComponent },
  { path: 'computer_maintenance', component: InterfaceprincipalComponent , children: [{ 
    path:'',
    loadChildren: () => import('./interfaceprincipal/interfaceprincipal.module').then(m => m.InterfaceprincipalModule)
  }] }
  
];


@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
