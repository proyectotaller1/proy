import { Injectable } from '@angular/core';
import { Persona } from '../model/persona';
import { Usuarios } from '../model/usuarios';
import { HttpClient, HttpHeaders, HttpParams } from '@angular/common/http';
import { Observable } from 'rxjs';
import { environment } from 'src/environments/environment.prod';
import { Solicitud } from '../model/solicitud';
@Injectable({
  providedIn: 'root'
})
export class UsuariosService {
  private url = `${environment.apiUrl}`;
  constructor(
    private http:HttpClient
  ) { }
  validarLogin(log: Usuarios): Observable<any>{
    const parametro = new HttpParams()
    .set('usuario', log.usuario)
    .set('contrasena', log.contrasena);
    console.log("llega usuarioo::"+log.usuario);
    
    return this.http.post<any>(this.url+'/login',parametro.toString(),
    {
      headers: new HttpHeaders()
      .set('Content-type', 'application/x-www-form-urlencoded')
    })
    
  }
  
  getListaPorSoliPersona(seq: number): Observable<any[]> {
    const usu: Usuarios= this.obtnerdato()
    const headers = new HttpHeaders({
      'Authorization': usu.token+''
    });
    console.log("llegan daotsssssssss::"+this.obtnerdato);
    return this.http.get<Solicitud[]>(this.url + '/listarPerSol/'+seq, {headers: headers});
  }
  clave: string = "usu";
  
  guardarDato(data: any) {
    if (data) {
      localStorage.setItem(this.clave, JSON.stringify(data));
      console.log("Datos guardados correctamente.");
    } else {
      console.error("Intentando guardar datos nulos o no válidos.");
    }
  }
  obtnerdato() {
    const storedData = localStorage.getItem(this.clave);
    console.log("Datos obtenidos:", storedData);
  
    return storedData ? JSON.parse(storedData) : {};
  }



}
