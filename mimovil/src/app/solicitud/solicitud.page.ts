import { Component, OnInit } from '@angular/core';
import { Persona } from '../model/persona';
import { SolicitudService } from '../service/solicitud.service';
import { Usuarios } from '../model/usuarios';
import { Solicitud } from '../model/solicitud';
import { UsuariosService } from '../service/usuarios.service';
@Component({
  selector: 'app-solicitud',
  templateUrl: './solicitud.page.html',
  styleUrls: ['./solicitud.page.scss'],
})
export class SolicitudPage implements OnInit {

  constructor(private solService: UsuariosService) { }
  
  listaSolicitud: Solicitud[]=[]; //objeto persona
  mostradoPersona?: boolean;  // Propiedad agregada
  ngOnInit() {
    const xusurio: Usuarios = this.solService.obtnerdato()
    const xpersona: any = xusurio.personas
      this.solService.getListaPorSoliPersona(xpersona.seq_persona).subscribe(xlista => this.listaSolicitud = xlista)
      
  }

}
