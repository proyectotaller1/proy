import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { HomePage } from '../home/home.page';
import { SolicitudPage } from './solicitud.page';

const routes: Routes = [
  {
    path: '',
    component: SolicitudPage
  }
];


@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class SolicitudPageRoutingModule {}
