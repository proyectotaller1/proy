import { Component } from '@angular/core';
import { FormControl,FormGroup, Validators } from '@angular/forms';
import { Usuarios } from '../model/usuarios';
import { UsuariosService } from '../service/usuarios.service';
import { Router } from '@angular/router';
import { XusuService } from '../service/xusu.service';
@Component({
  selector: 'app-home',
  templateUrl: 'home.page.html',
  styleUrls: ['home.page.scss'],
})
export class HomePage {

  xusuario!:any
  valido:any;
  
  regForm= new FormGroup({
    xuser : new FormControl('',[Validators.required]),
    xcontrasena  : new FormControl('',[Validators.required])
   })
   get fu(){
    return this.regForm.controls
  }
  constructor(private enviarUsu: XusuService, private router: Router,private logSer: UsuariosService) {}
  get controlar(){
    return this.regForm.controls
  }
  mostrarErr: boolean= false;
  usuarioErr: boolean= false;
  mostrar(){
    console.log("mostrando....");
    this.mostrarErr= this.vacio()
  }
  vacio(){
    if(this.regForm.get('xuser')?.value==='' || this.regForm.get('xcontrasena')?.value==='' ){
      return true;

    }return false;
  }
  
  Ingresar(){
      console.log('obteniendo');
      this.xusuario={
      usuario: this.regForm.get('xuser')?.value,
      contrasena: this.regForm.get('xcontrasena')?.value
      }
    /*---------------token------------------*/
    
    console.log(this.xusuario);
    this.logSer.validarLogin(this.xusuario).subscribe((val)=>{
      if(val != null){
        this.valido = val;
        console.log("validado " + this.valido);
        console.log("token: "+this.valido.token);
         // Almacena los datos del usuario en el localStorage
      this.logSer.guardarDato(this.valido);
         // Navegar a la página principal
          // Navegar a la página principal
          this.router.navigate(['solicitud']);
      }else{
        console.log("no existe el usuario");
        // this.usuarioErr=true;
      }
    })
    
  }
  }

