import { Persona } from "./persona";
export interface Usuarios {
    usuario: string;
    contrasena: string;
    cod_estado: number;
    personas?: Persona;
    token?: string;
}
