export interface Persona {
    seq_persona: number;
    cedula_identidad: string;
    nombres: string
    apellido_paterno: string;
    apellido_materno: string;
    fotografia: string
    telefono_celular: string
    cod_estado: number;
}
