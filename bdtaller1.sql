PGDMP     5    7                |         	   proyfinal    15.3    15.3 W    w           0    0    ENCODING    ENCODING        SET client_encoding = 'UTF8';
                      false            x           0    0 
   STDSTRINGS 
   STDSTRINGS     (   SET standard_conforming_strings = 'on';
                      false            y           0    0 
   SEARCHPATH 
   SEARCHPATH     8   SELECT pg_catalog.set_config('search_path', '', false);
                      false            z           1262    17263 	   proyfinal    DATABASE     ~   CREATE DATABASE proyfinal WITH TEMPLATE = template0 ENCODING = 'UTF8' LOCALE_PROVIDER = libc LOCALE = 'Spanish_Bolivia.1252';
    DROP DATABASE proyfinal;
                postgres    false            �            1259    17362    activos    TABLE     F  CREATE TABLE public.activos (
    seq_activo integer NOT NULL,
    id_etiqueta_fisica character varying(255),
    nombre character varying(255) NOT NULL,
    marca character varying(255) NOT NULL,
    modelo character varying(255),
    comentarios character varying(255) NOT NULL,
    cod_estado integer DEFAULT 1 NOT NULL
);
    DROP TABLE public.activos;
       public         heap    postgres    false            �            1259    17361    activos_seq_activo_seq    SEQUENCE     �   CREATE SEQUENCE public.activos_seq_activo_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 -   DROP SEQUENCE public.activos_seq_activo_seq;
       public          postgres    false    223            {           0    0    activos_seq_activo_seq    SEQUENCE OWNED BY     Q   ALTER SEQUENCE public.activos_seq_activo_seq OWNED BY public.activos.seq_activo;
          public          postgres    false    222            �            1259    17322    fallas    TABLE     �   CREATE TABLE public.fallas (
    id_falla integer NOT NULL,
    nombre character varying(255) NOT NULL,
    cod_estado integer DEFAULT 1 NOT NULL
);
    DROP TABLE public.fallas;
       public         heap    postgres    false            �            1259    17555    mantenimiento_correctivos    TABLE     )  CREATE TABLE public.mantenimiento_correctivos (
    seq_mantenimiento_correctivo integer NOT NULL,
    fecha_finalizacion date,
    comentario_deteccion_falla character varying(255) NOT NULL,
    descripcion_causa character varying(255),
    cod_tipo_solucion character varying(255) NOT NULL,
    descripcion_solucion character varying(255),
    duracion character varying(255) NOT NULL,
    comentarios character varying(255) NOT NULL,
    cod_estado integer DEFAULT 1 NOT NULL,
    id_falla integer NOT NULL,
    seq_orden_trabajo integer NOT NULL
);
 -   DROP TABLE public.mantenimiento_correctivos;
       public         heap    postgres    false            �            1259    17554 :   mantenimiento_correctivos_seq_mantenimiento_correctivo_seq    SEQUENCE     �   CREATE SEQUENCE public.mantenimiento_correctivos_seq_mantenimiento_correctivo_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 Q   DROP SEQUENCE public.mantenimiento_correctivos_seq_mantenimiento_correctivo_seq;
       public          postgres    false    233            |           0    0 :   mantenimiento_correctivos_seq_mantenimiento_correctivo_seq    SEQUENCE OWNED BY     �   ALTER SEQUENCE public.mantenimiento_correctivos_seq_mantenimiento_correctivo_seq OWNED BY public.mantenimiento_correctivos.seq_mantenimiento_correctivo;
          public          postgres    false    232            �            1259    17402    ordenes_trabajo    TABLE     �  CREATE TABLE public.ordenes_trabajo (
    seq_orden_trabajo integer NOT NULL,
    cod_tipo_mantenimiento character varying(255) NOT NULL,
    numero_orden_trabajo character varying(255) NOT NULL,
    fecha_emicion date NOT NULL,
    ubicacion_activo character varying(255),
    seq_persona integer NOT NULL,
    seq_solicitud_servicio integer NOT NULL,
    cod_atentido integer DEFAULT 0 NOT NULL,
    cod_estado integer DEFAULT 1 NOT NULL
);
 #   DROP TABLE public.ordenes_trabajo;
       public         heap    postgres    false            �            1259    17433    ordenes_trabajo_activo    TABLE     x   CREATE TABLE public.ordenes_trabajo_activo (
    seq_activo integer NOT NULL,
    seq_orden_trabajo integer NOT NULL
);
 *   DROP TABLE public.ordenes_trabajo_activo;
       public         heap    postgres    false            �            1259    17432 ,   ordenes_trabajo_activo_seq_orden_trabajo_seq    SEQUENCE     �   CREATE SEQUENCE public.ordenes_trabajo_activo_seq_orden_trabajo_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 C   DROP SEQUENCE public.ordenes_trabajo_activo_seq_orden_trabajo_seq;
       public          postgres    false    229            }           0    0 ,   ordenes_trabajo_activo_seq_orden_trabajo_seq    SEQUENCE OWNED BY     }   ALTER SEQUENCE public.ordenes_trabajo_activo_seq_orden_trabajo_seq OWNED BY public.ordenes_trabajo_activo.seq_orden_trabajo;
          public          postgres    false    228            �            1259    17401 %   ordenes_trabajo_seq_orden_trabajo_seq    SEQUENCE     �   CREATE SEQUENCE public.ordenes_trabajo_seq_orden_trabajo_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 <   DROP SEQUENCE public.ordenes_trabajo_seq_orden_trabajo_seq;
       public          postgres    false    227            ~           0    0 %   ordenes_trabajo_seq_orden_trabajo_seq    SEQUENCE OWNED BY     o   ALTER SEQUENCE public.ordenes_trabajo_seq_orden_trabajo_seq OWNED BY public.ordenes_trabajo.seq_orden_trabajo;
          public          postgres    false    226            �            1259    17265    personas    TABLE       CREATE TABLE public.personas (
    seq_persona integer NOT NULL,
    cedula_identidad character varying(255),
    nombres character varying(255) NOT NULL,
    apellido_paterno character varying(255) NOT NULL,
    apellido_materno character varying(255),
    fotografia character varying(255),
    telefono_celular character varying(255),
    cod_estado integer DEFAULT 1 NOT NULL
);
    DROP TABLE public.personas;
       public         heap    postgres    false            �            1259    17264    personas_seq_persona_seq    SEQUENCE     �   CREATE SEQUENCE public.personas_seq_persona_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 /   DROP SEQUENCE public.personas_seq_persona_seq;
       public          postgres    false    215                       0    0    personas_seq_persona_seq    SEQUENCE OWNED BY     U   ALTER SEQUENCE public.personas_seq_persona_seq OWNED BY public.personas.seq_persona;
          public          postgres    false    214            �            1259    17372    recepciones_activos    TABLE       CREATE TABLE public.recepciones_activos (
    seq_recepcion_activo integer NOT NULL,
    fecha date NOT NULL,
    seq_persona integer NOT NULL,
    seq_solicitud_servicio integer NOT NULL,
    seq_activo integer NOT NULL,
    cod_estado integer DEFAULT 1 NOT NULL
);
 '   DROP TABLE public.recepciones_activos;
       public         heap    postgres    false            �            1259    17371 ,   recepciones_activos_seq_recepcion_activo_seq    SEQUENCE     �   CREATE SEQUENCE public.recepciones_activos_seq_recepcion_activo_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 C   DROP SEQUENCE public.recepciones_activos_seq_recepcion_activo_seq;
       public          postgres    false    225            �           0    0 ,   recepciones_activos_seq_recepcion_activo_seq    SEQUENCE OWNED BY     }   ALTER SEQUENCE public.recepciones_activos_seq_recepcion_activo_seq OWNED BY public.recepciones_activos.seq_recepcion_activo;
          public          postgres    false    224            �            1259    17283    roles    TABLE     �   CREATE TABLE public.roles (
    id_rol integer NOT NULL,
    nombre character varying(255) NOT NULL,
    cod_estado integer DEFAULT 1 NOT NULL
);
    DROP TABLE public.roles;
       public         heap    postgres    false            �            1259    17329    solicitudes_servicio    TABLE     �   CREATE TABLE public.solicitudes_servicio (
    seq_solicitud_servicio integer NOT NULL,
    fecha date NOT NULL,
    descripcion_problema character varying(255),
    seq_persona integer NOT NULL,
    cod_estado integer DEFAULT 1 NOT NULL
);
 (   DROP TABLE public.solicitudes_servicio;
       public         heap    postgres    false            �            1259    17328 /   solicitudes_servicio_seq_solicitud_servicio_seq    SEQUENCE     �   CREATE SEQUENCE public.solicitudes_servicio_seq_solicitud_servicio_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 F   DROP SEQUENCE public.solicitudes_servicio_seq_solicitud_servicio_seq;
       public          postgres    false    221            �           0    0 /   solicitudes_servicio_seq_solicitud_servicio_seq    SEQUENCE OWNED BY     �   ALTER SEQUENCE public.solicitudes_servicio_seq_solicitud_servicio_seq OWNED BY public.solicitudes_servicio.seq_solicitud_servicio;
          public          postgres    false    220            �            1259    17475    tecnicos_ejecutantes    TABLE     (  CREATE TABLE public.tecnicos_ejecutantes (
    seq_tecnicos_ejecutantes integer NOT NULL,
    fecha_de_asignacion date NOT NULL,
    cod_responsable integer DEFAULT 0 NOT NULL,
    seq_persona integer NOT NULL,
    seq_orden_trabajo integer NOT NULL,
    cod_estado integer DEFAULT 1 NOT NULL
);
 (   DROP TABLE public.tecnicos_ejecutantes;
       public         heap    postgres    false            �            1259    17474 1   tecnicos_ejecutantes_seq_tecnicos_ejecutantes_seq    SEQUENCE     �   CREATE SEQUENCE public.tecnicos_ejecutantes_seq_tecnicos_ejecutantes_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 H   DROP SEQUENCE public.tecnicos_ejecutantes_seq_tecnicos_ejecutantes_seq;
       public          postgres    false    231            �           0    0 1   tecnicos_ejecutantes_seq_tecnicos_ejecutantes_seq    SEQUENCE OWNED BY     �   ALTER SEQUENCE public.tecnicos_ejecutantes_seq_tecnicos_ejecutantes_seq OWNED BY public.tecnicos_ejecutantes.seq_tecnicos_ejecutantes;
          public          postgres    false    230            �            1259    17272    usuarios    TABLE     �   CREATE TABLE public.usuarios (
    usuario character varying(255) NOT NULL,
    contrasena character varying(255) NOT NULL,
    seq_persona integer NOT NULL,
    cod_estado integer DEFAULT 1 NOT NULL,
    token character varying(255)
);
    DROP TABLE public.usuarios;
       public         heap    postgres    false            �            1259    17289    usuarios_roles    TABLE     q   CREATE TABLE public.usuarios_roles (
    usuario character varying(255) NOT NULL,
    id_rol integer NOT NULL
);
 "   DROP TABLE public.usuarios_roles;
       public         heap    postgres    false            �           2604    17365    activos seq_activo    DEFAULT     x   ALTER TABLE ONLY public.activos ALTER COLUMN seq_activo SET DEFAULT nextval('public.activos_seq_activo_seq'::regclass);
 A   ALTER TABLE public.activos ALTER COLUMN seq_activo DROP DEFAULT;
       public          postgres    false    223    222    223            �           2604    17558 6   mantenimiento_correctivos seq_mantenimiento_correctivo    DEFAULT     �   ALTER TABLE ONLY public.mantenimiento_correctivos ALTER COLUMN seq_mantenimiento_correctivo SET DEFAULT nextval('public.mantenimiento_correctivos_seq_mantenimiento_correctivo_seq'::regclass);
 e   ALTER TABLE public.mantenimiento_correctivos ALTER COLUMN seq_mantenimiento_correctivo DROP DEFAULT;
       public          postgres    false    232    233    233            �           2604    17405 !   ordenes_trabajo seq_orden_trabajo    DEFAULT     �   ALTER TABLE ONLY public.ordenes_trabajo ALTER COLUMN seq_orden_trabajo SET DEFAULT nextval('public.ordenes_trabajo_seq_orden_trabajo_seq'::regclass);
 P   ALTER TABLE public.ordenes_trabajo ALTER COLUMN seq_orden_trabajo DROP DEFAULT;
       public          postgres    false    226    227    227            �           2604    17436 (   ordenes_trabajo_activo seq_orden_trabajo    DEFAULT     �   ALTER TABLE ONLY public.ordenes_trabajo_activo ALTER COLUMN seq_orden_trabajo SET DEFAULT nextval('public.ordenes_trabajo_activo_seq_orden_trabajo_seq'::regclass);
 W   ALTER TABLE public.ordenes_trabajo_activo ALTER COLUMN seq_orden_trabajo DROP DEFAULT;
       public          postgres    false    228    229    229            �           2604    17268    personas seq_persona    DEFAULT     |   ALTER TABLE ONLY public.personas ALTER COLUMN seq_persona SET DEFAULT nextval('public.personas_seq_persona_seq'::regclass);
 C   ALTER TABLE public.personas ALTER COLUMN seq_persona DROP DEFAULT;
       public          postgres    false    214    215    215            �           2604    17375 (   recepciones_activos seq_recepcion_activo    DEFAULT     �   ALTER TABLE ONLY public.recepciones_activos ALTER COLUMN seq_recepcion_activo SET DEFAULT nextval('public.recepciones_activos_seq_recepcion_activo_seq'::regclass);
 W   ALTER TABLE public.recepciones_activos ALTER COLUMN seq_recepcion_activo DROP DEFAULT;
       public          postgres    false    224    225    225            �           2604    17332 +   solicitudes_servicio seq_solicitud_servicio    DEFAULT     �   ALTER TABLE ONLY public.solicitudes_servicio ALTER COLUMN seq_solicitud_servicio SET DEFAULT nextval('public.solicitudes_servicio_seq_solicitud_servicio_seq'::regclass);
 Z   ALTER TABLE public.solicitudes_servicio ALTER COLUMN seq_solicitud_servicio DROP DEFAULT;
       public          postgres    false    221    220    221            �           2604    17478 -   tecnicos_ejecutantes seq_tecnicos_ejecutantes    DEFAULT     �   ALTER TABLE ONLY public.tecnicos_ejecutantes ALTER COLUMN seq_tecnicos_ejecutantes SET DEFAULT nextval('public.tecnicos_ejecutantes_seq_tecnicos_ejecutantes_seq'::regclass);
 \   ALTER TABLE public.tecnicos_ejecutantes ALTER COLUMN seq_tecnicos_ejecutantes DROP DEFAULT;
       public          postgres    false    230    231    231            j          0    17362    activos 
   TABLE DATA           q   COPY public.activos (seq_activo, id_etiqueta_fisica, nombre, marca, modelo, comentarios, cod_estado) FROM stdin;
    public          postgres    false    223   �|       f          0    17322    fallas 
   TABLE DATA           >   COPY public.fallas (id_falla, nombre, cod_estado) FROM stdin;
    public          postgres    false    219   �}       t          0    17555    mantenimiento_correctivos 
   TABLE DATA           �   COPY public.mantenimiento_correctivos (seq_mantenimiento_correctivo, fecha_finalizacion, comentario_deteccion_falla, descripcion_causa, cod_tipo_solucion, descripcion_solucion, duracion, comentarios, cod_estado, id_falla, seq_orden_trabajo) FROM stdin;
    public          postgres    false    233   C~       n          0    17402    ordenes_trabajo 
   TABLE DATA           �   COPY public.ordenes_trabajo (seq_orden_trabajo, cod_tipo_mantenimiento, numero_orden_trabajo, fecha_emicion, ubicacion_activo, seq_persona, seq_solicitud_servicio, cod_atentido, cod_estado) FROM stdin;
    public          postgres    false    227   :       p          0    17433    ordenes_trabajo_activo 
   TABLE DATA           O   COPY public.ordenes_trabajo_activo (seq_activo, seq_orden_trabajo) FROM stdin;
    public          postgres    false    229   ��       b          0    17265    personas 
   TABLE DATA           �   COPY public.personas (seq_persona, cedula_identidad, nombres, apellido_paterno, apellido_materno, fotografia, telefono_celular, cod_estado) FROM stdin;
    public          postgres    false    215   ɀ       l          0    17372    recepciones_activos 
   TABLE DATA           �   COPY public.recepciones_activos (seq_recepcion_activo, fecha, seq_persona, seq_solicitud_servicio, seq_activo, cod_estado) FROM stdin;
    public          postgres    false    225   4�       d          0    17283    roles 
   TABLE DATA           ;   COPY public.roles (id_rol, nombre, cod_estado) FROM stdin;
    public          postgres    false    217   ��       h          0    17329    solicitudes_servicio 
   TABLE DATA           |   COPY public.solicitudes_servicio (seq_solicitud_servicio, fecha, descripcion_problema, seq_persona, cod_estado) FROM stdin;
    public          postgres    false    221   �       r          0    17475    tecnicos_ejecutantes 
   TABLE DATA           �   COPY public.tecnicos_ejecutantes (seq_tecnicos_ejecutantes, fecha_de_asignacion, cod_responsable, seq_persona, seq_orden_trabajo, cod_estado) FROM stdin;
    public          postgres    false    231   ��       c          0    17272    usuarios 
   TABLE DATA           W   COPY public.usuarios (usuario, contrasena, seq_persona, cod_estado, token) FROM stdin;
    public          postgres    false    216   �       e          0    17289    usuarios_roles 
   TABLE DATA           9   COPY public.usuarios_roles (usuario, id_rol) FROM stdin;
    public          postgres    false    218   _�       �           0    0    activos_seq_activo_seq    SEQUENCE SET     D   SELECT pg_catalog.setval('public.activos_seq_activo_seq', 4, true);
          public          postgres    false    222            �           0    0 :   mantenimiento_correctivos_seq_mantenimiento_correctivo_seq    SEQUENCE SET     h   SELECT pg_catalog.setval('public.mantenimiento_correctivos_seq_mantenimiento_correctivo_seq', 4, true);
          public          postgres    false    232            �           0    0 ,   ordenes_trabajo_activo_seq_orden_trabajo_seq    SEQUENCE SET     [   SELECT pg_catalog.setval('public.ordenes_trabajo_activo_seq_orden_trabajo_seq', 1, false);
          public          postgres    false    228            �           0    0 %   ordenes_trabajo_seq_orden_trabajo_seq    SEQUENCE SET     T   SELECT pg_catalog.setval('public.ordenes_trabajo_seq_orden_trabajo_seq', 19, true);
          public          postgres    false    226            �           0    0    personas_seq_persona_seq    SEQUENCE SET     G   SELECT pg_catalog.setval('public.personas_seq_persona_seq', 17, true);
          public          postgres    false    214            �           0    0 ,   recepciones_activos_seq_recepcion_activo_seq    SEQUENCE SET     [   SELECT pg_catalog.setval('public.recepciones_activos_seq_recepcion_activo_seq', 45, true);
          public          postgres    false    224            �           0    0 /   solicitudes_servicio_seq_solicitud_servicio_seq    SEQUENCE SET     ]   SELECT pg_catalog.setval('public.solicitudes_servicio_seq_solicitud_servicio_seq', 7, true);
          public          postgres    false    220            �           0    0 1   tecnicos_ejecutantes_seq_tecnicos_ejecutantes_seq    SEQUENCE SET     _   SELECT pg_catalog.setval('public.tecnicos_ejecutantes_seq_tecnicos_ejecutantes_seq', 7, true);
          public          postgres    false    230            �           2606    17370    activos activos_pkey 
   CONSTRAINT     Z   ALTER TABLE ONLY public.activos
    ADD CONSTRAINT activos_pkey PRIMARY KEY (seq_activo);
 >   ALTER TABLE ONLY public.activos DROP CONSTRAINT activos_pkey;
       public            postgres    false    223            �           2606    17327    fallas fallas_pkey 
   CONSTRAINT     V   ALTER TABLE ONLY public.fallas
    ADD CONSTRAINT fallas_pkey PRIMARY KEY (id_falla);
 <   ALTER TABLE ONLY public.fallas DROP CONSTRAINT fallas_pkey;
       public            postgres    false    219            �           2606    17563 8   mantenimiento_correctivos mantenimiento_correctivos_pkey 
   CONSTRAINT     �   ALTER TABLE ONLY public.mantenimiento_correctivos
    ADD CONSTRAINT mantenimiento_correctivos_pkey PRIMARY KEY (seq_mantenimiento_correctivo);
 b   ALTER TABLE ONLY public.mantenimiento_correctivos DROP CONSTRAINT mantenimiento_correctivos_pkey;
       public            postgres    false    233            �           2606    17438 2   ordenes_trabajo_activo ordenes_trabajo_activo_pkey 
   CONSTRAINT     �   ALTER TABLE ONLY public.ordenes_trabajo_activo
    ADD CONSTRAINT ordenes_trabajo_activo_pkey PRIMARY KEY (seq_activo, seq_orden_trabajo);
 \   ALTER TABLE ONLY public.ordenes_trabajo_activo DROP CONSTRAINT ordenes_trabajo_activo_pkey;
       public            postgres    false    229    229            �           2606    17409 $   ordenes_trabajo ordenes_trabajo_pkey 
   CONSTRAINT     q   ALTER TABLE ONLY public.ordenes_trabajo
    ADD CONSTRAINT ordenes_trabajo_pkey PRIMARY KEY (seq_orden_trabajo);
 N   ALTER TABLE ONLY public.ordenes_trabajo DROP CONSTRAINT ordenes_trabajo_pkey;
       public            postgres    false    227            �           2606    17271    personas personas_pkey 
   CONSTRAINT     ]   ALTER TABLE ONLY public.personas
    ADD CONSTRAINT personas_pkey PRIMARY KEY (seq_persona);
 @   ALTER TABLE ONLY public.personas DROP CONSTRAINT personas_pkey;
       public            postgres    false    215            �           2606    17378 ,   recepciones_activos recepciones_activos_pkey 
   CONSTRAINT     |   ALTER TABLE ONLY public.recepciones_activos
    ADD CONSTRAINT recepciones_activos_pkey PRIMARY KEY (seq_recepcion_activo);
 V   ALTER TABLE ONLY public.recepciones_activos DROP CONSTRAINT recepciones_activos_pkey;
       public            postgres    false    225            �           2606    17288    roles roles_pkey 
   CONSTRAINT     R   ALTER TABLE ONLY public.roles
    ADD CONSTRAINT roles_pkey PRIMARY KEY (id_rol);
 :   ALTER TABLE ONLY public.roles DROP CONSTRAINT roles_pkey;
       public            postgres    false    217            �           2606    17337 .   solicitudes_servicio solicitudes_servicio_pkey 
   CONSTRAINT     �   ALTER TABLE ONLY public.solicitudes_servicio
    ADD CONSTRAINT solicitudes_servicio_pkey PRIMARY KEY (seq_solicitud_servicio);
 X   ALTER TABLE ONLY public.solicitudes_servicio DROP CONSTRAINT solicitudes_servicio_pkey;
       public            postgres    false    221            �           2606    17482 .   tecnicos_ejecutantes tecnicos_ejecutantes_pkey 
   CONSTRAINT     �   ALTER TABLE ONLY public.tecnicos_ejecutantes
    ADD CONSTRAINT tecnicos_ejecutantes_pkey PRIMARY KEY (seq_tecnicos_ejecutantes);
 X   ALTER TABLE ONLY public.tecnicos_ejecutantes DROP CONSTRAINT tecnicos_ejecutantes_pkey;
       public            postgres    false    231            �           2606    17307    usuarios usuarios_pkey 
   CONSTRAINT     Y   ALTER TABLE ONLY public.usuarios
    ADD CONSTRAINT usuarios_pkey PRIMARY KEY (usuario);
 @   ALTER TABLE ONLY public.usuarios DROP CONSTRAINT usuarios_pkey;
       public            postgres    false    216            �           2606    17316 "   usuarios_roles usuarios_roles_pkey 
   CONSTRAINT     m   ALTER TABLE ONLY public.usuarios_roles
    ADD CONSTRAINT usuarios_roles_pkey PRIMARY KEY (usuario, id_rol);
 L   ALTER TABLE ONLY public.usuarios_roles DROP CONSTRAINT usuarios_roles_pkey;
       public            postgres    false    218    218            �           2606    17564 A   mantenimiento_correctivos mantenimiento_correctivos_id_falla_fkey    FK CONSTRAINT     �   ALTER TABLE ONLY public.mantenimiento_correctivos
    ADD CONSTRAINT mantenimiento_correctivos_id_falla_fkey FOREIGN KEY (id_falla) REFERENCES public.fallas(id_falla);
 k   ALTER TABLE ONLY public.mantenimiento_correctivos DROP CONSTRAINT mantenimiento_correctivos_id_falla_fkey;
       public          postgres    false    3253    219    233            �           2606    17569 J   mantenimiento_correctivos mantenimiento_correctivos_seq_orden_trabajo_fkey    FK CONSTRAINT     �   ALTER TABLE ONLY public.mantenimiento_correctivos
    ADD CONSTRAINT mantenimiento_correctivos_seq_orden_trabajo_fkey FOREIGN KEY (seq_orden_trabajo) REFERENCES public.ordenes_trabajo(seq_orden_trabajo);
 t   ALTER TABLE ONLY public.mantenimiento_correctivos DROP CONSTRAINT mantenimiento_correctivos_seq_orden_trabajo_fkey;
       public          postgres    false    3261    233    227            �           2606    17439 =   ordenes_trabajo_activo ordenes_trabajo_activo_seq_activo_fkey    FK CONSTRAINT     �   ALTER TABLE ONLY public.ordenes_trabajo_activo
    ADD CONSTRAINT ordenes_trabajo_activo_seq_activo_fkey FOREIGN KEY (seq_activo) REFERENCES public.activos(seq_activo);
 g   ALTER TABLE ONLY public.ordenes_trabajo_activo DROP CONSTRAINT ordenes_trabajo_activo_seq_activo_fkey;
       public          postgres    false    3257    229    223            �           2606    17444 D   ordenes_trabajo_activo ordenes_trabajo_activo_seq_orden_trabajo_fkey    FK CONSTRAINT     �   ALTER TABLE ONLY public.ordenes_trabajo_activo
    ADD CONSTRAINT ordenes_trabajo_activo_seq_orden_trabajo_fkey FOREIGN KEY (seq_orden_trabajo) REFERENCES public.ordenes_trabajo(seq_orden_trabajo);
 n   ALTER TABLE ONLY public.ordenes_trabajo_activo DROP CONSTRAINT ordenes_trabajo_activo_seq_orden_trabajo_fkey;
       public          postgres    false    229    227    3261            �           2606    17410 0   ordenes_trabajo ordenes_trabajo_seq_persona_fkey    FK CONSTRAINT     �   ALTER TABLE ONLY public.ordenes_trabajo
    ADD CONSTRAINT ordenes_trabajo_seq_persona_fkey FOREIGN KEY (seq_persona) REFERENCES public.personas(seq_persona);
 Z   ALTER TABLE ONLY public.ordenes_trabajo DROP CONSTRAINT ordenes_trabajo_seq_persona_fkey;
       public          postgres    false    3245    227    215            �           2606    17415 ;   ordenes_trabajo ordenes_trabajo_seq_solicitud_servicio_fkey    FK CONSTRAINT     �   ALTER TABLE ONLY public.ordenes_trabajo
    ADD CONSTRAINT ordenes_trabajo_seq_solicitud_servicio_fkey FOREIGN KEY (seq_solicitud_servicio) REFERENCES public.solicitudes_servicio(seq_solicitud_servicio);
 e   ALTER TABLE ONLY public.ordenes_trabajo DROP CONSTRAINT ordenes_trabajo_seq_solicitud_servicio_fkey;
       public          postgres    false    221    227    3255            �           2606    17384 7   recepciones_activos recepciones_activos_seq_activo_fkey    FK CONSTRAINT     �   ALTER TABLE ONLY public.recepciones_activos
    ADD CONSTRAINT recepciones_activos_seq_activo_fkey FOREIGN KEY (seq_activo) REFERENCES public.activos(seq_activo);
 a   ALTER TABLE ONLY public.recepciones_activos DROP CONSTRAINT recepciones_activos_seq_activo_fkey;
       public          postgres    false    3257    223    225            �           2606    17379 8   recepciones_activos recepciones_activos_seq_persona_fkey    FK CONSTRAINT     �   ALTER TABLE ONLY public.recepciones_activos
    ADD CONSTRAINT recepciones_activos_seq_persona_fkey FOREIGN KEY (seq_persona) REFERENCES public.personas(seq_persona);
 b   ALTER TABLE ONLY public.recepciones_activos DROP CONSTRAINT recepciones_activos_seq_persona_fkey;
       public          postgres    false    3245    225    215            �           2606    17389 C   recepciones_activos recepciones_activos_seq_solicitud_servicio_fkey    FK CONSTRAINT     �   ALTER TABLE ONLY public.recepciones_activos
    ADD CONSTRAINT recepciones_activos_seq_solicitud_servicio_fkey FOREIGN KEY (seq_solicitud_servicio) REFERENCES public.solicitudes_servicio(seq_solicitud_servicio);
 m   ALTER TABLE ONLY public.recepciones_activos DROP CONSTRAINT recepciones_activos_seq_solicitud_servicio_fkey;
       public          postgres    false    221    3255    225            �           2606    17338 :   solicitudes_servicio solicitudes_servicio_seq_persona_fkey    FK CONSTRAINT     �   ALTER TABLE ONLY public.solicitudes_servicio
    ADD CONSTRAINT solicitudes_servicio_seq_persona_fkey FOREIGN KEY (seq_persona) REFERENCES public.personas(seq_persona);
 d   ALTER TABLE ONLY public.solicitudes_servicio DROP CONSTRAINT solicitudes_servicio_seq_persona_fkey;
       public          postgres    false    3245    215    221            �           2606    17488 @   tecnicos_ejecutantes tecnicos_ejecutantes_seq_orden_trabajo_fkey    FK CONSTRAINT     �   ALTER TABLE ONLY public.tecnicos_ejecutantes
    ADD CONSTRAINT tecnicos_ejecutantes_seq_orden_trabajo_fkey FOREIGN KEY (seq_orden_trabajo) REFERENCES public.ordenes_trabajo(seq_orden_trabajo);
 j   ALTER TABLE ONLY public.tecnicos_ejecutantes DROP CONSTRAINT tecnicos_ejecutantes_seq_orden_trabajo_fkey;
       public          postgres    false    227    3261    231            �           2606    17483 :   tecnicos_ejecutantes tecnicos_ejecutantes_seq_persona_fkey    FK CONSTRAINT     �   ALTER TABLE ONLY public.tecnicos_ejecutantes
    ADD CONSTRAINT tecnicos_ejecutantes_seq_persona_fkey FOREIGN KEY (seq_persona) REFERENCES public.personas(seq_persona);
 d   ALTER TABLE ONLY public.tecnicos_ejecutantes DROP CONSTRAINT tecnicos_ejecutantes_seq_persona_fkey;
       public          postgres    false    3245    215    231            �           2606    17299 )   usuarios_roles usuarios_roles_id_rol_fkey    FK CONSTRAINT     �   ALTER TABLE ONLY public.usuarios_roles
    ADD CONSTRAINT usuarios_roles_id_rol_fkey FOREIGN KEY (id_rol) REFERENCES public.roles(id_rol);
 S   ALTER TABLE ONLY public.usuarios_roles DROP CONSTRAINT usuarios_roles_id_rol_fkey;
       public          postgres    false    218    3249    217            �           2606    17317 *   usuarios_roles usuarios_roles_usuario_fkey    FK CONSTRAINT     �   ALTER TABLE ONLY public.usuarios_roles
    ADD CONSTRAINT usuarios_roles_usuario_fkey FOREIGN KEY (usuario) REFERENCES public.usuarios(usuario);
 T   ALTER TABLE ONLY public.usuarios_roles DROP CONSTRAINT usuarios_roles_usuario_fkey;
       public          postgres    false    3247    216    218            �           2606    17278 "   usuarios usuarios_seq_persona_fkey    FK CONSTRAINT     �   ALTER TABLE ONLY public.usuarios
    ADD CONSTRAINT usuarios_seq_persona_fkey FOREIGN KEY (seq_persona) REFERENCES public.personas(seq_persona);
 L   ALTER TABLE ONLY public.usuarios DROP CONSTRAINT usuarios_seq_persona_fkey;
       public          postgres    false    3245    215    216            j   �   x�=�Mj�0�r
] ���3W覥0G�G2���B:���>=��(�t(��<��
5	n�jf���	Y���$55�	B���fW�ߋ��U]�̗�>�;K3��-n]��Xw��SLm_� ���(��¸�E���x0�Vvi���r���͹���t��VU(sux��~���&þ�Ony��k]��dI���G�uO;]�      f   u   x�M�A
�@D���)�b��Wwn*�&�2��MB�����<��2����Y�ďuʳ�X9�%��/.o�ܮ�uxJ�
�Q�V���;�5��*�B��V>�.­G���UT/�      t   �   x���AN1E��)r��$3�k���2�d)��P�u8G��p�B�bA�EY�����°�A�ڙ37)0c�U�ȑ+K��,X�i78�R�N�(6�I��R�P�A0JT��q:�W�.'���J������z�u��sNWs��3Snb�S�8�^-í���/�O_Y�Y5��?\�Pm���a��S\�0ts�l��j��r�Ս�͸�������?\����1��	��      n   R  x����n�0���S�:�I���igڟ�.ad[YKG����R��̪���ٟ?;D0y��#!�(T:!����u Ȁ�!�cR>%��� J�q�KF�H�<�H�T��� Kz�)�v���6���0Vc��I�w R�QE0���ٳt����X����v]�jy�K���ݴCW�kVF�qT٥
��"�Y/$����qҶGw�g��2�
�����w��v�y^A�_�C֚wӘQ���X����+秕���ҽ����ڷ2�{�7����ug~���Oa���҇�
4����������S����E�GM%|�>�X20Ey��v'�����X      p      x�3�42�2�4�2�4҆�\1z\\\ $U�      b   [  x�U��j�0E���$H��˴�B)���U7�x�(�� ې��;v�B�͙3�JJk���uC���z�ǔBh�@Ѭ�� �Ui[�,J(���0�b��y��L#� w��V0"5/��cjp��Xg�d�c!��3~��.�).��ƯN�ɬ��V�i�Y��6iDxM#��ݑ�W����zͨ��j�;'���xrj)��g�g��-��7����mh�M��up��"Zo�Wʁ($��U:㽆�"���3e
�T^8ũ�
�wVl<R��K.�N��0�n]Hx��lS�����:)+��~"dɛ�s�J�Ys�q@��f6�sϖ�rZ3��*�����R      l   P   x�M��� ��.Tv����h*(T��g�w1����	�X�2xx���7B|��qdL�漞6m����!�W      d   C   x�340��I��LKNL�/�4�240�tL����,.)��q:�d�敤�y��!��y���@^� ��d      h   �   x�]��
�0E�ӯ�T����_ꖇ�ҍ}���!�K8��b�gO��".��9+�u錖1��@���N��A�,�㏑�df��0Ni�m�P[���
��^�����,�g�t����e<�W�p_�"@M��'����>�      r   Y   x�]�K�0�5s�~��.��]�hBHx�K\=��\T��(�4�y3�{�}�K����Yf��Ŭ,�Ʊ���O�0^Ծ~ZP��o��      c   \   x�M�1
� ��99L!֨�K�P\
�x�Z$�����]
�۽��ܦp�DN����)��6gx=0�*2�qo�j�U�U�����'�      e   H   x��*M�34�440�*-N-2���,S ��2��&'���`�!��1*b�9\��"�,�e����� �C)S     